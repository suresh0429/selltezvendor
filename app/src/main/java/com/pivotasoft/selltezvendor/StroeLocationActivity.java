package com.pivotasoft.selltezvendor;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.pivotasoft.selltezvendor.Adapters.LocationAdapter;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Response.StoreLocations;
import com.pivotasoft.selltezvendor.Storage.PrefManager;
import com.pivotasoft.selltezvendor.Storage.Utilities;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StroeLocationActivity extends AppCompatActivity {
    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;
    FusedLocationProviderClient mFusedLocationClient;
    @BindView(R.id.recyclerStore)
    RecyclerView recyclerStore;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PrefManager pref;
    private boolean doubleBackToExitPressedOnce=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stroe_location);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Select Store");


        pref = new PrefManager(getApplicationContext());

        // fetch locations
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        requestStoragePermission();

        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        boolean isFirst = preferences.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME,false);
        String storeId = preferences.getString(Utilities.KEY_STORE_ID,"");
        String storeName = preferences.getString(Utilities.KEY_STORE_NAME,"");

        if (isFirst){
            Intent intent = new Intent(StroeLocationActivity.this, HomeActivity.class);
            intent.putExtra("storeId",storeId);
            intent.putExtra("storeName",storeName);
            startActivity(intent);
            finish();
        }

    }

    private void showLocationsDialog(double latitude, double longitude) {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> profile = pref.getUserDetails();
        String tokenValue = profile.get("AccessToken");


        Call<StoreLocations> call = RetrofitClient.getInstance().getApi().storeLocations(tokenValue, latitude, longitude);
        call.enqueue(new Callback<StoreLocations>() {
            @Override
            public void onResponse(Call<StoreLocations> call, Response<StoreLocations> response) {
                progressBar.setVisibility(View.GONE);
                StoreLocations addressResponse = response.body();

                if (response.isSuccessful()) {

                    if (addressResponse.getMessage().equalsIgnoreCase("success")) {


                        List<StoreLocations.StoresdataBean> locationItemsArray = response.body() != null ? response.body().getStoresdata() : null;


                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(StroeLocationActivity.this, LinearLayoutManager.VERTICAL, false);
                        recyclerStore.setLayoutManager(mLayoutManager1);
                        recyclerStore.setItemAnimator(new DefaultItemAnimator());

                        LocationAdapter adapter = new LocationAdapter(getApplicationContext(), locationItemsArray);
                        recyclerStore.setAdapter(adapter);
                    }
                } else {

                    Toast.makeText(getApplicationContext(), "no stores Found !", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StoreLocations> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                //Snackbar.make(recyclerStore, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });

    }


    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // getLastLocation();
                            fetchLocation();
                            //  Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(StroeLocationActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void fetchLocation() {

        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    // setTexts(currentLocation);
                    showLocationsDialog(currentLocation.getLatitude(), currentLocation.getLongitude());
                    //locationShowFirsttime(currentLocation.getLatitude(),currentLocation.getLongitude());
                    //Toast.makeText(getApplicationContext(), currentLocation.getLatitude() + "" + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toasty.normal(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
}
