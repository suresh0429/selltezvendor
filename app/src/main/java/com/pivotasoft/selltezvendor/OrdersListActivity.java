package com.pivotasoft.selltezvendor;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pivotasoft.selltezvendor.Adapters.OrdersListAdapter;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Response.OrderListResponse;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersListActivity extends AppCompatActivity {
    AppController appController;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtExpectedTime)
    TextView txtExpectedTime;
    @BindView(R.id.txtPaymentMode)
    TextView txtPaymentMode;
    @BindView(R.id.txtPaymentStatus)
    TextView txtPaymentStatus;
    @BindView(R.id.txtOrderStatus)
    TextView txtOrderStatus;
    @BindView(R.id.layoutParent)
    LinearLayout layoutParent;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PrefManager pref;
    OrdersListAdapter ordersListAdapter;

    String userId, tokenValue, deviceId, orderId, finalBill, expectedTime, paymentMode, paymentStatus, orderStatus1,storeId;
    String[] descriptionData = {"OrderPlace", "InProgress", "Shipped", "delivered", "Completed"};
    boolean cartStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Orders Items");
        appController = (AppController) getApplication();


        // Displaying user information from shared preferences
        pref = new PrefManager(getApplicationContext());

        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (getIntent() != null) {
            orderId = getIntent().getStringExtra("Order_ID");
            finalBill = getIntent().getStringExtra("finalBill");
            expectedTime = getIntent().getStringExtra("expectedTime");
            paymentMode = getIntent().getStringExtra("paymentMode");
            paymentStatus = getIntent().getStringExtra("paymentStatus");
            orderStatus1 = getIntent().getStringExtra("orderStatus");
            storeId = getIntent().getStringExtra("storeId");
            cartStatus = getIntent().getBooleanExtra("Checkout", false);
        }


        if (appController.isConnection()) {

            prepareOrderListData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
    }


    private void prepareOrderListData() {

        progressBar.setVisibility(View.VISIBLE);

        Call<OrderListResponse> call = RetrofitClient.getInstance().getApi().ordersList(tokenValue, orderId);
        call.enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(Call<OrderListResponse> call, Response<OrderListResponse> response) {

                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {

                    OrderListResponse orderListResponse = response.body();

                    List<OrderListResponse.ItemdataBean> productsBeanList = orderListResponse.getItemdata();

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    ordersListAdapter = new OrdersListAdapter(OrdersListActivity.this, productsBeanList);
                    recyclerView.setAdapter(ordersListAdapter);

                    txtExpectedTime.setText("Expected Delivery : "+expectedTime);
                    txtOrderStatus.setText("Order Status : "+orderStatus1);
                    txtPaymentMode.setText("Payment Mode : "+paymentMode);
                    txtPaymentStatus.setText("Payment Status : "+paymentStatus);
                    txtTotal.setText("Total Amount : "+"\u20b9"+String.format("%.2f", Double.parseDouble(finalBill)));

                }


            }

            @Override
            public void onFailure(Call<OrderListResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(OrdersListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:


                Intent intent = new Intent(OrdersListActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        Intent intent = new Intent(OrdersListActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


}
