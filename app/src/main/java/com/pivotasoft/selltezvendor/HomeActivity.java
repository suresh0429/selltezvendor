package com.pivotasoft.selltezvendor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.pivotasoft.selltezvendor.Adapters.OrderIdAdapter;
import com.pivotasoft.selltezvendor.Apis.ApiPush;
import com.pivotasoft.selltezvendor.Apis.PushClient;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Helper.Converter;
import com.pivotasoft.selltezvendor.Interface.Dilogueinterface;
import com.pivotasoft.selltezvendor.Model.OrderIdItem;
import com.pivotasoft.selltezvendor.Reciever.ConnectivityReceiver;
import com.pivotasoft.selltezvendor.Response.AnaliticsResponse;
import com.pivotasoft.selltezvendor.Response.Data;
import com.pivotasoft.selltezvendor.Response.NotificationBody;
import com.pivotasoft.selltezvendor.Response.OrdersResponse;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;
import com.pivotasoft.selltezvendor.Storage.Utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.ABOUT_US_URL;
import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.FAQ_URL;
import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.PRIVACY_POLICY_URL;
import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.RETURN_POLICY_URL;
import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.TERMS_CONDITIONS_URL;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Dilogueinterface {

    int checkedItem = 0;
    String[] status;
    AlertDialog.Builder builder;
    boolean doubleBackToExitPressedOnce = false;
    ConnectivityReceiver connectivityReceiver;
    int color = Color.RED;
    AppController appController;

    OrderIdAdapter myOrderAdapter;
    String tokenValue, deviceId, storeId, storeName;
    boolean checkoutStatus;
    Dilogueinterface dilogueinterface;
    @BindView(R.id.orderidRecycler)
    RecyclerView orderidRecycler;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.radio0)
    RadioButton radio0;
    @BindView(R.id.radio1)
    RadioButton radio1;
    @BindView(R.id.radio2)
    RadioButton radio2;
    @BindView(R.id.daily_weekly_button_view)
    RadioGroup dailyWeeklyButtonView;
    @BindView(R.id.txtGrossEarnings)
    TextView txtGrossEarnings;
    @BindView(R.id.txtNetEarnings)
    TextView txtNetEarnings;
    @BindView(R.id.txtnoOrders)
    TextView txtnoOrders;
    @BindView(R.id.rbCurrent)
    RadioButton rbCurrent;
    @BindView(R.id.rbFinished)
    RadioButton rbFinished;
    @BindView(R.id.filter)
    RadioGroup filter;

    private PrefManager pref;
    Snackbar snackbar;
    int cartindex;
    String imagePic, loc_area, checnkVeriosn, android_version, keyFcm;
    ImageView nav_Image;
    String title = "";
    TextView nav_username, nav_useremail, txtShop;
    private ArrayList<OrderIdItem> orderIdItemsCurrent = new ArrayList<>();
    private ArrayList<OrderIdItem> orderIdItemsFinised = new ArrayList<>();
    //    private FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String, Object> firebaseDefaultMap;
    private static final String TAG = "HomeActivity";
    private static final String VERSION_CODE_KEY = "app_latest_version";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.selltezlogo);
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //LandScape mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        appController = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());
        dilogueinterface = (Dilogueinterface) this;

        // Method to manually check connection status
        boolean isConnected = appController.isConnection();
        //showSnack(isConnected);

        if (getIntent() != null) {
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);
        nav_username = (TextView) hView.findViewById(R.id.userName);
        nav_useremail = (TextView) hView.findViewById(R.id.userEmail);
        nav_Image = (ImageView) hView.findViewById(R.id.imageView);


        if (isConnected) {

            userData();


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("storeId", storeId);
                    intent.putExtra("storeName", storeName);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }


    }


    // user information
    private void userData() {
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        storeId = profile.get("id");
        storeName = profile.get("name");
        String email = profile.get("email");
        imagePic = profile.get("profilepic");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");
        keyFcm = profile.get("keyFcm");

        nav_useremail.setText(email);
        nav_username.setText(storeName);

        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {
            // loading album cover using Glide library
            Glide.with(getApplicationContext()).load(imagePic).into(nav_Image);
        } else {

            nav_Image.setImageResource(R.drawable.ic_user);
        }

        progressBar.setVisibility(View.VISIBLE);


        storeAnalyitics("1");
        dailyWeeklyButtonView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = dailyWeeklyButtonView.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);

                if (radioButton.getText().equals("Daily")) {
                    storeAnalyitics("1");
                } else if (radioButton.getText().equals("Weekly")) {
                    storeAnalyitics("7");
                } else {
                    storeAnalyitics("30");
                }


                // Toast.makeText(HomeActivity.this, radioButton.getText(), Toast.LENGTH_SHORT).show();
            }
        });

        // filter with data
        prepareOrderIdData("Current");
        filter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = filter.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton radioButton = (RadioButton) findViewById(selectedId);
                if (radioButton.getText().equals("Current")) {
                    prepareOrderIdData("Current");
                } else if (radioButton.getText().equals("Finished")) {
                    prepareOrderIdData("Finished");
                }
            }
        });


    }


    // cart count
    private void cartCount() {

        appController.cartCount(storeId, tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindex", "" + cartindex);
        invalidateOptionsMenu();


    }

    private void prepareOrderIdData(String filter) {
        progressBar.setVisibility(View.VISIBLE);

        Call<OrdersResponse> call = RetrofitClient.getInstance().getApi().ordersIdList(tokenValue, storeId);
        call.enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                if (response.isSuccessful()) {

                    progressBar.setVisibility(View.GONE);
                    OrdersResponse myOrdersResponse = response.body();

                    List<OrdersResponse.StoreordersdataBean> modulesBeanList = response.body().getStoreordersdata();

                    orderIdItemsCurrent.clear();
                    orderIdItemsFinised.clear();
                    for (OrdersResponse.StoreordersdataBean storeordersdataBean : modulesBeanList) {

                        if (storeordersdataBean.getOrderstatus().equalsIgnoreCase("Rejected") || storeordersdataBean.getOrderstatus().equalsIgnoreCase("Confirmed")) {
                            orderIdItemsFinised.add(new OrderIdItem(storeordersdataBean.getOrderid(), storeordersdataBean.getStoreid(), storeordersdataBean.getBookingdatetime(),
                                    storeordersdataBean.getBillamount(), storeordersdataBean.getDiscount(), storeordersdataBean.getDeliverycharges(), storeordersdataBean.getFinalbill(),
                                    storeordersdataBean.getInvoiceno(), storeordersdataBean.getPaymentmode(), storeordersdataBean.getPaymentsummary(), storeordersdataBean.getPaymentstatus(),
                                    storeordersdataBean.getOrderstatus(), storeordersdataBean.getExpectedtime(), storeordersdataBean.getBuyerid(), storeordersdataBean.getFullname(), storeordersdataBean.getMobile(),
                                    storeordersdataBean.getFcmtoken()));
                        } else {
                            orderIdItemsCurrent.add(new OrderIdItem(storeordersdataBean.getOrderid(), storeordersdataBean.getStoreid(), storeordersdataBean.getBookingdatetime(),
                                    storeordersdataBean.getBillamount(), storeordersdataBean.getDiscount(), storeordersdataBean.getDeliverycharges(), storeordersdataBean.getFinalbill(),
                                    storeordersdataBean.getInvoiceno(), storeordersdataBean.getPaymentmode(), storeordersdataBean.getPaymentsummary(), storeordersdataBean.getPaymentstatus(),
                                    storeordersdataBean.getOrderstatus(), storeordersdataBean.getExpectedtime(), storeordersdataBean.getBuyerid(), storeordersdataBean.getFullname(), storeordersdataBean.getMobile(),
                                    storeordersdataBean.getFcmtoken()));
                        }
                    }

                    if (filter.equalsIgnoreCase("Current")){

                        if (orderIdItemsCurrent.size() != 0){
                            txtError.setVisibility(View.GONE);
                        }else {
                            txtError.setVisibility(View.VISIBLE);
                            txtError.setText("No Orders Found !");
                        }

                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        layoutManager.setReverseLayout(true);
                        layoutManager.setStackFromEnd(true);
                        orderidRecycler.setItemAnimator(new DefaultItemAnimator());
                        orderidRecycler.setLayoutManager(layoutManager);
                        myOrderAdapter = new OrderIdAdapter(getApplicationContext(), orderIdItemsCurrent, dilogueinterface);
                        orderidRecycler.setAdapter(myOrderAdapter);
                        myOrderAdapter.notifyDataSetChanged();

                    }else {
                        if (orderIdItemsFinised.size() != 0){
                            txtError.setVisibility(View.GONE);
                        }
                        else {
                            progressBar.setVisibility(View.GONE);
                            txtError.setVisibility(View.VISIBLE);
                            txtError.setText("No Orders Found !");
                        }

                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        layoutManager.setReverseLayout(true);
                        layoutManager.setStackFromEnd(true);
                        orderidRecycler.setItemAnimator(new DefaultItemAnimator());
                        orderidRecycler.setLayoutManager(layoutManager);
                        myOrderAdapter = new OrderIdAdapter(getApplicationContext(), orderIdItemsFinised, dilogueinterface);
                        orderidRecycler.setAdapter(myOrderAdapter);
                        myOrderAdapter.notifyDataSetChanged();

                    }





                } else {
                    progressBar.setVisibility(View.GONE);
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Orders Found !");
                }
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void storeAnalyitics(String duration) {

        progressBar.setVisibility(View.VISIBLE);

        Call<AnaliticsResponse> call = RetrofitClient.getInstance().getApi().storeAnalitics(storeId, tokenValue, duration);
        call.enqueue(new Callback<AnaliticsResponse>() {
            @Override
            public void onResponse(Call<AnaliticsResponse> call, Response<AnaliticsResponse> response) {
                if (response.isSuccessful()) {

                    progressBar.setVisibility(View.GONE);
                    AnaliticsResponse myOrdersResponse = response.body();

                    List<AnaliticsResponse.AnalyticsdataBean> modulesBeanList = response.body().getAnalyticsdata();

                    txtGrossEarnings.setText("\u20b9" + modulesBeanList.get(0).getGrossearnings());
                    txtNetEarnings.setText("\u20b9" + modulesBeanList.get(0).getNetearnings());
                    txtnoOrders.setText(modulesBeanList.get(0).getNofoforders());


                } else {
                    progressBar.setVisibility(View.GONE);
                    txtGrossEarnings.setText("\u20b9" + "0.00");
                    txtNetEarnings.setText("\u20b9" + "0.00");
                    txtnoOrders.setText("0");

                }
            }

            @Override
            public void onFailure(Call<AnaliticsResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void onInvoiceClick(final OrderIdItem orderListBean) {

        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.diloge_invoice, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();


        TextView txtDismiss = dialogView.findViewById(R.id.txtDismiss);
        TextView txtInvoiceId = dialogView.findViewById(R.id.txtInvoiceId);
        TextView txtBillAmount = dialogView.findViewById(R.id.txtBillAmount);
        TextView txtDeliveryCharges = dialogView.findViewById(R.id.txtDeliveryCharges);
        TextView txtGrossSettleMent = dialogView.findViewById(R.id.txtGrossSettleMent);
        //TextView txtGatewayCharges =  dialogView.findViewById(R.id.txtGatewayCharges);
        TextView txtselltezCommision = dialogView.findViewById(R.id.txtselltezCommision);
        TextView txtGst = dialogView.findViewById(R.id.txtGst);
        TextView txtNetPayable = dialogView.findViewById(R.id.txtNetPayable);

        txtInvoiceId.setText("Invoiceno : " + orderListBean.getInvoiceno());
        txtBillAmount.setText("Bill Amount : " + String.format("%.2f", Double.parseDouble(orderListBean.getBillamount())));


        Double gatewayCharges = ((Double.parseDouble(orderListBean.getBillamount()) - (Double.parseDouble(orderListBean.getDiscount() + orderListBean.getDeliverycharges())))) * 0.03;
        txtDeliveryCharges.setText("Gateway charges(3%) : -" + String.format("%.2f", gatewayCharges));

        Double selltezCommision = (Double.parseDouble(orderListBean.getFinalbill()) - gatewayCharges) * 0.03;
        txtselltezCommision.setText("Selltez Commission(3%)  : - " + String.format("%.2f", selltezCommision));

        Double grossSettlement = Double.parseDouble(orderListBean.getBillamount()) - (selltezCommision + gatewayCharges);
        txtGrossSettleMent.setText("Gross Settlement : " + String.format("%.2f", grossSettlement));

        Double gst = grossSettlement * 0.01;
        txtGst.setText("TCS (GST 1%) : - " + String.format("%.2f", gst));

        Double netPayable = grossSettlement - gst;

        txtNetPayable.setText("Net Payable : " + "\u20b9" + String.format("%.2f", netPayable));

        txtDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


    }

    @Override
    public void onUpdateStatusClick(final OrderIdItem orderListBean) {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Update Order Status");

        if (orderListBean.getOrderstatus().equalsIgnoreCase("Accepted")) {
            // add a radio button list
            status = new String[]{"Delivered"};
        } else if (orderListBean.getOrderstatus().equalsIgnoreCase("Pending")) {
            // add a radio button list
            //status = new String[]{"Accepted", "Delivered"};
            status = new String[]{"Rejected", "Accepted"};
        }

        // add a radio button list
        builder.setSingleChoiceItems(status, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkedItem = which;
            }
        });
// add OK and Cancel buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("TAG", "onClick: " + status[checkedItem]);
                updateStatus(status[checkedItem], orderListBean.getOrderid());
                sendNotificationToPatner(status[checkedItem], orderListBean.getOrderid(), orderListBean.getFcmtoken(), orderListBean.getInvoiceno(), orderListBean.getBuyerid());
            }
        });
        builder.setNegativeButton("Cancel", null);
// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void updateStatus(String status, String orderid) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());

        progressBar.setVisibility(View.VISIBLE);
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateOrderStatus(status, orderid, currentDateandTime, tokenValue);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    ResponseBody myOrdersResponse = response.body();

                    Toasty.success(getApplicationContext(), "Status Changed Successfully", Toast.LENGTH_SHORT).show();

                    prepareOrderIdData("Current");

                } else {
                    progressBar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void Logout() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
           /* editor.putString(Utilities.KEY_STORE_ID,home.getStoreid());
            editor.putString(Utilities.KEY_STORE_NAME,home.getTitle());*/
        editor.apply();

        pref.clearSession();
        //Logout
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();

        userData();
        cartCount();
//        locationShowFirsttime(currentLocation.getLatitude(), currentLocation.getLongitude());

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        userData();
        cartCount();
        // locationShowFirsttime(currentLocation.getLatitude(), currentLocation.getLongitude());

    }

    @Override
    protected void onStart() {
        super.onStart();

        userData();
        cartCount();
//        locationShowFirsttime(currentLocation.getLatitude(), currentLocation.getLongitude());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds countries to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        final MenuItem alertMenuItem = menu.findItem(R.id.action_location);
        RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
        txtShop = (TextView) rootView.findViewById(R.id.txt_area);
        txtShop.setText(storeName);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(alertMenuItem);
            }
        });


        final MenuItem menuItem = menu.findItem(R.id.action_cart);

        menuItem.setIcon(Converter.convertLayoutToImage(HomeActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {

            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("storeId", storeId);
            intent.putExtra("storeName", storeName);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            return true;
        } else if (id == R.id.action_location) {

           /* if(cartindex != 0){
                ClearCart();
            }
            else {
                Intent intent = new Intent(getApplicationContext(), StroeLocationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
                                     *//*editor.putString(Utilities.KEY_STORE_ID,home.getStoreid());
                                    editor.putString(Utilities.KEY_STORE_NAME,home.getTitle());*//*
                editor.apply();
            }*/

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_account) {
            //My Account
            Intent intent = new Intent(HomeActivity.this, MyAccountActivity.class);
            intent.putExtra("Checkout", false);
            intent.putExtra("storeId", storeId);
            intent.putExtra("storeName", storeName);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.My_orders) {
            //My orders
            Intent intent = new Intent(HomeActivity.this, CatagoeryActvity.class);
            intent.putExtra("Checkout", false);
            intent.putExtra("storeId", storeId);
            intent.putExtra("storeName", storeName);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.action_notification) {

            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.nav_logout) {

            Logout();

        } else if (id == R.id.nav_issues) {

            Intent intent = new Intent(getApplicationContext(), IssuesListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.nav_coupons) {

            Intent intent = new Intent(getApplicationContext(), CouponsListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.nav_share) {

            String sAux = "Install Selltez application click below link \n" + "https://play.google.com/store/apps/details?id=com.pivotasoft.selltez";
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, sAux);
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        }   /* else if (id == R.id.nav_support) {


            Intent intent = new Intent(getApplicationContext(), SupportUsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }*/ else if (id == R.id.nav_privacy) {

            title = "Privacy & Policy";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", PRIVACY_POLICY_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_terms) {

            title = "Terms & Conditions";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", TERMS_CONDITIONS_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_return_policy) {

            title = "Return Policy";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", RETURN_POLICY_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_faq) {

            title = "FAQ's";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", FAQ_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_aboutus) {

            title = "About Us";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", ABOUT_US_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toasty.error(this, "Click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);


            }
        }
    }

    private void sendNotificationToPatner(String status, String orderid, String fcmtoken, String invoiceno, String buyerid) {

        String title = "Selltez";
        String content = "";
        if (status.equalsIgnoreCase("Accepted")) {
            content = "Hi Selltez Consumer! Your order with Invoice#" + invoiceno + " accepted by " + storeName + " . Your order will be delivered in time. Thank you.";

        } else if (status.equalsIgnoreCase("Rejected")) {
            content = "Hi Selltez Consumer! Your order with Invoice#" + invoiceno + " rejected by " + storeName + " . We will initiate refund process in case of online payment. Sorry for the inconvenience.";

        } else if (status.equalsIgnoreCase("Delivered")) {
            content = "Hi Selltez Consumer! Your order with Invoice#" + invoiceno + " from  " + storeName + " is out for delivery. Your order will be delivered in time. Thank you.";

        }

        String to = fcmtoken;
        Log.d(TAG, "sendNotificationToPatner: " + fcmtoken);

        Data data = new Data(title, orderid, content);
        NotificationBody body = new NotificationBody(data, "fTrQdsEwU0Q:APA91bF8fwH1hEFpL_MoyjKXxMUi0DwagtxsvnExytOlKYz6zSNYusy0yaqbCuZQhTpUJvIj_FFSOQfsokElcZHTvYhVqRyn2Bke1YGrenFindGO7h12wtS6OVu1SLtD8Aajn-OLa-cw");

        ApiPush apiService = PushClient.getClient().create(ApiPush.class);
        Call<ResponseBody> responseBodyCall = apiService.sendNotification(body);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "Successfully notification send by using retrofit.");
                }else {
                    Log.d(TAG, "Failed notification send by using retrofit.");
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });

        // save notification
        saveNotification(content, buyerid);

    }

    private void saveNotification(String content, String buyerid) {
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().saveNotification(buyerid, "1234567890", "0", content);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    ResponseBody myOrdersResponse = response.body();
                    Log.d(TAG, "onResponse: " + "success");

                } else {
                    progressBar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
