package com.pivotasoft.selltezvendor.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pivotasoft.selltezvendor.R;
import com.pivotasoft.selltezvendor.ProductListActivity;
import com.pivotasoft.selltezvendor.Response.SubCatResponse;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.IMAGE_CAT_URL;
import static com.pivotasoft.selltezvendor.Storage.Utilities.capitalize;

public class SubCatageoryAdapter extends RecyclerView.Adapter<SubCatageoryAdapter.MyViewHolder> {
    private Context mContext;
    private List<SubCatResponse.SubcategoriesdataBean> homeList;
    private String catId;
    private String storeId;
    private String storeName;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView thumbnail;
        public TextView txtName;
        public LinearLayout linearLayout;


        public MyViewHolder(View view) {
            super(view);

            thumbnail = (CircleImageView) view.findViewById(R.id.thumbnail);
            txtName = (TextView) view.findViewById(R.id.txtCatName);
            linearLayout = (LinearLayout) view.findViewById(R.id.parentLayout);


        }
    }

    public SubCatageoryAdapter(Context mContext, List<SubCatResponse.SubcategoriesdataBean> homekitchenList, String catId, String storeId, String storeName) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
        this.catId = catId;
        this.storeId = storeId;
        this.storeName = storeName;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subcat_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SubCatResponse.SubcategoriesdataBean home = homeList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(IMAGE_CAT_URL+home.getSubcatpic()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.thumbnail);

        holder.txtName.setText(capitalize(home.getSubcategoryname()));

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


 //             Activity activity = (Activity) mContext;
                Intent intent = new Intent(mContext, ProductListActivity.class);
                intent.putExtra("catId", home.getCategoryid());
                intent.putExtra("title", home.getSubcategoryname());
                intent.putExtra("subcatId", home.getSubcategoryid());
                intent.putExtra("storeId", storeId);
                intent.putExtra("storeName", storeName);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                // activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

               /* SharedPreferences preferences1 = mContext.getSharedPreferences("SORT", 0);
                SharedPreferences.Editor editor = preferences1.edit();
                editor.putString("catId", home.getCategory_id());
                editor.putString("title", home.getTitle());
                editor.putString("subcatId", home.getId());
                editor.putString("sort", "Price High to Low");
                editor.apply();*/


            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


}