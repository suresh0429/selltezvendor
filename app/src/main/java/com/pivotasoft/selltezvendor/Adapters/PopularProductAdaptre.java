package com.pivotasoft.selltezvendor.Adapters;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pivotasoft.selltezvendor.R;
import com.pivotasoft.selltezvendor.Response.ProductResponse;

import java.util.List;

public class PopularProductAdaptre extends RecyclerView.Adapter<PopularProductAdaptre.MyViewHolder> {
    private Context mContext;
    private List<ProductResponse.ProductsdataBean> homeList;
    // ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView txtPrice,txtName,txtDiscountPrice,txtDiscountTag,product_unit;
        //  public RatingBar ratingBar;
        public CardView linearLayout;
        public LinearLayout tagLayout;


        public MyViewHolder(View view) {
            super(view);

            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            txtPrice = (TextView) view.findViewById(R.id.txtproductPrice);
            product_unit = (TextView) view.findViewById(R.id.product_unit);
            txtName = (TextView) view.findViewById(R.id.txtproductName);
            txtDiscountPrice = (TextView) view.findViewById(R.id.txtDiscountPrice);
            linearLayout=(CardView)view.findViewById(R.id.parentLayout);
            tagLayout = (LinearLayout)view.findViewById(R.id.tagLayout);
            txtDiscountTag = (TextView)view.findViewById(R.id.txtDiscountTag);

        }
    }

    public PopularProductAdaptre(Context mContext, List<ProductResponse.ProductsdataBean> homekitchenList) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public PopularProductAdaptre.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.popular_card, parent, false);

        return new PopularProductAdaptre.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PopularProductAdaptre.MyViewHolder holder, final int position) {
        final ProductResponse.ProductsdataBean home = homeList.get(position);

       /* // loading album cover using Glide library
        Glide.with(mContext).load(PRODUCT_IMAGE_BASE_URL2+home.getImages().get(0)).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbnail);
        Log.d("ADAPTERIMASGE", "onBindViewHolder: "+PRODUCT_IMAGE_BASE_URL2+home.getImages().get(0));
        holder.txtName.setText(capitalize(home.getProduct_name()));
        holder.txtDiscountPrice.setText("\u20B9"+home.getSelling_price());

        holder.txtPrice.setText("\u20B9" + home.getMrp_price());
        holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtPrice.setTextColor(Color.RED);

        holder.product_unit.setText("" +home.getUnit_value()+home.getUnit_name());



        //  holder.ratingBar.setRating(Float.parseFloat(home.getRating()));
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId",home.getId());
                intent.putExtra("productName",home.getProduct_name());
                // intent.putExtra("module",home.getModule());
                // Log.e("MODULE",""+home.getModule());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                // activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }});
*/

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }



}
