package com.pivotasoft.selltezvendor.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pivotasoft.selltezvendor.ProductDetailsActivity;
import com.pivotasoft.selltezvendor.R;
import com.pivotasoft.selltezvendor.Response.ProductResponse;

import java.util.ArrayList;
import java.util.List;

import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.IMAGE_PRODUCT_URL;
import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.PRODUCT_IMAGE_BASE_URL2;
import static com.pivotasoft.selltezvendor.Storage.Utilities.capitalize;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> implements Filterable {
    private Context mContext;
    private List<ProductResponse.ProductsdataBean> homeList;
    private List<ProductResponse.ProductsdataBean> filterhomeList;
    private String storeId;
    private String storeName;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView txtPrice, txtName, txtDiscountPrice, txtDiscountTag, product_unit;
        //  public RatingBar ratingBar;
        public CardView linearLayout;
        public Button imgEdit;
        public LinearLayout tagLayout, shadowImageView;

        public MyViewHolder(View view) {
            super(view);

            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            txtPrice = (TextView) view.findViewById(R.id.txtproductPrice);
            product_unit = (TextView) view.findViewById(R.id.product_unit);
            txtName = (TextView) view.findViewById(R.id.txtproductName);
            txtDiscountPrice = (TextView) view.findViewById(R.id.txtDiscountPrice);
            linearLayout = (CardView) view.findViewById(R.id.parentLayout);
            tagLayout = (LinearLayout) view.findViewById(R.id.tagLayout);
            shadowImageView = (LinearLayout) view.findViewById(R.id.shadowImageView);
            txtDiscountTag = (TextView) view.findViewById(R.id.txtDiscountTag);
            imgEdit = (Button) view.findViewById(R.id.imgEdit);


        }
    }


    public SearchAdapter(Context mContext, List<ProductResponse.ProductsdataBean> homekitchenList, String storeId, String storeName) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
        this.filterhomeList = homekitchenList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.products_item_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductResponse.ProductsdataBean home = filterhomeList.get(position);

       /* if (home.getStatus().equalsIgnoreCase("2")){
            holder.shadowImageView.setVisibility(View.VISIBLE);
            holder.linearLayout.setClickable(false);
            holder.linearLayout.setEnabled(false);
        }else {
            holder.shadowImageView.setVisibility(View.GONE);
            holder.linearLayout.setClickable(true);
            holder.linearLayout.setEnabled(true);

        }*/

        // loading album cover using Glide library
        Glide.with(mContext).load(IMAGE_PRODUCT_URL + home.getProductpic()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbnail);
        Log.d("ADAPTERIMASGE", "onBindViewHolder: " + PRODUCT_IMAGE_BASE_URL2 + home.getProductpic());
        holder.txtName.setText(capitalize(home.getTitle()));
        holder.txtDiscountPrice.setText("\u20B9" + home.getFinalprice());

        holder.txtPrice.setText("\u20B9" + home.getUnitprice());
        holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtPrice.setTextColor(Color.RED);

        holder.product_unit.setText("" + home.getMeasureunits());


        //  holder.ratingBar.setRating(Float.parseFloat(home.getRating()));
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId", home.getProductid());
                intent.putExtra("productName", home.getTitle());
                intent.putExtra("price", home.getUnitprice());
                intent.putExtra("discount",home.getDiscount());
                intent.putExtra("finalPrice",home.getFinalprice());
                intent.putExtra("discription", home.getDescription());
                intent.putExtra("status", home.getStatus());
                intent.putExtra("weight", home.getMeasureunits());
                intent.putExtra("storeId", home.getStoreid());
                intent.putExtra("storeName", storeName);
                intent.putExtra("productImage", IMAGE_PRODUCT_URL + home.getProductpic());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);


            }
        });

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId", home.getProductid());
                intent.putExtra("productName", home.getTitle());
                intent.putExtra("price", home.getUnitprice());
                intent.putExtra("discount",home.getDiscount());
                intent.putExtra("finalPrice",home.getFinalprice());
                intent.putExtra("discription", home.getDescription());
                intent.putExtra("status", home.getStatus());
                intent.putExtra("weight", home.getMeasureunits());
                intent.putExtra("storeId", home.getStoreid());
                intent.putExtra("storeName", storeName);
                intent.putExtra("productImage", IMAGE_PRODUCT_URL + home.getProductpic());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return filterhomeList.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.isEmpty()) {
                    filterhomeList = homeList;
                } else {
                    List<ProductResponse.ProductsdataBean> filteredList = new ArrayList<>();
                    for (ProductResponse.ProductsdataBean name : homeList) {
                        if (name.getTitle().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(name);
                        }
                        filterhomeList = filteredList;
                    }

                }
                FilterResults results = new FilterResults();
                results.values = filterhomeList;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filterhomeList = (List<ProductResponse.ProductsdataBean>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
