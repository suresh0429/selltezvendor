package com.pivotasoft.selltezvendor.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;


import com.pivotasoft.selltezvendor.ProductDetailsActivity;
import com.pivotasoft.selltezvendor.R;
import com.pivotasoft.selltezvendor.Response.WishListDeleteresponse;
import com.pivotasoft.selltezvendor.Response.WishlistResponse;
import com.pivotasoft.selltezvendor.WishListActivity;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.pivotasoft.selltezvendor.Storage.Utilities.capitalize;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.MyViewHolder> {

    private Context mContext;
    private List<WishlistResponse.WishListBean> cartListBeanList;


    public WishListAdapter(Context mContext, List<WishlistResponse.WishListBean> cartListBeanList) {
        this.mContext = mContext;
        this.cartListBeanList = cartListBeanList;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtItemName, txtPrice, btnDecrement;
        public ImageView thumbnail;
        public LinearLayout parentLayout;


        public MyViewHolder(View view) {
            super(view);
            txtItemName = (TextView) view.findViewById(R.id.item_name);
            txtPrice = (TextView) view.findViewById(R.id.item_price);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            btnDecrement = (TextView) view.findViewById(R.id.remove_item);
            parentLayout = (LinearLayout) view.findViewById(R.id.parentLayout);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.wish_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final WishlistResponse.WishListBean cartListBean = cartListBeanList.get(position);

        holder.txtItemName.setText(capitalize(cartListBean.getProduct_name()));
        Log.e("PRO_NAME", "" + cartListBean.getProduct_name());

        if (cartListBean.getDis_price().equalsIgnoreCase("0") || cartListBean.getDis_price().equalsIgnoreCase("0.00")) {
            holder.txtPrice.setText(" \u20B9" + cartListBean.getInclTax());
        } else {
            holder.txtPrice.setText(" \u20B9" + cartListBean.getDis_price());
        }

        // loading album cover using Glide library
        Glide.with(mContext).load(cartListBean.getImage()).into(holder.thumbnail);

        holder.btnDecrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.alert_dialog);


                // set the custom dialog components - text, image and button
                TextView te = (TextView) dialog.findViewById(R.id.txtAlert);
                te.setText("Are You Sure Want to Delete ?");

                TextView yes = (TextView) dialog.findViewById(R.id.btnYes);
                TextView no = (TextView) dialog.findViewById(R.id.btnNo);

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final WishlistResponse.WishListBean cartListBean = cartListBeanList.get(position);

                        delete(view, cartListBean.getId());
                        cartListBeanList.remove(position);
                        notifyDataSetChanged();
                        Log.e("COUNTADDRESS", "" + cartListBeanList.size());

                        dialog.dismiss();


                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productName", cartListBean.getProduct_name());
                intent.putExtra("module", cartListBean.getCategory());
                Log.e("MODULE", "" + cartListBean.getCategory());


                if (cartListBean.getCategory().equalsIgnoreCase("mobiles")) {

                    intent.putExtra("productId", cartListBean.getProduct_id() + "&itemId=" + cartListBean.getItem_id());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                } else if (cartListBean.getCategory().equalsIgnoreCase("homekitchen")) {
                    String timeValue = "";
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("hh a");
                        Date lunchTime = dateFormat.parse("1 AM");
                        Date dinnerTime = dateFormat.parse("3 PM");

                        //Get current time
                        Date CurrentTime = dateFormat.parse(dateFormat.format(new Date()));
                        //Sample time
                        //  Date CurrentTime = dateFormat.parse("4 PM");

                        if (CurrentTime.after(lunchTime) && CurrentTime.before(dinnerTime)) {
                            //  Toast.makeText(mContext, "lunch", Toast.LENGTH_SHORT).show();
                            timeValue = "lunch";
                        } else {
                            timeValue = "dinner";
                            //  Toast.makeText(mContext, "dinner", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                   /* Intent intentKitchen = new Intent(mContext, HomeKitchenActivity.class);
                    intentKitchen.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentKitchen.putExtra("TIMEVALUE", timeValue);
                    mContext.startActivity(intentKitchen);*/
                } else {

                    intent.putExtra("productId", cartListBean.getProduct_id());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }


            }
        });
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return cartListBeanList.size();
    }


    private void delete(final View view, String id) {


        Call<WishListDeleteresponse> call = RetrofitClient.getInstance().getApi().deleteWishList(id);

        call.enqueue(new Callback<WishListDeleteresponse>() {
            @Override
            public void onResponse(Call<WishListDeleteresponse> call, retrofit2.Response<WishListDeleteresponse> response) {


                WishListDeleteresponse wishListDeleteresponse = response.body();

                if (response.isSuccessful()) {

                    if (wishListDeleteresponse != null && wishListDeleteresponse.getStatus().equalsIgnoreCase("Item Deleted Successfully From WishList")) {

                        Snackbar.make(view, wishListDeleteresponse.getMessage(), Snackbar.LENGTH_SHORT).show();

                                Intent intent = new Intent(mContext, WishListActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(intent);

                                }
                }
            }

            @Override
            public void onFailure(Call<WishListDeleteresponse> call, Throwable t) {

            }
        });
    }


}