package com.pivotasoft.selltezvendor.Adapters;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.pivotasoft.selltezvendor.AddCouponActivity;
import com.pivotasoft.selltezvendor.R;
import com.pivotasoft.selltezvendor.Response.BannersResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CouponsAdapter extends RecyclerView.Adapter<CouponsAdapter.MyViewHolder> {


    private Context mContext;
    private List<BannersResponse.CouponsdataBean> homeList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgEdit)
        ImageView imgEdit;
        @BindView(R.id.txtCode)
        TextView txtCode;
        @BindView(R.id.txtValid)
        TextView txtValid;
        @BindView(R.id.txtDescription)
        TextView txtDescription;
        @BindView(R.id.txtCondition)
        TextView txtCondition;
        @BindView(R.id.linearParent)
        LinearLayout linearParent;
        @BindView(R.id.cardView)
        CardView cardView;
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public CouponsAdapter(Context mContext, List<BannersResponse.CouponsdataBean> homekitchenList) {
        this.mContext = mContext;
        this.homeList = homekitchenList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.coupon_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final BannersResponse.CouponsdataBean home = homeList.get(position);

        holder.txtCode.setText( home.getCouponcode());
        holder.txtDescription.setText(home.getDescription());
        holder.txtValid.setText("Validity : " + home.getFromdate() + " - " + home.getTodate());

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent1 = new Intent(mContext, AddCouponActivity.class);
                intent1.putExtra("code", home.getCouponcode());
                intent1.putExtra("description", home.getDescription());
                intent1.putExtra("fromDate", home.getFromdate());
                intent1.putExtra("toDate", home.getTodate());
                intent1.putExtra("couponId", home.getCouponid());
                intent1.putExtra("discountPercentage", home.getDiscountpercent());
                intent1.putExtra("minTransaction", home.getMintransaction());
                intent1.putExtra("maxDiscount", home.getMaxdiscount());
                intent1.putExtra("actionBarTitle", "Update Coupon");
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent1);


            }

        });

        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(position);

        holder.linearParent.setBackgroundColor(color);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


}
