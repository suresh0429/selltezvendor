package com.pivotasoft.selltezvendor.Adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pivotasoft.selltezvendor.Interface.CartProductClickListener;
import com.pivotasoft.selltezvendor.Model.Product;
import com.pivotasoft.selltezvendor.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.IMAGE_PRODUCT_URL;
import static com.pivotasoft.selltezvendor.Storage.Utilities.capitalize;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {


    @BindView(R.id.thumbnail1)
    ImageView thumbnail1;
    @BindView(R.id.product_name)
    TextView productName;
    @BindView(R.id.product_Price)
    TextView productPrice;
    @BindView(R.id.final_product_Price)
    TextView finalProductPrice;

    @BindView(R.id.product_minus)
    TextView productMinus;
    @BindView(R.id.product_quantity)
    TextView productQuantity;
    @BindView(R.id.product_plus)
    TextView productPlus;
    @BindView(R.id.btnFavourite)
    TextView btnFavourite;
    @BindView(R.id.linearSaveItem)
    LinearLayout linearSaveItem;
    @BindView(R.id.btnRemove)
    TextView btnRemove;
    @BindView(R.id.linearremove)
    LinearLayout linearremove;
    private Context mContext;
    private List<Product> productsModelList;
    private CartProductClickListener productClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnail1)
        ImageView thumbnail1;
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.product_Price)
        TextView productPrice;
        @BindView(R.id.final_product_Price)
        TextView final_product_Price;
        @BindView(R.id.product_minus)
        TextView productMinus;
        @BindView(R.id.product_quantity)
        TextView productQuantity;
        @BindView(R.id.product_plus)
        TextView productPlus;
        @BindView(R.id.btnFavourite)
        TextView btnFavourite;
        @BindView(R.id.linearSaveItem)
        LinearLayout linearSaveItem;
        @BindView(R.id.btnRemove)
        TextView btnRemove;
        @BindView(R.id.linearremove)
        LinearLayout linearremove;
        @BindView(R.id.img_remove)
        ImageView imgRemove;
        @BindView(R.id.product_unit)
        TextView product_unit;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public CartAdapter(Context mContext, List<Product> productsModelList, CartProductClickListener productClickListener) {
        this.mContext = mContext;
        this.productsModelList = productsModelList;
        this.productClickListener = productClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_product, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Product productsModel = productsModelList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(IMAGE_PRODUCT_URL + productsModel.getProductpic()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbnail1);


        holder.productName.setText(capitalize(productsModel.getTitle()));
        holder.productQuantity.setText("" + productsModel.getQuantity());
        holder.product_unit.setText("" +productsModel.getMeasureunits());

        holder.productPrice.setText(mContext.getResources().getString(R.string.Rs) + productsModel.getFinalprice());
        /*holder.productPrice.setPaintFlags(holder.productPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.productPrice.setTextColor(Color.RED);*/

        holder.final_product_Price.setText("Item Total : "+mContext.getResources().getString(R.string.Rs) + productsModel.getItemtotal() );


        holder.productMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                productClickListener.onMinusClick(productsModel);
            }
        });

        holder.productPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                productClickListener.onPlusClick(productsModel);
            }
        });

//        holder.btnFavourite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                productClickListener.onWishListDialog(productsModel);
//            }
//        });

        holder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                productClickListener.onRemoveDialog(productsModel);
            }
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return productsModelList.size();
    }

}