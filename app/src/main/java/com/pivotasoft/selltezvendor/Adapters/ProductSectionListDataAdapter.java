package com.pivotasoft.selltezvendor.Adapters;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pivotasoft.selltezvendor.Model.SingleItemModel;
import com.pivotasoft.selltezvendor.ProductDetailsActivity;
import com.pivotasoft.selltezvendor.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.pivotasoft.selltezvendor.Apis.RetrofitClient.IMAGE_PRODUCT_URL;
import static com.pivotasoft.selltezvendor.Storage.Utilities.capitalize;

public class ProductSectionListDataAdapter extends RecyclerView.Adapter<ProductSectionListDataAdapter.SingleItemRowHolder> {


    private ArrayList<SingleItemModel> itemsList;
    private Context mContext;
    private String storeName;

    public ProductSectionListDataAdapter(Context context, ArrayList<SingleItemModel> itemsList, String storeName) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.storeName = storeName;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        final SingleItemModel singleItem = itemsList.get(i);

       /* if (singleItem.getStatus().equalsIgnoreCase("2")){
            holder.shadowImageView.setVisibility(View.VISIBLE);
            holder.parentLayout.setClickable(false);
            holder.parentLayout.setEnabled(false);
        }else {
            holder.shadowImageView.setVisibility(View.GONE);
            holder.parentLayout.setClickable(true);
            holder.parentLayout.setEnabled(true);

        }*/


        holder.product_unit.setText("" +singleItem.getMeasureunits());
        holder.txtproductPrice.setText("\u20B9" + singleItem.getUnitprice());
        holder.txtproductPrice.setPaintFlags(holder.txtproductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtproductPrice.setTextColor(Color.RED);

        holder.txtDiscountPrice.setText("\u20B9" + singleItem.getFinalprice());
        Glide.with(mContext).load(IMAGE_PRODUCT_URL+ singleItem.getProductpic()).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbnail);
        holder.txtproductName.setText(capitalize(singleItem.getTitle()));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId",singleItem.getProductid());
                intent.putExtra("productName",singleItem.getTitle());
                intent.putExtra("price",singleItem.getUnitprice());
                intent.putExtra("discount",singleItem.getDiscount());
                intent.putExtra("finalPrice",singleItem.getFinalprice());
                intent.putExtra("discription",singleItem.getDescription());
                intent.putExtra("status",singleItem.getStatus());
                intent.putExtra("weight",singleItem.getMeasureunits());
                intent.putExtra("storeId",singleItem.getStoreid());
                intent.putExtra("storeName",storeName);
                intent.putExtra("productImage",IMAGE_PRODUCT_URL+singleItem.getProductpic());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId",singleItem.getProductid());
                intent.putExtra("productName",singleItem.getTitle());
                intent.putExtra("price",singleItem.getUnitprice());
                intent.putExtra("discount",singleItem.getDiscount());
                intent.putExtra("finalPrice",singleItem.getFinalprice());
                intent.putExtra("discription",singleItem.getDescription());
                intent.putExtra("status",singleItem.getStatus());
                intent.putExtra("weight",singleItem.getMeasureunits());
                intent.putExtra("storeId",singleItem.getStoreid());
                intent.putExtra("storeName",storeName);
                intent.putExtra("productImage",IMAGE_PRODUCT_URL+singleItem.getProductpic());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.txtproductName)
        TextView txtproductName;
        @BindView(R.id.product_unit)
        TextView product_unit;
        @BindView(R.id.txtproductPrice)
        TextView txtproductPrice;
        @BindView(R.id.txtDiscountPrice)
        TextView txtDiscountPrice;
        @BindView(R.id.txtDiscountTag)
        TextView txtDiscountTag;
        @BindView(R.id.shadowImageView)
        LinearLayout shadowImageView;
        @BindView(R.id.tagLayout)
        LinearLayout tagLayout;
        @BindView(R.id.parentLayout)
        CardView parentLayout;
        @BindView(R.id.imgEdit)
        Button imgEdit;
        public SingleItemRowHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

           /* this.tvTitle = (TextView) view.findViewById(R.id.txtproductName);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();

                }
            });*/


        }

    }

}