package com.pivotasoft.selltezvendor;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCouponActivity extends AppCompatActivity {
    @BindView(R.id.etCouponCode)
    TextInputEditText etCouponCode;
    @BindView(R.id.etValidFrom)
    TextInputEditText etValidFrom;
    @BindView(R.id.etValidto)
    TextInputEditText etValidto;
    @BindView(R.id.etdescription)
    TextInputEditText etdescription;
    @BindView(R.id.etMinTransaction)
    TextInputEditText etMinTransaction;
    @BindView(R.id.etDiscountPercentage)
    TextInputEditText etDiscountPercentage;
    @BindView(R.id.etMaxDiscount)
    TextInputEditText etMaxDiscount;
    @BindView(R.id.btnAdd)
    Button btnAdd;

    int mYear, mMonth, mDay;
    String selectedDate, userId, tokenValue, code, description, fromDate, toDate, couponId, discountPercentage, minTransaction, maxDiscount, actionBarTitle;
    long selecdMinDat;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private SimpleDateFormat dateFormatter;
    private PrefManager pref;
    AppController appController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_coupon);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        appController = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");

        if (getIntent() != null) {
            code = getIntent().getStringExtra("code");
            description = getIntent().getStringExtra("description");
            fromDate = getIntent().getStringExtra("fromDate");
            toDate = getIntent().getStringExtra("toDate");
            couponId = getIntent().getStringExtra("couponId");
            discountPercentage = getIntent().getStringExtra("discountPercentage");
            minTransaction = getIntent().getStringExtra("minTransaction");
            maxDiscount = getIntent().getStringExtra("maxDiscount");
            actionBarTitle = getIntent().getStringExtra("actionBarTitle");

            etCouponCode.setText(code);
            etdescription.setText(description);
            etMaxDiscount.setText(maxDiscount);
            etMinTransaction.setText(minTransaction);
            etDiscountPercentage.setText(discountPercentage);
            etValidFrom.setText(fromDate);
            etValidto.setText(toDate);

            getSupportActionBar().setTitle(actionBarTitle);

            if (actionBarTitle.equalsIgnoreCase("Add Coupon")) {

                btnAdd.setText("Add");

            } else {
                btnAdd.setText("Update");
            }
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.etValidFrom, R.id.etValidto, R.id.btnAdd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etValidFrom:

                etValidto.setText(null);
                etValidto.setClickable(false);

                // date of birth picker
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                c.setTimeInMillis(System.currentTimeMillis() - 1000);

                DatePickerDialog datePickerDialog = new DatePickerDialog(AddCouponActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                                Calendar newDate = Calendar.getInstance();
                                newDate.set(year, month, dayOfMonth);

                                etValidFrom.setText(dateFormatter.format(newDate.getTime()));
                                selectedDate = dateFormatter.format(newDate.getTime());
                                //dateFormat.format(c.getTime());
                            }
                        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();


                break;
            case R.id.etValidto:

                if (!etValidFrom.getText().toString().isEmpty()) {
                    try {
                        Date d = dateFormatter.parse(selectedDate);
                        selecdMinDat = d.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    Calendar calendar1 = Calendar.getInstance(Locale.getDefault());
                    DatePickerDialog datePickerDialog1 = new DatePickerDialog(AddCouponActivity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                    //todo
                                    Calendar newDate = Calendar.getInstance();
                                    newDate.set(year, month, dayOfMonth);
                                    etValidto.setText(dateFormatter.format(newDate.getTime()));

                                }
                            }, calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog1.getDatePicker().setMinDate(selecdMinDat + 1000);
                    datePickerDialog1.show();
                } else {
                    Toasty.error(getApplicationContext(), "Please Select From Date", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnAdd:
                if (appController.isConnection()) {
                    progressBar.setVisibility(View.VISIBLE);
                    String couponCode = etCouponCode.getText().toString();
                    String minimumTransaction = etMinTransaction.getText().toString();
                    String discountPrice = etDiscountPercentage.getText().toString();
                    String maxDiscount = etMaxDiscount.getText().toString();
                    String from = etValidFrom.getText().toString();
                    String to = etValidto.getText().toString();
                    String description = etdescription.getText().toString();

                    if (!couponCode.isEmpty() || !minimumTransaction.isEmpty() || !discountPrice.isEmpty() || !maxDiscount.isEmpty() || !from.isEmpty() || !to.isEmpty() || !description.isEmpty()) {

                        if (actionBarTitle.equalsIgnoreCase("Add Coupon")) {

                            Call<ResponseBody> call = RetrofitClient.getInstance().getApi().newCoupon(tokenValue, userId, couponCode, description, minimumTransaction, discountPrice, maxDiscount, from, to);
                            call.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    ResponseBody bannersResponse = response.body();
                                    progressBar.setVisibility(View.GONE);
                                    if (response.isSuccessful()) {
                                        Intent intent = new Intent(getApplicationContext(), CouponsListActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        Toasty.success(getApplicationContext(), "Coupon Added Successfully.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toasty.error(getApplicationContext(), "Error On Server", Toast.LENGTH_SHORT).show();

                                    }


                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            Call<ResponseBody> call1 = RetrofitClient.getInstance().getApi().modifyCoupon(tokenValue, userId, couponId, couponCode, description, minimumTransaction, discountPrice, maxDiscount, from, to);
                            call1.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    ResponseBody bannersResponse = response.body();
                                    progressBar.setVisibility(View.GONE);
                                    if (response.isSuccessful()) {
                                        Intent intent = new Intent(getApplicationContext(), CouponsListActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        Toasty.success(getApplicationContext(), "Coupon Update Successfully.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toasty.error(getApplicationContext(), "Error On Server", Toast.LENGTH_SHORT).show();

                                    }


                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        }


                    } else {
                        Toasty.error(getApplicationContext(), "Enter All Fields!", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toasty.error(getApplicationContext(), "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
