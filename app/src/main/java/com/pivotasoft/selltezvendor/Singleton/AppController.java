package com.pivotasoft.selltezvendor.Singleton;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Reciever.ConnectivityReceiver;
import com.pivotasoft.selltezvendor.Response.CartResponse;
import com.pivotasoft.selltezvendor.Storage.PrefManager;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();


    private static AppController mInstance;

    private PrefManager pref;
    private static final String PREFS_NAME = "CARTCOUNT";




    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
       /* HashMap<String, String> profile = pref.getUserDetails();
        String userId=profile.get("id");*/

       // appEnvironment = AppEnvironment.SANDBOX;




    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }





    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    // check internet connection
    public boolean isConnection(){

        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();

        return networkInfo !=null && networkInfo.isConnected();
    }





    public void cartCount(final String userId,final String tokenvalue) {

        Call<CartResponse> call = RetrofitClient.getInstance().getApi().getCart(tokenvalue, userId);

        call.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {

                if (response.isSuccessful()){

                    CartResponse cartCountResponse = response.body();


                    int cartindex =cartCountResponse.getCartlistdata().size();
                    Log.e("CART_INDEX",""+cartindex);




                    SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("itemCount", cartindex);
                    editor.apply();




                }else{

                    SharedPreferences preferences =getSharedPreferences(PREFS_NAME,0);
                    SharedPreferences.Editor editor =preferences.edit();
                    editor.putInt("itemCount",0);
                    editor.apply();
                }


            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {
                SharedPreferences preferences =getSharedPreferences(PREFS_NAME,0);
                SharedPreferences.Editor editor =preferences.edit();
                editor.putInt("itemCount",0);
                editor.apply();
            }
        });

    }







}
