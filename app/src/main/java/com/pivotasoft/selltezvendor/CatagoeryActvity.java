package com.pivotasoft.selltezvendor;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.demono.AutoScrollViewPager;
import com.pivotasoft.selltezvendor.Adapters.AutoViewPager;
import com.pivotasoft.selltezvendor.Adapters.HomeAdapter;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Response.BannersResponse;
import com.pivotasoft.selltezvendor.Response.CategoriesResponse;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatagoeryActvity extends AppCompatActivity  {
    int checkedItem = 0;

    int color = Color.RED;
    AppController appController;
    private PrefManager pref;
    @BindView(R.id.homeRecycler)
    RecyclerView homeRecycler;
    @BindView(R.id.popularRecycler)
    RecyclerView popularRecycler;
    @BindView(R.id.viewPager)
    AutoScrollViewPager viewPager;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.emptyBanner)
    LinearLayout emptyBanner;
    String userId, tokenValue, deviceId,storeId,storeName;
    boolean checkoutStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_id_actvity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Categories");
        appController = (AppController) getApplication();


        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");

        }

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");



        // search Layout
        TextView searchLayout = (TextView) findViewById(R.id.searchLayout);
        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CatagoeryActvity.this, SearchActivity.class);
                intent.putExtra("storeId",userId);
                intent.putExtra("storeName",storeName);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        homeRecycler.setLayoutManager(mLayoutManager);
        homeRecycler.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        homeRecycler.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        popularRecycler.setLayoutManager(mLayoutManager1);
        popularRecycler.setItemAnimator(new DefaultItemAnimator());




        if (appController.isConnection()) {

            progressBar.setVisibility(View.VISIBLE);
            prepareHomeBannersData(userId);
            prepareHomeData(tokenValue);

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }
    }


    // home banners
    public void prepareHomeBannersData(String storeId) {

        Call<BannersResponse> call = RetrofitClient.getInstance().getApi().storeCoupens(tokenValue, storeId);
        call.enqueue(new Callback<BannersResponse>() {
            @Override
            public void onResponse(Call<BannersResponse> call, Response<BannersResponse> response) {
                BannersResponse bannersResponse = response.body();
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    assert response.body() != null;
                    final List<BannersResponse.CouponsdataBean> homeBannersBeanList = response.body().getCouponsdata();

                    if (homeBannersBeanList.size() != 0) {
                        AutoViewPager mAdapter = new AutoViewPager(homeBannersBeanList, CatagoeryActvity.this);
                        viewPager.setAdapter(mAdapter);
                        // optional start auto scroll
                        viewPager.startAutoScroll();
                        emptyBanner.setVisibility(View.GONE);
                    } else {
                        emptyBanner.setVisibility(View.VISIBLE);
                        viewPager.setVisibility(View.GONE);

                    }


                }


            }

            @Override
            public void onFailure(Call<BannersResponse> call, Throwable t) {
                Snackbar.make(homeRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

                progressBar.setVisibility(View.GONE);
            }
        });

    }

    // home modules
    private void prepareHomeData(String tokenValue) {


        Call<CategoriesResponse> call = RetrofitClient.getInstance().getApi().categories(tokenValue);
        call.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                CategoriesResponse addressResponse = response.body();
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    // modules
                    final List<CategoriesResponse.CategoriesdataBean> modulesBeanList = response.body().getCategoriesdata();


                    final HomeAdapter adapter = new HomeAdapter(getApplicationContext(), modulesBeanList, storeId,storeName);
                    homeRecycler.setAdapter(adapter);

                } else {
                    Toasty.success(getApplicationContext(), "No data Found", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                Snackbar.make(homeRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                Intent intent = new Intent(CatagoeryActvity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout", checkoutStatus);
                intent.putExtra("storeId", storeId);
                intent.putExtra("storeName", storeName);
                startActivity(intent);

                break;


        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        Intent intent = new Intent(CatagoeryActvity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("Checkout", checkoutStatus);
        intent.putExtra("storeId", storeId);
        intent.putExtra("storeName", storeName);
        startActivity(intent);

    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
