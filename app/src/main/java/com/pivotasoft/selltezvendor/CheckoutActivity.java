package com.pivotasoft.selltezvendor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Response.CouponResponse;
import com.pivotasoft.selltezvendor.Response.CreateOrderResponse;
import com.pivotasoft.selltezvendor.Response.PostOrderResponse;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;
import com.pivotasoft.selltezvendor.Storage.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {
    public static final String TAG = "CheckoutActivity : ";
    @BindView(R.id.txtShippingAddress)
    TextView txtShippingAddress;
    @BindView(R.id.txtChangeAddress)
    TextView txtChangeAddress;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.couponImage)
    ImageView couponImage;
    @BindView(R.id.etCoupon)
    EditText etCoupon;
    @BindView(R.id.txtApply)
    TextView txtApply;
    @BindView(R.id.relativeCoupon)
    RelativeLayout relativeCoupon;
    @BindView(R.id.txtCart)
    TextView txtCart;
    @BindView(R.id.txtCartTotal)
    TextView txtCartTotal;
    @BindView(R.id.txtCoupon)
    TextView txtCoupon;
    @BindView(R.id.txtCouponPrice)
    TextView txtCouponPrice;
    @BindView(R.id.txtDelivery)
    TextView txtDelivery;
    @BindView(R.id.txtDeliveryPrice)
    TextView txtDeliveryPrice;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.txtTotalPrice)
    TextView txtTotalPrice;
    @BindView(R.id.txtPaymentMode)
    TextView txtPaymentMode;
    @BindView(R.id.btnOrder)
    Button btnOrder;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;
    @BindView(R.id.txtExpectedDate)
    TextView txtExpectedDate;
    @BindView(R.id.rbCod)
    RadioButton rbCod;
    @BindView(R.id.rbOnline)
    RadioButton rbOnline;
    private ProgressDialog pDialog;
    private PrefManager pref;
    String userId, addressid, couponId="0", email, tokenValue, deviceId, loc_area, loc_pincode, shippingCharges,
            city, username, state, addressline1, area, storeId,storeName,bookingDateAndTime,expectedtime;
    int grandTotal;
    boolean cartStatus;
    double deliveryCharges, handlingCharges, finalTotal,discount;
    String txnId,paymentmode="";
    private AppCompatActivity activity;
    AppController appController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Checkout");

        appController = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        email = profile.get("email");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        // location Prefences
        SharedPreferences locationPref = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        boolean locationPrefBoolean = locationPref.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
        loc_area = locationPref.getString(Utilities.KEY_AREA, "");
        loc_pincode = locationPref.getString(Utilities.KEY_PINCODE, "");
        shippingCharges = locationPref.getString(Utilities.KEY_SHIPPINGCHARGES, "");


        // transaction id
        txnId = System.currentTimeMillis() + "";

        if (appController.isConnection()) {

            if (getIntent().getExtras() != null) {
                addressid = getIntent().getStringExtra("addressId");
                city = getIntent().getStringExtra("city");
                username = getIntent().getStringExtra("username");
                state = getIntent().getStringExtra("state");
                addressline1 = getIntent().getStringExtra("addressline1");
                area = getIntent().getStringExtra("area");
                grandTotal = getIntent().getIntExtra("grandTotal", 0);
                cartStatus = getIntent().getBooleanExtra("Checkout", false);
                storeId = getIntent().getStringExtra("storeId");
                storeName = getIntent().getStringExtra("storeName");

                txtUserName.setText(username);
                txtAddress.setText(addressline1 + "," + area + "," + city + "," + state);

                txtChangeAddress.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CheckoutActivity.this, AddressListActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("Checkout", cartStatus);
                        intent.putExtra("grandTotal", grandTotal);
                        intent.putExtra("storeId", storeId);
                        startActivity(intent);
                    }
                });


                txtCartTotal.setText("\u20B9" + grandTotal);

                // deliver charges

                if (grandTotal > 1000 && grandTotal < 2000) {
                    deliveryCharges = 40;
                    Log.d(TAG, "onCreate: " + deliveryCharges);

                } else if (grandTotal < 1000) {
                    deliveryCharges = 60;
                } else {
                    deliveryCharges = 0.00;

                }

                double amount = Double.parseDouble(String.valueOf(grandTotal));
                handlingCharges = (amount / 100.0f) * 2;

                // delivery charges with handling Charges
                deliveryCharges = deliveryCharges + handlingCharges;

                // final Total with deliveryCharges
                finalTotal = grandTotal + deliveryCharges;

                txtDeliveryPrice.setText("\u20B9" + String.format("%.2f", deliveryCharges));
                txtTotalPrice.setText("\u20B9" + String.format("%.2f", finalTotal));

                txtApply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!etCoupon.getText().toString().isEmpty()) {
                            CouponsData(etCoupon.getText().toString());
                            keypadHide(CheckoutActivity.this);
                        } else {
                            Toasty.normal(getApplicationContext(), "Enter Coupon Code", Toast.LENGTH_SHORT).show();
                        }

                    }
                });


                rbCod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            paymentmode = "Cod";
                        }
                    }
                });

                rbOnline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            paymentmode = "Online";
                        }
                    }
                });

                btnOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (paymentmode.equalsIgnoreCase("")){
                            Toasty.normal(getApplicationContext(),"Please Select Payment Mode.",Toast.LENGTH_SHORT).show();
                        }else {
                             btnOrder.setEnabled(false);
                             btnOrder.setClickable(false);
                             btnOrder.setAlpha(0.5f);

                            postOrder();

                        }
                    }
                });

                expectedDelivery();
            }


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }

    }


    private void expectedDelivery() {

        try {
            SimpleDateFormat sdfFull = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault());
            SimpleDateFormat sdfTommorrow = new SimpleDateFormat("yyyy-MM-dd 11:00 a", Locale.getDefault());

            SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a", Locale.US);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            Date StartTime = timeFormat.parse("07:00 AM");
            Date EndTime = timeFormat.parse("04:00 AM");

            //Get current time
            Date CurrentTime = timeFormat.parse(timeFormat.format(new Date()));
            // Get Current Date
            Date CurrentDate = dateFormat.parse(dateFormat.format(new Date()));

            bookingDateAndTime = sdfFull.format(new Date());


            if (CurrentTime.after(StartTime) && CurrentTime.before(EndTime)) {

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(CurrentTime);
                calendar.add(Calendar.HOUR, 3);
                String Datetime = timeFormat.format(calendar.getTime());
                String stringCurrentDate = dateFormat.format(CurrentDate);

                String finalCurrentDate = sdf.format(sdf.parse(stringCurrentDate+" "+Datetime));
               /* Log.d(TAG, "expectedDelivery: "+finalCurrentDate);
                Log.d(TAG, "expectedDelivery: "+sdfFull.format(sdf.parse(finalCurrentDate)));*/
                expectedtime = sdfFull.format(sdf.parse(finalCurrentDate));
                txtExpectedDate.setText(""+finalCurrentDate);
            } else {

                //Get Tommorrow Date with time
                Date tommorrow = new Date(CurrentDate.getTime() + (1000 * 60 * 60 * 24));
                String tomorowDateTime = sdfTommorrow.format(tommorrow);
                expectedtime = sdfFull.format(sdf.parse(tomorowDateTime));
                txtExpectedDate.setText(""+tomorowDateTime);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void CouponsData(String couponCode) {
        progressBar.setVisibility(View.VISIBLE);
        Call<CouponResponse> call = RetrofitClient.getInstance().getApi().couponsData(tokenValue, storeId, couponCode, userId);
        call.enqueue(new Callback<CouponResponse>() {
            @Override
            public void onResponse(Call<CouponResponse> call, Response<CouponResponse> response) {

                progressBar.setVisibility(View.GONE);

                Log.d(TAG, "onResponse: "+response.errorBody());

                if (response.isSuccessful()) {
                    CouponResponse cartResponse = response.body();
                    final List<CouponResponse.CouponsdataBean> addressBeans = cartResponse.getCouponsdata();

                    if (grandTotal >= Integer.parseInt(addressBeans.get(0).getMintransaction())) {

                        discount = Double.parseDouble(addressBeans.get(0).getMaxdiscount());
                        finalTotal = finalTotal - Double.parseDouble(addressBeans.get(0).getMaxdiscount());
                        couponId = addressBeans.get(0).getCouponid();
                        txtCoupon.setVisibility(View.VISIBLE);
                        txtCouponPrice.setVisibility(View.VISIBLE);
                        txtCouponPrice.setText("- " + "\u20B9" + String.format("%.2f", Double.parseDouble(addressBeans.get(0).getMaxdiscount())));
                        txtTotalPrice.setText("\u20B9" + String.format("%.2f", finalTotal));

                        txtApply.setText("Remove");
                        etCoupon.setFocusable(false);
                        if (txtApply.getText().toString().equals("Remove")) {
                            txtApply.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    txtApply.setText("Apply");
                                    etCoupon.setFocusable(true);
                                    etCoupon.setFocusableInTouchMode(true);
                                    etCoupon.setText("");
                                    txtCoupon.setVisibility(View.GONE);
                                    txtCouponPrice.setVisibility(View.GONE);
                                    couponId ="0";
                                    finalTotal = finalTotal + Double.parseDouble(addressBeans.get(0).getMaxdiscount());
                                    txtTotalPrice.setText("\u20B9" + String.format("%.2f", finalTotal));

                                    discount = 0.00;
                                }
                            });

                        }

                    } else {
                        couponId ="0";
                        discount = 0.00;
                        Toasty.normal(getApplicationContext(), "Minimum Transaction amount Should be " + addressBeans.get(0).getMintransaction(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    couponId ="0";
                    discount = 0.00;

                    if (response.body() != null) {
                        CouponResponse items = response.body();
                        //Log.d(TAG, "onResponse: "+items.getMessage());
                    } else if (response.errorBody() != null) {
                        try {
                            String errors = response.errorBody().string();
                            try {
                                JSONObject obj = new JSONObject(errors.toString());

                               // Log.d(TAG, "onResponse: "+obj.getString("message"));
                                Toasty.normal(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                }

            }

            @Override
            public void onFailure(Call<CouponResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

            }
        });

    }

    // order post
    private void postOrder() {

        progressBar.setVisibility(View.VISIBLE);
        Call<PostOrderResponse> call = RetrofitClient.getInstance().getApi().newOrder(tokenValue,storeId, userId, bookingDateAndTime, String.valueOf(grandTotal),couponId, String.valueOf(discount), String.valueOf(deliveryCharges), String.valueOf(finalTotal),addressid);
        call.enqueue(new Callback<PostOrderResponse>() {
            @Override
            public void onResponse(Call<PostOrderResponse> call, Response<PostOrderResponse> response) {

                progressBar.setVisibility(View.GONE);
                final PostOrderResponse invoicePostResponse = response.body();

                if (response.isSuccessful()) {

                    btnOrder.setEnabled(true);
                    btnOrder.setClickable(true);
                    btnOrder.setAlpha(0.9f);

                    if (paymentmode.equalsIgnoreCase("Cod")){
                        processOrder(invoicePostResponse.getDetails().getOrderid());
                    }else {

                        Intent intent = new Intent(CheckoutActivity.this,RazorPayActivity.class);
                        intent.putExtra("orderId",invoicePostResponse.getDetails().getOrderid());
                        intent.putExtra("Checkout", cartStatus);
                        intent.putExtra("storeId", storeId);
                        intent.putExtra("storeName", storeName);
                        intent.putExtra("finalBill", finalTotal);
                        intent.putExtra("couponId", couponId);
                        intent.putExtra("expectedtime", expectedtime);
                        startActivity(intent);
                    }



                }else {

                }
            }

            @Override
            public void onFailure(Call<PostOrderResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    private void processOrder(final int orderid){

        progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Call<CreateOrderResponse> call = RetrofitClient.getInstance().getApi().processOrder(tokenValue, userId, String.valueOf(orderid),couponId, paymentmode,"Cod",expectedtime);
                call.enqueue(new Callback<CreateOrderResponse>() {
                    @Override
                    public void onResponse(Call<CreateOrderResponse> call, Response<CreateOrderResponse> response) {

                        progressBar.setVisibility(View.GONE);
                        final CreateOrderResponse invoicePostResponse = response.body();

                        if (response.isSuccessful()) {

                            btnOrder.setEnabled(true);
                            btnOrder.setClickable(true);
                            btnOrder.setAlpha(0.9f);

                            showCustomDialog(invoicePostResponse.getDetails().getInvoiceno(), invoicePostResponse.getDetails().getMessage());

                            // cart update
                            appController.cartCount(userId, tokenValue);


                        }else {

                        }
                    }

                    @Override
                    public void onFailure(Call<CreateOrderResponse> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        },1000);

    }

    private void showCustomDialog(final String order_id, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        TextView txtmessage = (TextView) dialogView.findViewById(R.id.txtMessage);
        txtmessage.setText(message);


        Button buttonOk = (Button) dialogView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent order_detail = new Intent(CheckoutActivity.this, CatagoeryActvity.class);
               // order_detail.putExtra("Order_ID", order_id);
                order_detail.putExtra("Checkout", cartStatus);
                order_detail.putExtra("storeId", storeId);
                order_detail.putExtra("storeName", storeName);
                order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(order_detail);
                finish();
            }
        });

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }


    private double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }

    public static void keypadHide(Activity context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(Objects.requireNonNull(context.getCurrentFocus()).getWindowToken(), 0);
        } catch (Exception ignored) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
