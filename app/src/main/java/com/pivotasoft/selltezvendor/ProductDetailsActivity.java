package com.pivotasoft.selltezvendor;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pivotasoft.selltezvendor.Storage.Utilities.capitalize;

public class ProductDetailsActivity extends AppCompatActivity {


    AppController appController;
    @BindView(R.id.etProductname)
    TextInputEditText etProductname;
    @BindView(R.id.etUnitPrice)
    TextInputEditText etUnitPrice;
    @BindView(R.id.etUnit)
    TextInputEditText etUnit;
    @BindView(R.id.etDiscount)
    TextInputEditText etDiscount;
    @BindView(R.id.etAvailability)
    TextInputEditText etAvailability;
    @BindView(R.id.etdescription)
    TextInputEditText etdescription;
    @BindView(R.id.btnAddtocart)
    Button btnAddtocart;
    @BindView(R.id.progressBarMain)
    ProgressBar progressBarMain;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;
    @BindView(R.id.etFinalPrice)
    TextInputEditText etFinalPrice;
    private PrefManager pref;
    String id, title, discount, price, finalPrice,discription, status, productImage, weight, storeId, deviceId, tokenValue, currentDateandTime, storeName;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        currentDateandTime = sdf.format(new Date());
        Log.d("TAG", "onClick: " + currentDateandTime);

        if (getIntent().getExtras() != null) {
            id = getIntent().getStringExtra("productId");
            title = getIntent().getStringExtra("productName");
            price = getIntent().getStringExtra("price");
            discount = getIntent().getStringExtra("discount");
            finalPrice = getIntent().getStringExtra("finalPrice");
            discription = getIntent().getStringExtra("discription");
            status = getIntent().getStringExtra("status");
            weight = getIntent().getStringExtra("weight");
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
            productImage = getIntent().getStringExtra("productImage");
            Log.d("TAG", "loadOncreateData: " + productImage);
        }
        getSupportActionBar().setTitle(capitalize(title));

        appController = (AppController) getApplicationContext();

        pref = new PrefManager(getApplicationContext());

        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        storeId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        etProductname.setText(title);
        etUnit.setText(weight);
        etUnitPrice.setText(price);
        etDiscount.setText(discount);
        etFinalPrice.setText(finalPrice);
        if (status.equalsIgnoreCase("1")) {
            etAvailability.setText("Available");
        } else {
            etAvailability.setText("Out of Stock");
        }

        etdescription.setText(discription);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;


        }
        return super.onOptionsItemSelected(item);


    }


    @OnClick({R.id.etAvailability, R.id.btnAddtocart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etAvailability:
                // setup the alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(ProductDetailsActivity.this);
                builder.setTitle("Choose One");
// add a list
                final String[] animals = {"Available", "Out of Stock"};
                builder.setItems(animals, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: // horse
                                etAvailability.setText("Available");
                            case 1: // cow
                                etAvailability.setText("Out of Stock");
                        }
                    }
                });
// create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();

                break;
            case R.id.btnAddtocart:
                if (appController.isConnection()) {

                    String productTitle = etProductname.getText().toString();
                    String unitPrice = etUnitPrice.getText().toString();
                    String unitMeasurment = etUnit.getText().toString();
                    String discount = etDiscount.getText().toString();
                    String finalPrice = etFinalPrice.getText().toString();
                    String description = etdescription.getText().toString();
                    String avalibulity = etAvailability.getText().toString();
                    if (avalibulity.equalsIgnoreCase("Available")) {
                        status = "1";
                    } else {
                        status = "2";
                    }


                    progressBarMain.setVisibility(View.VISIBLE);
                    Call<ResponseBody> call = RetrofitClient.getInstance().getApi().modifyProduct(tokenValue, storeId, id, productTitle, unitPrice, unitMeasurment, discount, finalPrice, description, status);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                progressBarMain.setVisibility(View.GONE);
                                ResponseBody myOrdersResponse = response.body();

                                Toasty.success(getApplicationContext(), "Product Update Successfully.", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ProductDetailsActivity.this,HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            } else {
                                progressBarMain.setVisibility(View.GONE);

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            progressBarMain.setVisibility(View.GONE);
                            Toasty.normal(ProductDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });


                } else {

                    Toasty.error(ProductDetailsActivity.this, "No Internet Connection!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
