package com.pivotasoft.selltezvendor.Interface;

import com.pivotasoft.selltezvendor.Model.OrderIdItem;
import com.pivotasoft.selltezvendor.Response.OrdersResponse;

public interface Dilogueinterface {
    void onInvoiceClick(OrderIdItem orderListBean);
    void onUpdateStatusClick(OrderIdItem orderListBean);
}
