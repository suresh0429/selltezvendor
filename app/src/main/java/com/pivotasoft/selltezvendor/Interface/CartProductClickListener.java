package com.pivotasoft.selltezvendor.Interface;

import com.pivotasoft.selltezvendor.Model.Product;

public interface CartProductClickListener {

    void onMinusClick(Product product);

    void onPlusClick(Product product);

    void onSaveClick(Product product);

    void onRemoveDialog(Product product);

    void onWishListDialog(Product product);

}
