package com.pivotasoft.selltezvendor.Interface;

import com.pivotasoft.selltezvendor.Response.CheckoutResponse;

import java.util.List;

public interface PaymentTypeInterface {

    void onItemClick(List<CheckoutResponse.DataBean.PaymentGatewayBean> paymentGatewayBean, int position);
}
