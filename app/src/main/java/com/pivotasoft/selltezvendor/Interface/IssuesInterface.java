package com.pivotasoft.selltezvendor.Interface;

import com.pivotasoft.selltezvendor.Model.Product;
import com.pivotasoft.selltezvendor.Response.IssuesListResponse;

public interface IssuesInterface {
    void issueModify(IssuesListResponse.StoreissuelistdataBean storeissuelistdataBean);
}
