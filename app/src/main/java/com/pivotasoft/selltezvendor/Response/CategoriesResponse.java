package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class CategoriesResponse {


    /**
     * categoriesdata : [{"categoryid":"1","categoryname":"Staples","categorypic":"staples.jpg"},{"categoryid":"2","categoryname":"Vegetables","categorypic":"vegetables.jpg"},{"categoryid":"3","categoryname":"Fruits","categorypic":"fruits.jpg"},{"categoryid":"4","categoryname":"Bakery","categorypic":"bakery.jpg"},{"categoryid":"5","categoryname":"Beverages","categorypic":"beverages.jpg"},{"categoryid":"6","categoryname":"Household Care","categorypic":"household.jpg"},{"categoryid":"7","categoryname":"Personal Care","categorypic":"personalcare.jpg"},{"categoryid":"8","categoryname":"Baby Care","categorypic":"babycare.jpg"}]
     * message : success
     */

    private String message;
    private List<CategoriesdataBean> categoriesdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CategoriesdataBean> getCategoriesdata() {
        return categoriesdata;
    }

    public void setCategoriesdata(List<CategoriesdataBean> categoriesdata) {
        this.categoriesdata = categoriesdata;
    }

    public static class CategoriesdataBean {
        /**
         * categoryid : 1
         * categoryname : Staples
         * categorypic : staples.jpg
         */

        private String categoryid;
        private String categoryname;
        private String categorypic;

        public String getCategoryid() {
            return categoryid;
        }

        public void setCategoryid(String categoryid) {
            this.categoryid = categoryid;
        }

        public String getCategoryname() {
            return categoryname;
        }

        public void setCategoryname(String categoryname) {
            this.categoryname = categoryname;
        }

        public String getCategorypic() {
            return categorypic;
        }

        public void setCategorypic(String categorypic) {
            this.categorypic = categorypic;
        }
    }
}
