package com.pivotasoft.selltezvendor.Response;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("title")
    private String title;

    @SerializedName("orderId")
    private String orderId;

    @SerializedName("content")
    private String content;

    public Data(String title, String orderId, String content) {
        this.title = title;
        this.orderId = orderId;
        this.content = content;
    }
}
