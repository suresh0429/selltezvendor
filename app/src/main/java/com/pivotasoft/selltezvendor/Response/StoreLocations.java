package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class StoreLocations {


    /**
     * storesdata : [{"storeid":"1","title":"Swathi Market","subtitle":"Grocceries","owner":"Kumar CH","starttime":"08:00:00","endtime":"19:00:00","supportkm":"5","address":"KPHB Colony","city":"Hyderabad","latitude":"17.4968","longitude":"78.3972","mobile":"9556677882","email":"2","password":"test123","status":"1","distance":"0.00018987059593200684","ratings":"4.3"}]
     * message : success
     */

    private String message;
    private List<StoresdataBean> storesdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StoresdataBean> getStoresdata() {
        return storesdata;
    }

    public void setStoresdata(List<StoresdataBean> storesdata) {
        this.storesdata = storesdata;
    }

    public static class StoresdataBean {
        /**
         * storeid : 1
         * title : Swathi Market
         * subtitle : Grocceries
         * owner : Kumar CH
         * starttime : 08:00:00
         * endtime : 19:00:00
         * supportkm : 5
         * address : KPHB Colony
         * city : Hyderabad
         * latitude : 17.4968
         * longitude : 78.3972
         * mobile : 9556677882
         * email : 2
         * password : test123
         * status : 1
         * distance : 0.00018987059593200684
         * ratings : 4.3
         */

        private String storeid;
        private String title;
        private String subtitle;
        private String owner;
        private String starttime;
        private String endtime;
        private String supportkm;
        private String address;
        private String city;
        private String latitude;
        private String longitude;
        private String mobile;
        private String email;
        private String password;
        private String status;
        private String distance;
        private String ratings;

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getSupportkm() {
            return supportkm;
        }

        public void setSupportkm(String supportkm) {
            this.supportkm = supportkm;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getRatings() {
            return ratings;
        }

        public void setRatings(String ratings) {
            this.ratings = ratings;
        }
    }
}
