package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class IssuesListResponse {


    /**
     * storeissuelistdata : [{"issueid":"7","orderid":"35","description":"It's getting late","assignedon":"2020-05-12 10:23:54","resolvedescription":"Pending","resolvedon":null,"buyerid":"1","bookingdatetime":"2020-05-12 15:51:51","billamount":"2053","couponid":"4","discount":"100","deliverycharges":"41.06","finalbill":"1994.06","addressid":"1","storeid":"2","fullname":"Eeswar T","mobile":"9160106499","email":"gangadhar.t9@gmail.com","password":"cc03e747a6afbbcbf8be7668acfebee5","status":"2","userkey":"oYyROWQzc2JxupBqAZLVCwmin3egKPf1","invoiceno":"ST000010000000035"}]
     * message : success
     */

    private String message;
    private List<StoreissuelistdataBean> storeissuelistdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StoreissuelistdataBean> getStoreissuelistdata() {
        return storeissuelistdata;
    }

    public void setStoreissuelistdata(List<StoreissuelistdataBean> storeissuelistdata) {
        this.storeissuelistdata = storeissuelistdata;
    }

    public static class StoreissuelistdataBean {
        /**
         * issueid : 7
         * orderid : 35
         * description : It's getting late
         * assignedon : 2020-05-12 10:23:54
         * resolvedescription : Pending
         * resolvedon : null
         * buyerid : 1
         * bookingdatetime : 2020-05-12 15:51:51
         * billamount : 2053
         * couponid : 4
         * discount : 100
         * deliverycharges : 41.06
         * finalbill : 1994.06
         * addressid : 1
         * storeid : 2
         * fullname : Eeswar T
         * mobile : 9160106499
         * email : gangadhar.t9@gmail.com
         * password : cc03e747a6afbbcbf8be7668acfebee5
         * status : 2
         * userkey : oYyROWQzc2JxupBqAZLVCwmin3egKPf1
         * invoiceno : ST000010000000035
         */

        private String issueid;
        private String orderid;
        private String description;
        private String assignedon;
        private String resolvedescription;
        private Object resolvedon;
        private String buyerid;
        private String bookingdatetime;
        private String billamount;
        private String couponid;
        private String discount;
        private String deliverycharges;
        private String finalbill;
        private String addressid;
        private String storeid;
        private String fullname;
        private String mobile;
        private String email;
        private String password;
        private String status;
        private String userkey;
        private String invoiceno;

        public String getIssueid() {
            return issueid;
        }

        public void setIssueid(String issueid) {
            this.issueid = issueid;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getAssignedon() {
            return assignedon;
        }

        public void setAssignedon(String assignedon) {
            this.assignedon = assignedon;
        }

        public String getResolvedescription() {
            return resolvedescription;
        }

        public void setResolvedescription(String resolvedescription) {
            this.resolvedescription = resolvedescription;
        }

        public Object getResolvedon() {
            return resolvedon;
        }

        public void setResolvedon(Object resolvedon) {
            this.resolvedon = resolvedon;
        }

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }

        public String getBookingdatetime() {
            return bookingdatetime;
        }

        public void setBookingdatetime(String bookingdatetime) {
            this.bookingdatetime = bookingdatetime;
        }

        public String getBillamount() {
            return billamount;
        }

        public void setBillamount(String billamount) {
            this.billamount = billamount;
        }

        public String getCouponid() {
            return couponid;
        }

        public void setCouponid(String couponid) {
            this.couponid = couponid;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getDeliverycharges() {
            return deliverycharges;
        }

        public void setDeliverycharges(String deliverycharges) {
            this.deliverycharges = deliverycharges;
        }

        public String getFinalbill() {
            return finalbill;
        }

        public void setFinalbill(String finalbill) {
            this.finalbill = finalbill;
        }

        public String getAddressid() {
            return addressid;
        }

        public void setAddressid(String addressid) {
            this.addressid = addressid;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUserkey() {
            return userkey;
        }

        public void setUserkey(String userkey) {
            this.userkey = userkey;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }
    }
}
