package com.pivotasoft.selltezvendor.Response;

import com.google.gson.annotations.SerializedName;

public class NotificationBody {
    @SerializedName("data")
    private Data data;

    @SerializedName("to")
    private String to;

    public NotificationBody(Data data, String to) {
        this.data = data;
        this.to = to;
    }
}
