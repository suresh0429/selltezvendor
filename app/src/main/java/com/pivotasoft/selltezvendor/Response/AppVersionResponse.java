package com.pivotasoft.selltezvendor.Response;

import com.google.gson.annotations.SerializedName;

public class AppVersionResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : {"android_version":"1.0","ios_version":"1.0"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * android_version : 1.0
         * ios_version : 1.0
         */

        @SerializedName("android_version")
        private String androidVersion;
        @SerializedName("ios_version")
        private String iosVersion;

        public String getAndroidVersion() {
            return androidVersion;
        }

        public void setAndroidVersion(String androidVersion) {
            this.androidVersion = androidVersion;
        }

        public String getIosVersion() {
            return iosVersion;
        }

        public void setIosVersion(String iosVersion) {
            this.iosVersion = iosVersion;
        }
    }
}
