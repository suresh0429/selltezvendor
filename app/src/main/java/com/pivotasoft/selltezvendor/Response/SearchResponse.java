package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class SearchResponse {


    /**
     * status : 1
     * search_list : [{"maincatId":"23","category":"mobiles","product_id":"PIKM782780","product_name":"Galaxy A6 +","unit_price_incl_tax":"26990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM782780/a1.jpg","item_id":"107","dis_percent":"20.00","dis_price":"21990.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM394795","product_name":"Galaxy A6 2018","unit_price_incl_tax":"21990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM394795/a1.jpg","item_id":"110","dis_percent":"24.00","dis_price":"16990.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM547340","product_name":"Galaxy A8 Plus","unit_price_incl_tax":"34990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM547340/samsung-galaxy-a8-plus-black-afront.jpg","item_id":"104","dis_percent":"15.00","dis_price":"29900.00","varient":"Black(6 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM880810","product_name":"Galaxy A8 Star","unit_price_incl_tax":"39990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM880810/a-a8starb.jpg","item_id":"244","dis_percent":"14.00","dis_price":"34900.00","varient":"Black(6 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM749714","product_name":"Galaxy J2 2018","unit_price_incl_tax":"7690.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM749714/a1.jpg","item_id":"144","dis_percent":"0.00","dis_price":"0.00","varient":"Black(1 GB-8 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM444906","product_name":"Galaxy J2 Core","unit_price_incl_tax":"6190.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM444906/a-j2coreb.jpg","item_id":"246","dis_percent":"0.00","dis_price":"0.00","varient":"Black(1 GB-8 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM604900","product_name":"Galaxy J4 ","unit_price_incl_tax":"15990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM604900/a1.jpg","item_id":"133","dis_percent":"35.00","dis_price":"10990.00","varient":"Black(3 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM884521","product_name":"Galaxy J6 2018 ","unit_price_incl_tax":"19990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM884521/b1.jpg","item_id":"119","dis_percent":"25.00","dis_price":"14990.00","varient":"Blue(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM207245","product_name":"Galaxy J8 2018  64GB","unit_price_incl_tax":"22990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM207245/b1.jpg","item_id":"116","dis_percent":"24.00","dis_price":"17990.00","varient":"Blue(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM926428","product_name":"Galaxy Note 8","unit_price_incl_tax":"64900.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM926428/1.jpg","item_id":"85","dis_percent":"8.00","dis_price":"59900.00","varient":"Black(6 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM471692","product_name":"Galaxy Note9 ","unit_price_incl_tax":"73600.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM471692/b1.jpg","item_id":"51","dis_percent":"8.00","dis_price":"67900.00","varient":"Midnight Black (6 GB-128 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM441500","product_name":"Galaxy S9 ","unit_price_incl_tax":"70900.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM441500/a1.jpg","item_id":"65","dis_percent":"8.00","dis_price":"65900.00","varient":"Coral Blue(4 GB-256 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM850190","product_name":"Galaxy S9 Plus","unit_price_incl_tax":"72900.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM850190/b1.jpg","item_id":"54","dis_percent":"8.00","dis_price":"67900.00","varient":"Midnight Black (6 GB-256 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM901186","product_name":"HONOR 7x","unit_price_incl_tax":"9500.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM901186/PIKM2.2LW2X0.jpg","item_id":"23","dis_percent":"0.00","dis_price":"0.00","varient":"Gold(4 GB-32 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM909964","product_name":"HONOR 9 lite","unit_price_incl_tax":"11500.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM909964/PIKM1.BK949F.jpg","item_id":"24","dis_percent":"0.00","dis_price":"0.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM628356","product_name":"Lg Q6+","unit_price_incl_tax":"7000.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM628356//1.jpg","item_id":"437","dis_percent":"0.00","dis_price":"0.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM275974","product_name":"MI A2","unit_price_incl_tax":"0.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM275974/368934058_a..jpg","item_id":"512","dis_percent":"0.00","dis_price":"0.00","varient":"Blue(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM118890","product_name":"Mi Max 2 (4+64)","unit_price_incl_tax":"16999.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM118890/PIKMa.YJ64J2.jpg","item_id":"286","dis_percent":"8.00","dis_price":"15999.00","varient":"Gold(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM917752","product_name":"Mi Mix2 (6+128)","unit_price_incl_tax":"37999.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM917752/a.jpg","item_id":"268","dis_percent":"22.00","dis_price":"29999.00","varient":"White(6 GB-128 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM610541","product_name":"Oppo A3S","unit_price_incl_tax":"10990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM610541/483847008_a..jpg","item_id":"381","dis_percent":"0.00","dis_price":"0.00","varient":"Lilac Purple(2 GB-16 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM806341","product_name":"Oppo A5","unit_price_incl_tax":"14990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM806341/14461395_a..jpg","item_id":"390","dis_percent":"0.00","dis_price":"0.00","varient":"Blue(4 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM232497","product_name":"Oppo A71","unit_price_incl_tax":"9990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM232497/479491574_a..jpg","item_id":"375","dis_percent":"0.00","dis_price":"0.00","varient":"Black(3 GB-16 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM217945","product_name":"Oppo A83","unit_price_incl_tax":"10990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM217945/913880215_a..jpg","item_id":"377","dis_percent":"0.00","dis_price":"0.00","varient":"Champagne Gold(3 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM526809","product_name":"Oppo F7","unit_price_incl_tax":"19990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM526809/930833872_a..jpg","item_id":"385","dis_percent":"0.00","dis_price":"0.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM128536","product_name":"Oppo F9 Pro","unit_price_incl_tax":"23990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM128536/472190559_a..jpg","item_id":"392","dis_percent":"0.00","dis_price":"0.00","varient":"Starry Purple(6 GB-64 GB)"},{"maincatId":"22","category":"mobiles","product_id":"PIKM684480","product_name":"pTron HBE7 (With In-Line Microphone)","unit_price_incl_tax":"599.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM684480//1.jpg","item_id":"504","dis_percent":"20.00","dis_price":"479.00","varient":""},{"maincatId":"22","category":"mobiles","product_id":"PIKM880504","product_name":"Ptron Kicks (Wireless Headphones)","unit_price_incl_tax":"999.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM880504//1.jpg","item_id":"493","dis_percent":"20.00","dis_price":"799.00","varient":""},{"maincatId":"23","category":"mobiles","product_id":"PIKM339103","product_name":"Redmi 5","unit_price_incl_tax":"12999.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM339103/167072786_a..jpg","item_id":"311","dis_percent":"5.00","dis_price":"12499.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM015408","product_name":"Redmi 5A","unit_price_incl_tax":"7499.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM015408/837566490_a..jpg","item_id":"324","dis_percent":"0.00","dis_price":"0.00","varient":"Gold(3 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM976351","product_name":"Redmi 6","unit_price_incl_tax":"0.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM976351/747548528_a..jpg","item_id":"537","dis_percent":"0.00","dis_price":"0.00","varient":"Blue(3 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM591832","product_name":"Redmi 6 Pro","unit_price_incl_tax":"0.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM591832/921364806_a..jpg","item_id":"545","dis_percent":"0.00","dis_price":"0.00","varient":"Gold(3 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM140217","product_name":"Redmi 6A","unit_price_incl_tax":"0.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM140217/661082103_a..jpg","item_id":"522","dis_percent":"0.00","dis_price":"0.00","varient":"Blue(2 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM624679","product_name":"Redmi Note 5","unit_price_incl_tax":"12999.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM624679/PIKMa.GR9335.jpg","item_id":"351","dis_percent":"5.00","dis_price":"12499.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM662760","product_name":"Redmi Note 5  Pro","unit_price_incl_tax":"17999.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM662760/a.jpg","item_id":"276","dis_percent":"4.00","dis_price":"17499.00","varient":"Gold(6 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM213246","product_name":"Redmi Y 2","unit_price_incl_tax":"10499.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM213246/64166734_a..jpg","item_id":"296","dis_percent":"0.00","dis_price":"0.00","varient":"Gold(3 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM172930","product_name":"Samsung A7`18","unit_price_incl_tax":"28990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM172930/269687225_a..jpg","item_id":"418","dis_percent":"0.00","dis_price":"0.00","varient":"Black(6 GB-128 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM468201","product_name":"Samsung A8+","unit_price_incl_tax":"22000.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM468201/PIKMb.K05JF3.jpg","item_id":"21","dis_percent":"0.00","dis_price":"0.00","varient":"Black(6 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM053803","product_name":"Samsung A9`18","unit_price_incl_tax":"39990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM053803/362626693_a..jpg","item_id":"412","dis_percent":"0.00","dis_price":"0.00","varient":"Rose Pink(8 GB-128 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM258712","product_name":"Samsung E 1200 ","unit_price_incl_tax":"1100.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM258712/a1.jpg","item_id":"101","dis_percent":"0.00","dis_price":"0.00","varient":"Black(156 MB-512 MB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM407639","product_name":"Samsung GURU FM Plus","unit_price_incl_tax":"1375.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM407639/b1.jpg","item_id":"170","dis_percent":"0.00","dis_price":"0.00","varient":"Blue(4 MB-NA)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM288604","product_name":"Samsung GURU MUSIC 2","unit_price_incl_tax":"1625.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM288604/d1.jpg","item_id":"157","dis_percent":"0.00","dis_price":"0.00","varient":"White(208 MB-512 MB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM466203","product_name":"Samsung J4+","unit_price_incl_tax":"8000.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM466203/PIKM1.6QV748.jpg","item_id":"25","dis_percent":"0.00","dis_price":"0.00","varient":"Gold(2 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM985496","product_name":"Samsung J4+ `18","unit_price_incl_tax":"9990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM985496/75957256_a..jpg","item_id":"430","dis_percent":"0.00","dis_price":"0.00","varient":"Black(2 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM917264","product_name":"Samsung J6+` 18","unit_price_incl_tax":"14990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM917264/PIKMa.623H29.jpg","item_id":"426","dis_percent":"0.00","dis_price":"0.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM424907","product_name":"Samsung J7 Max","unit_price_incl_tax":"8000.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM424907/PIKM1.9LQ88G.jpg","item_id":"22","dis_percent":"0.00","dis_price":"0.00","varient":"Gold(4 GB-32 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM789014","product_name":"Samsung J7 Pro","unit_price_incl_tax":"9000.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM789014//1.jpg","item_id":"438","dis_percent":"0.00","dis_price":"0.00","varient":"Black(3 GB-64 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM449640","product_name":"Samsung J8 ","unit_price_incl_tax":"13500.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM449640//b.jpg","item_id":"439","dis_percent":"0.00","dis_price":"0.00","varient":"Black(4 GB-64 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM647819","product_name":"Samsung METRO 312","unit_price_incl_tax":"2025.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM647819/a1.jpg","item_id":"163","dis_percent":"0.00","dis_price":"0.00","varient":"Black(16 MB-512 MB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM602937","product_name":"Samsung METRO 360","unit_price_incl_tax":"2800.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM602937/a1.jpg","item_id":"165","dis_percent":"0.00","dis_price":"0.00","varient":"Black(32 MB-NA)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM025461","product_name":"Samsung METRO XL","unit_price_incl_tax":"3225.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM025461/samsung-metro-black-1front.jpg","item_id":"161","dis_percent":"0.00","dis_price":"0.00","varient":"Black(64 MB-512 MB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM792635","product_name":"Vivo V11 Pro","unit_price_incl_tax":"28990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM792635/PIKMa-v11pron.449675.jpg","item_id":"253","dis_percent":"13.00","dis_price":"25990.00","varient":"Neon Purple(6 GB-128 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM624279","product_name":"Vivo Y81","unit_price_incl_tax":"13990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM624279/a-Y81.jpg","item_id":"233","dis_percent":"10.00","dis_price":"12990.00","varient":"Gold(3 GB-32 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM181082","product_name":"Vivo Y83 ","unit_price_incl_tax":"9000.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM181082/PIKM1.6LVH73.jpg","item_id":"436","dis_percent":"0.00","dis_price":"0.00","varient":"Gold(4 GB-32 GB)"},{"maincatId":"24","category":"mobiles","product_id":"PIKM683643","product_name":"Vivo Y83","unit_price_incl_tax":"8500.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM683643//b1.jpg","item_id":"435","dis_percent":"0.00","dis_price":"0.00","varient":"Black(4 GB-32 GB)"},{"maincatId":"23","category":"mobiles","product_id":"PIKM670286","product_name":"Vivo Y83 Pro","unit_price_incl_tax":"16990.00","rating":"0","image_path":"https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM670286/a-Y83 pro.jpg","item_id":"235","dis_percent":"0.00","dis_price":"0.00","varient":"Black(4 GB-64 GB)"}]
     * message : Get All Searched Items
     */

    private int status;
    private String message;
    private List<SearchListBean> search_list;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SearchListBean> getSearch_list() {
        return search_list;
    }

    public void setSearch_list(List<SearchListBean> search_list) {
        this.search_list = search_list;
    }

    public static class SearchListBean {
        /**
         * maincatId : 23
         * category : mobiles
         * product_id : PIKM782780
         * product_name : Galaxy A6 +
         * unit_price_incl_tax : 26990.00
         * rating : 0
         * image_path : https://pickany24x7.com/mobiles/mobiles/../../image/mobiles/product_images/PIKM782780/a1.jpg
         * item_id : 107
         * dis_percent : 20.00
         * dis_price : 21990.00
         * varient : Black(4 GB-64 GB)
         */

        private String maincatId;
        private String category;
        private String product_id;
        private String product_name;
        private String unit_price_incl_tax;
        private String rating;
        private String image_path;
        private String item_id;
        private String dis_percent;
        private String dis_price;
        private String varient;

        public String getMaincatId() {
            return maincatId;
        }

        public void setMaincatId(String maincatId) {
            this.maincatId = maincatId;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getUnit_price_incl_tax() {
            return unit_price_incl_tax;
        }

        public void setUnit_price_incl_tax(String unit_price_incl_tax) {
            this.unit_price_incl_tax = unit_price_incl_tax;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getImage_path() {
            return image_path;
        }

        public void setImage_path(String image_path) {
            this.image_path = image_path;
        }

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getDis_percent() {
            return dis_percent;
        }

        public void setDis_percent(String dis_percent) {
            this.dis_percent = dis_percent;
        }

        public String getDis_price() {
            return dis_price;
        }

        public void setDis_price(String dis_price) {
            this.dis_price = dis_price;
        }

        public String getVarient() {
            return varient;
        }

        public void setVarient(String varient) {
            this.varient = varient;
        }
    }
}
