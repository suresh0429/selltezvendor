package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class AnaliticsResponse {


    /**
     * analyticsdata : [{"grossearnings":"10796.70","commission":"317.55","tcs":"105.85","transactionfees":"211.70","netearnings":"10161.60","nofoforders":"6","storeid":"2"}]
     * message : success
     */

    private String message;
    private List<AnalyticsdataBean> analyticsdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AnalyticsdataBean> getAnalyticsdata() {
        return analyticsdata;
    }

    public void setAnalyticsdata(List<AnalyticsdataBean> analyticsdata) {
        this.analyticsdata = analyticsdata;
    }

    public static class AnalyticsdataBean {
        /**
         * grossearnings : 10796.70
         * commission : 317.55
         * tcs : 105.85
         * transactionfees : 211.70
         * netearnings : 10161.60
         * nofoforders : 6
         * storeid : 2
         */

        private String grossearnings;
        private String commission;
        private String tcs;
        private String transactionfees;
        private String netearnings;
        private String nofoforders;
        private String storeid;

        public String getGrossearnings() {
            return grossearnings;
        }

        public void setGrossearnings(String grossearnings) {
            this.grossearnings = grossearnings;
        }

        public String getCommission() {
            return commission;
        }

        public void setCommission(String commission) {
            this.commission = commission;
        }

        public String getTcs() {
            return tcs;
        }

        public void setTcs(String tcs) {
            this.tcs = tcs;
        }

        public String getTransactionfees() {
            return transactionfees;
        }

        public void setTransactionfees(String transactionfees) {
            this.transactionfees = transactionfees;
        }

        public String getNetearnings() {
            return netearnings;
        }

        public void setNetearnings(String netearnings) {
            this.netearnings = netearnings;
        }

        public String getNofoforders() {
            return nofoforders;
        }

        public void setNofoforders(String nofoforders) {
            this.nofoforders = nofoforders;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }
    }
}
