package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class OrderListResponse {


    /**
     * itemdata : [{"orderitemid":"10","orderid":"9","productid":"2","finalprice":"90","quantity":"8","itemtotal":"720","title":"Srinivasa Ragi Malt 500mg","measureunits":"500 mg","unitprice":"100","discount":"10","productpic":"srini-ragi-malt.png","description":"Srinivasa Ragi Malt 500mg","subcategoryid":"3"},{"orderitemid":"11","orderid":"9","productid":"5","finalprice":"13","quantity":"1","itemtotal":"13","title":"Tata Iodised Crystal Salt","measureunits":"1 KG","unitprice":"14","discount":"1","productpic":"tata-crystal-salt.png","description":"Tata Iodised Crystal Salt","subcategoryid":"1"},{"orderitemid":"12","orderid":"9","productid":"3","finalprice":"76","quantity":"18","itemtotal":"1368","title":"Aasirwad Sugar Release COntrol Atta 1KG","measureunits":"1 KG","unitprice":"80","discount":"5","productpic":"aasirwad-sug-atta.png","description":"Aasirwad Sugar Release COntrol Atta 1KG","subcategoryid":"3"}]
     * message : success
     */

    private String message;
    private List<ItemdataBean> itemdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ItemdataBean> getItemdata() {
        return itemdata;
    }

    public void setItemdata(List<ItemdataBean> itemdata) {
        this.itemdata = itemdata;
    }

    public static class ItemdataBean {
        /**
         * orderitemid : 10
         * orderid : 9
         * productid : 2
         * finalprice : 90
         * quantity : 8
         * itemtotal : 720
         * title : Srinivasa Ragi Malt 500mg
         * measureunits : 500 mg
         * unitprice : 100
         * discount : 10
         * productpic : srini-ragi-malt.png
         * description : Srinivasa Ragi Malt 500mg
         * subcategoryid : 3
         */

        private String orderitemid;
        private String orderid;
        private String productid;
        private String finalprice;
        private String quantity;
        private String itemtotal;
        private String title;
        private String measureunits;
        private String unitprice;
        private String discount;
        private String productpic;
        private String description;
        private String subcategoryid;

        public String getOrderitemid() {
            return orderitemid;
        }

        public void setOrderitemid(String orderitemid) {
            this.orderitemid = orderitemid;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getProductid() {
            return productid;
        }

        public void setProductid(String productid) {
            this.productid = productid;
        }

        public String getFinalprice() {
            return finalprice;
        }

        public void setFinalprice(String finalprice) {
            this.finalprice = finalprice;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getItemtotal() {
            return itemtotal;
        }

        public void setItemtotal(String itemtotal) {
            this.itemtotal = itemtotal;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMeasureunits() {
            return measureunits;
        }

        public void setMeasureunits(String measureunits) {
            this.measureunits = measureunits;
        }

        public String getUnitprice() {
            return unitprice;
        }

        public void setUnitprice(String unitprice) {
            this.unitprice = unitprice;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getProductpic() {
            return productpic;
        }

        public void setProductpic(String productpic) {
            this.productpic = productpic;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSubcategoryid() {
            return subcategoryid;
        }

        public void setSubcategoryid(String subcategoryid) {
            this.subcategoryid = subcategoryid;
        }
    }
}
