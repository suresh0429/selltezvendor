package com.pivotasoft.selltezvendor.Response;

public class PayuMoneyPutResponse {


    /**
     * status : 0
     * user_id :
     * order_id :
     * payment_id :
     * message : Online Transaction Failed
     */

    private int status;
    private String user_id;
    private String order_id;
    private String payment_id;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
