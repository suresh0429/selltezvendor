package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class CheckoutResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : {"mobile_verify_status":"1","mobile":"8985018103","address":[{"id":"45","user_id":"3","name":"Suresh Kumar","address_line1":"1235bjfjfjkf","address_line2":"ufjfjgjg","area":"Chandanagar","city":"Hyderabad","state":"Telangana","country":"India","pincode":"500050","latitude":"0","longitude":"0","contact_no":"8985018103","alternate_contact_no":"","is_default":"Yes","created_on":"2019-09-23 18:31:53","updated_on":"2019-09-23 18:31:53","delivery_status":true,"delivery_charges":0}],"products":[{"cart_id":"229","product_id":"29","c1_url":"beverages","c2_url":"HealthDrinksMix","c3_url":null,"product_name":"Mother's Horlicks Kesar Flavour ","selling_price":"463.00","mrp_price":"515.00","purchase_quantity":"16","unit_id":"2","unit_name":"gms","unit_value":"450","brand_id":"1","brand_name":"KILOMART","images":"b4461-4.png","p_url":"beverages/HealthDrinksMix/29","net_amount":7408,"gross_amount":8240}],"final_gross_amount":"8,240.00","final_net_amount":"7,408.00","cart_discount":"0.00","coupon_discount":"0.00","voucher_discount":"0.00","final_discount":"832.00","payable_amount":"7,408.00","payment_gateway":[{"id":"1","name":"Pay on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * mobile_verify_status : 1
         * mobile : 8985018103
         * address : [{"id":"45","user_id":"3","name":"Suresh Kumar","address_line1":"1235bjfjfjkf","address_line2":"ufjfjgjg","area":"Chandanagar","city":"Hyderabad","state":"Telangana","country":"India","pincode":"500050","latitude":"0","longitude":"0","contact_no":"8985018103","alternate_contact_no":"","is_default":"Yes","created_on":"2019-09-23 18:31:53","updated_on":"2019-09-23 18:31:53","delivery_status":true,"delivery_charges":0}]
         * products : [{"cart_id":"229","product_id":"29","c1_url":"beverages","c2_url":"HealthDrinksMix","c3_url":null,"product_name":"Mother's Horlicks Kesar Flavour ","selling_price":"463.00","mrp_price":"515.00","purchase_quantity":"16","unit_id":"2","unit_name":"gms","unit_value":"450","brand_id":"1","brand_name":"KILOMART","images":"b4461-4.png","p_url":"beverages/HealthDrinksMix/29","net_amount":7408,"gross_amount":8240}]
         * final_gross_amount : 8,240.00
         * final_net_amount : 7,408.00
         * cart_discount : 0.00
         * coupon_discount : 0.00
         * voucher_discount : 0.00
         * final_discount : 832.00
         * payable_amount : 7,408.00
         * payment_gateway : [{"id":"1","name":"Pay on Delivery","logo":"images/gateway/Cash-on-Delivery.jpg"}]
         */

        private String mobile_verify_status;
        private String mobile;
        private String final_gross_amount;
        private String final_net_amount;
        private String cart_discount;
        private String coupon_discount;
        private String voucher_discount;
        private String final_discount;
        private String payable_amount;
        private List<AddressBean> address;
        private List<ProductsBean> products;
        private List<PaymentGatewayBean> payment_gateway;

        public String getMobile_verify_status() {
            return mobile_verify_status;
        }

        public void setMobile_verify_status(String mobile_verify_status) {
            this.mobile_verify_status = mobile_verify_status;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getFinal_gross_amount() {
            return final_gross_amount;
        }

        public void setFinal_gross_amount(String final_gross_amount) {
            this.final_gross_amount = final_gross_amount;
        }

        public String getFinal_net_amount() {
            return final_net_amount;
        }

        public void setFinal_net_amount(String final_net_amount) {
            this.final_net_amount = final_net_amount;
        }

        public String getCart_discount() {
            return cart_discount;
        }

        public void setCart_discount(String cart_discount) {
            this.cart_discount = cart_discount;
        }

        public String getCoupon_discount() {
            return coupon_discount;
        }

        public void setCoupon_discount(String coupon_discount) {
            this.coupon_discount = coupon_discount;
        }

        public String getVoucher_discount() {
            return voucher_discount;
        }

        public void setVoucher_discount(String voucher_discount) {
            this.voucher_discount = voucher_discount;
        }

        public String getFinal_discount() {
            return final_discount;
        }

        public void setFinal_discount(String final_discount) {
            this.final_discount = final_discount;
        }

        public String getPayable_amount() {
            return payable_amount;
        }

        public void setPayable_amount(String payable_amount) {
            this.payable_amount = payable_amount;
        }

        public List<AddressBean> getAddress() {
            return address;
        }

        public void setAddress(List<AddressBean> address) {
            this.address = address;
        }

        public List<ProductsBean> getProducts() {
            return products;
        }

        public void setProducts(List<ProductsBean> products) {
            this.products = products;
        }

        public List<PaymentGatewayBean> getPayment_gateway() {
            return payment_gateway;
        }

        public void setPayment_gateway(List<PaymentGatewayBean> payment_gateway) {
            this.payment_gateway = payment_gateway;
        }

        public static class AddressBean {
            /**
             * id : 45
             * user_id : 3
             * name : Suresh Kumar
             * address_line1 : 1235bjfjfjkf
             * address_line2 : ufjfjgjg
             * area : Chandanagar
             * city : Hyderabad
             * state : Telangana
             * country : India
             * pincode : 500050
             * latitude : 0
             * longitude : 0
             * contact_no : 8985018103
             * alternate_contact_no :
             * is_default : Yes
             * created_on : 2019-09-23 18:31:53
             * updated_on : 2019-09-23 18:31:53
             * delivery_status : true
             * delivery_charges : 0
             */

            private String id;
            private String user_id;
            private String name;
            private String address_line1;
            private String address_line2;
            private String area;
            private String city;
            private String state;
            private String country;
            private String pincode;
            private String latitude;
            private String longitude;
            private String contact_no;
            private String alternate_contact_no;
            private String is_default;
            private String created_on;
            private String updated_on;
            private boolean delivery_status;
            private int delivery_charges;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getAddress_line1() {
                return address_line1;
            }

            public void setAddress_line1(String address_line1) {
                this.address_line1 = address_line1;
            }

            public String getAddress_line2() {
                return address_line2;
            }

            public void setAddress_line2(String address_line2) {
                this.address_line2 = address_line2;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getContact_no() {
                return contact_no;
            }

            public void setContact_no(String contact_no) {
                this.contact_no = contact_no;
            }

            public String getAlternate_contact_no() {
                return alternate_contact_no;
            }

            public void setAlternate_contact_no(String alternate_contact_no) {
                this.alternate_contact_no = alternate_contact_no;
            }

            public String getIs_default() {
                return is_default;
            }

            public void setIs_default(String is_default) {
                this.is_default = is_default;
            }

            public String getCreated_on() {
                return created_on;
            }

            public void setCreated_on(String created_on) {
                this.created_on = created_on;
            }

            public String getUpdated_on() {
                return updated_on;
            }

            public void setUpdated_on(String updated_on) {
                this.updated_on = updated_on;
            }

            public boolean isDelivery_status() {
                return delivery_status;
            }

            public void setDelivery_status(boolean delivery_status) {
                this.delivery_status = delivery_status;
            }

            public int getDelivery_charges() {
                return delivery_charges;
            }

            public void setDelivery_charges(int delivery_charges) {
                this.delivery_charges = delivery_charges;
            }
        }

        public static class ProductsBean {
            /**
             * cart_id : 229
             * product_id : 29
             * c1_url : beverages
             * c2_url : HealthDrinksMix
             * c3_url : null
             * product_name : Mother's Horlicks Kesar Flavour 
             * selling_price : 463.00
             * mrp_price : 515.00
             * purchase_quantity : 16
             * unit_id : 2
             * unit_name : gms
             * unit_value : 450
             * brand_id : 1
             * brand_name : KILOMART
             * images : b4461-4.png
             * p_url : beverages/HealthDrinksMix/29
             * net_amount : 7408
             * gross_amount : 8240
             */

            private String cart_id;
            private String product_id;
            private String c1_url;
            private String c2_url;
            private Object c3_url;
            private String product_name;
            private String selling_price;
            private String mrp_price;
            private String purchase_quantity;
            private String unit_id;
            private String unit_name;
            private String unit_value;
            private String brand_id;
            private String brand_name;
            private String images;
            private String p_url;
            private int net_amount;
            private int gross_amount;

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getC1_url() {
                return c1_url;
            }

            public void setC1_url(String c1_url) {
                this.c1_url = c1_url;
            }

            public String getC2_url() {
                return c2_url;
            }

            public void setC2_url(String c2_url) {
                this.c2_url = c2_url;
            }

            public Object getC3_url() {
                return c3_url;
            }

            public void setC3_url(Object c3_url) {
                this.c3_url = c3_url;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getSelling_price() {
                return selling_price;
            }

            public void setSelling_price(String selling_price) {
                this.selling_price = selling_price;
            }

            public String getMrp_price() {
                return mrp_price;
            }

            public void setMrp_price(String mrp_price) {
                this.mrp_price = mrp_price;
            }

            public String getPurchase_quantity() {
                return purchase_quantity;
            }

            public void setPurchase_quantity(String purchase_quantity) {
                this.purchase_quantity = purchase_quantity;
            }

            public String getUnit_id() {
                return unit_id;
            }

            public void setUnit_id(String unit_id) {
                this.unit_id = unit_id;
            }

            public String getUnit_name() {
                return unit_name;
            }

            public void setUnit_name(String unit_name) {
                this.unit_name = unit_name;
            }

            public String getUnit_value() {
                return unit_value;
            }

            public void setUnit_value(String unit_value) {
                this.unit_value = unit_value;
            }

            public String getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(String brand_id) {
                this.brand_id = brand_id;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getP_url() {
                return p_url;
            }

            public void setP_url(String p_url) {
                this.p_url = p_url;
            }

            public int getNet_amount() {
                return net_amount;
            }

            public void setNet_amount(int net_amount) {
                this.net_amount = net_amount;
            }

            public int getGross_amount() {
                return gross_amount;
            }

            public void setGross_amount(int gross_amount) {
                this.gross_amount = gross_amount;
            }
        }

        public static class PaymentGatewayBean {
            /**
             * id : 1
             * name : Pay on Delivery
             * logo : images/gateway/Cash-on-Delivery.jpg
             */

            private String id;
            private String name;
            private String logo;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }
        }
    }
}
