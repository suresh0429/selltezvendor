package com.pivotasoft.selltezvendor.Response;

public class WishListPostResponse {


    /**
     * status : 1
     * message : WishList Items has been Inserted
     */

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
