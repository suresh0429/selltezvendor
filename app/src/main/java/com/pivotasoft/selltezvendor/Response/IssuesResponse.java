package com.pivotasoft.selltezvendor.Response;

public class IssuesResponse {


    /**
     * issuedata : {"message":"Issue already raised. Please wait."}
     * status : success
     */

    private IssuedataBean issuedata;
    private String status;

    public IssuedataBean getIssuedata() {
        return issuedata;
    }

    public void setIssuedata(IssuedataBean issuedata) {
        this.issuedata = issuedata;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class IssuedataBean {
        /**
         * message : Issue already raised. Please wait.
         */

        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
