package com.pivotasoft.selltezvendor.Response;

public class CreateOrderResponse {

    /**
     * status : success
     * details : {"message":"Order processed successfully","invoiceno":"ST000010000000022"}
     */

    private String status;
    private DetailsBean details;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DetailsBean getDetails() {
        return details;
    }

    public void setDetails(DetailsBean details) {
        this.details = details;
    }

    public static class DetailsBean {
        /**
         * message : Order processed successfully
         * invoiceno : ST000010000000022
         */

        private String message;
        private String invoiceno;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }
    }
}
