package com.pivotasoft.selltezvendor.Response;

public class BaseResponse1 {

    /**
     * status : 10100
     * message : OTP has been send to your mobile.
     */

    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
