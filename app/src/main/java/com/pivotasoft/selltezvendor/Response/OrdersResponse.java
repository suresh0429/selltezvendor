package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class OrdersResponse {


    /**
     * storeordersdata : [{"orderid":"38","storeid":"1","bookingdatetime":"2020-05-12 17:24:11","billamount":"600","discount":"0","deliverycharges":"72","finalbill":"672","invoiceno":"ST000200000000038","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-13 11:00:00","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"39","storeid":"1","bookingdatetime":"2020-05-12 18:24:28","billamount":"560","discount":"0","deliverycharges":"71.2","finalbill":"631.2","invoiceno":"ST000200000000039","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-13 11:00:00","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"40","storeid":"1","bookingdatetime":"2020-05-12 18:27:54","billamount":"714","discount":"0","deliverycharges":"74.28","finalbill":"788.28","invoiceno":"ST000200000000040","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-13 11:00:00","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"41","storeid":"1","bookingdatetime":"2020-05-12 18:52:58","billamount":"520","discount":"0","deliverycharges":"70.4","finalbill":"590.4","invoiceno":"ST000200000000041","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-13 11:00:00","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"48","storeid":"1","bookingdatetime":"2020-05-13 21:38:22","billamount":"500","discount":"0","deliverycharges":"70","finalbill":"570","invoiceno":"ST000200000000048","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Pending","expectedtime":"2020-05-14 11:00:00","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"51","storeid":"1","bookingdatetime":"2020-05-13 21:51:37","billamount":"500","discount":"0","deliverycharges":"70","finalbill":"570","invoiceno":"ST000200000000051","paymentmode":"online","paymentsummary":"pay_EpuD4E62mBVnLl","paymentstatus":"Success","orderstatus":"Delivered","expectedtime":"2020-05-21 22:02:38","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"54","storeid":"1","bookingdatetime":"2020-05-13 22:03:42","billamount":"816","discount":"0","deliverycharges":"76.32","finalbill":"892.32","invoiceno":"ST000200000000054","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Delivered","expectedtime":"2020-05-21 21:53:02","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"90","storeid":"1","bookingdatetime":"2020-05-13 23:33:45","billamount":"510","discount":"0","deliverycharges":"70.2","finalbill":"580.2","invoiceno":"ST000200000000090","paymentmode":"online","paymentsummary":"pay_EpvxUoy0n14Hty","paymentstatus":"Success","orderstatus":"Confirmed","expectedtime":"2020-05-21 15:33:23","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"97","storeid":"1","bookingdatetime":"2020-05-20 11:50:48","billamount":"800","discount":"0","deliverycharges":"87.6","finalbill":"887.6","invoiceno":"ST000200000000097","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Confirmed","expectedtime":"2020-05-20 12:36:53","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"98","storeid":"1","bookingdatetime":"2020-05-20 12:57:21","billamount":"560","discount":"0","deliverycharges":"80.4","finalbill":"640.4","invoiceno":"ST000200000000098","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Delivered","expectedtime":"2020-05-21 21:11:54","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"99","storeid":"1","bookingdatetime":"2020-05-20 13:12:17","billamount":"2160","discount":"100","deliverycharges":"64.8","finalbill":"2124.8","invoiceno":"ST000200000000099","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Rejected","expectedtime":"2020-05-21 15:55:28","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"100","storeid":"1","bookingdatetime":"2020-05-21 15:04:13","billamount":"1760","discount":"0","deliverycharges":"95.2","finalbill":"1855.2","invoiceno":"ST000200000000100","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Delivered","expectedtime":"2020-05-21 20:55:20","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"101","storeid":"1","bookingdatetime":"2020-05-21 15:06:19","billamount":"640","discount":"0","deliverycharges":"82.8","finalbill":"722.8","invoiceno":"ST000200000000101","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Confirmed","expectedtime":"2020-05-21 22:04:00","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"102","storeid":"1","bookingdatetime":"2020-05-21 15:18:07","billamount":"510","discount":"0","deliverycharges":"78.9","finalbill":"588.9","invoiceno":"ST000200000000102","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Confirmed","expectedtime":"2020-05-21 17:34:43","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"103","storeid":"1","bookingdatetime":"2020-05-21 17:21:23","billamount":"510","discount":"0","deliverycharges":"78.9","finalbill":"588.9","invoiceno":"ST000200000000103","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Confirmed","expectedtime":"2020-05-21 17:32:12","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"},{"orderid":"104","storeid":"1","bookingdatetime":"2020-05-21 17:25:03","billamount":"640","discount":"0","deliverycharges":"82.8","finalbill":"722.8","invoiceno":"ST000200000000104","paymentmode":"Cod","paymentsummary":"Cod","paymentstatus":"Success","orderstatus":"Rejected","expectedtime":"2020-05-21 17:28:18","buyerid":"20","fullname":"suresh","mobile":"8985018103","fcmtoken":"cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20"}]
     * message : success
     */

    private String message;
    private List<StoreordersdataBean> storeordersdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StoreordersdataBean> getStoreordersdata() {
        return storeordersdata;
    }

    public void setStoreordersdata(List<StoreordersdataBean> storeordersdata) {
        this.storeordersdata = storeordersdata;
    }

    public static class StoreordersdataBean {
        /**
         * orderid : 38
         * storeid : 1
         * bookingdatetime : 2020-05-12 17:24:11
         * billamount : 600
         * discount : 0
         * deliverycharges : 72
         * finalbill : 672
         * invoiceno : ST000200000000038
         * paymentmode : Cod
         * paymentsummary : Cod
         * paymentstatus : Success
         * orderstatus : Pending
         * expectedtime : 2020-05-13 11:00:00
         * buyerid : 20
         * fullname : suresh
         * mobile : 8985018103
         * fcmtoken : cdWQKWKO8QE:APA91bGSQI34R7YpU64eU3iHdLiQrzHJ3LQqC84CZ4u-_9xoCNfCPw5Q1J8cTt9bBbzIKGNuuOBn0qcPrF05E4jZunlIkWlU9vKC_uc93i8RIOe4HoGODc45brZrvVfCE3Qf897Hbp20
         */

        private String orderid;
        private String storeid;
        private String bookingdatetime;
        private String billamount;
        private String discount;
        private String deliverycharges;
        private String finalbill;
        private String invoiceno;
        private String paymentmode;
        private String paymentsummary;
        private String paymentstatus;
        private String orderstatus;
        private String expectedtime;
        private String buyerid;
        private String fullname;
        private String mobile;
        private String fcmtoken;

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getBookingdatetime() {
            return bookingdatetime;
        }

        public void setBookingdatetime(String bookingdatetime) {
            this.bookingdatetime = bookingdatetime;
        }

        public String getBillamount() {
            return billamount;
        }

        public void setBillamount(String billamount) {
            this.billamount = billamount;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getDeliverycharges() {
            return deliverycharges;
        }

        public void setDeliverycharges(String deliverycharges) {
            this.deliverycharges = deliverycharges;
        }

        public String getFinalbill() {
            return finalbill;
        }

        public void setFinalbill(String finalbill) {
            this.finalbill = finalbill;
        }

        public String getInvoiceno() {
            return invoiceno;
        }

        public void setInvoiceno(String invoiceno) {
            this.invoiceno = invoiceno;
        }

        public String getPaymentmode() {
            return paymentmode;
        }

        public void setPaymentmode(String paymentmode) {
            this.paymentmode = paymentmode;
        }

        public String getPaymentsummary() {
            return paymentsummary;
        }

        public void setPaymentsummary(String paymentsummary) {
            this.paymentsummary = paymentsummary;
        }

        public String getPaymentstatus() {
            return paymentstatus;
        }

        public void setPaymentstatus(String paymentstatus) {
            this.paymentstatus = paymentstatus;
        }

        public String getOrderstatus() {
            return orderstatus;
        }

        public void setOrderstatus(String orderstatus) {
            this.orderstatus = orderstatus;
        }

        public String getExpectedtime() {
            return expectedtime;
        }

        public void setExpectedtime(String expectedtime) {
            this.expectedtime = expectedtime;
        }

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getFcmtoken() {
            return fcmtoken;
        }

        public void setFcmtoken(String fcmtoken) {
            this.fcmtoken = fcmtoken;
        }
    }
}
