package com.pivotasoft.selltezvendor.Response;

import java.util.List;

public class LoginResponse {
    /**
     * storedata : [{"storeid":"2","title":"Satya Super Market","subtitle":"Grocceries and General Stores","owner":"Prakash Rao L","starttime":"07:00:00","endtime":"19:00:00","supportkm":"5","address":"Near Polamamba Temple Peda Waltair","city":"Visakhapatnam","latitude":"17.731","longitude":"83.3305","mobile":"9030200009","email":"piv01email@gmail.com","password":"cc03e747a6afbbcbf8be7668acfebee5","status":"1"}]
     * message : success
     */

    private String message;
    private List<StoredataBean> storedata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<StoredataBean> getStoredata() {
        return storedata;
    }

    public void setStoredata(List<StoredataBean> storedata) {
        this.storedata = storedata;
    }

    public static class StoredataBean {
        /**
         * storeid : 2
         * title : Satya Super Market
         * subtitle : Grocceries and General Stores
         * owner : Prakash Rao L
         * starttime : 07:00:00
         * endtime : 19:00:00
         * supportkm : 5
         * address : Near Polamamba Temple Peda Waltair
         * city : Visakhapatnam
         * latitude : 17.731
         * longitude : 83.3305
         * mobile : 9030200009
         * email : piv01email@gmail.com
         * password : cc03e747a6afbbcbf8be7668acfebee5
         * status : 1
         */

        private String storeid;
        private String title;
        private String subtitle;
        private String owner;
        private String starttime;
        private String endtime;
        private String supportkm;
        private String address;
        private String city;
        private String latitude;
        private String longitude;
        private String mobile;
        private String email;
        private String password;
        private String status;

        public String getStoreid() {
            return storeid;
        }

        public void setStoreid(String storeid) {
            this.storeid = storeid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getSupportkm() {
            return supportkm;
        }

        public void setSupportkm(String supportkm) {
            this.supportkm = supportkm;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }



    /* *//**
     * userdata : [{"buyerid":"1","fullname":"Eeswar T","mobile":"9160106499","email":"gangadhar.t9@gmail.com","password":"cc03e747a6afbbcbf8be7668acfebee5","status":"2","userkey":"oYyROWQzc2JxupBqAZLVCwmin3egKPf1"}]
     * message : success
     *//*

    private String message;
    private List<UserdataBean> userdata;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserdataBean> getUserdata() {
        return userdata;
    }

    public void setUserdata(List<UserdataBean> userdata) {
        this.userdata = userdata;
    }

    public static class UserdataBean {
        *//**
         * buyerid : 1
         * fullname : Eeswar T
         * mobile : 9160106499
         * email : gangadhar.t9@gmail.com
         * password : cc03e747a6afbbcbf8be7668acfebee5
         * status : 2
         * userkey : oYyROWQzc2JxupBqAZLVCwmin3egKPf1
         *//*

        private String buyerid;
        private String fullname;
        private String mobile;
        private String email;
        private String password;
        private String status;
        private String userkey;

        public String getBuyerid() {
            return buyerid;
        }

        public void setBuyerid(String buyerid) {
            this.buyerid = buyerid;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUserkey() {
            return userkey;
        }

        public void setUserkey(String userkey) {
            this.userkey = userkey;
        }
    }*/
}

