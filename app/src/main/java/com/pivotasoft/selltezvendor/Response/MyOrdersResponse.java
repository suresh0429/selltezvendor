package com.pivotasoft.selltezvendor.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrdersResponse {

    /**
     * status : 10100
     * message : Order List.
     * data : {"recordTotalCnt":46,"recordData":[{"id":"76","reference_id":"KM2019091906531823","order_id":"KM2019091906531840","final_price":"320","order_status":"Confirmed","order_date":"2019-09-19","order_time":"18:53:18","discount":"0","total_mrp_price":"320","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"73","reference_id":"KM2019091906112815","order_id":"KM2019091906112862","final_price":"320","order_status":"Confirmed","order_date":"2019-09-19","order_time":"18:11:28","discount":"0","total_mrp_price":"320","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"72","reference_id":"KM2019091906101153","order_id":"KM2019091906101153","final_price":"315","order_status":"Confirmed","order_date":"2019-09-19","order_time":"18:10:11","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"71","reference_id":"KM2019091911064031","order_id":"KM2019091911064073","final_price":"420","order_status":"Confirmed","order_date":"2019-09-19","order_time":"11:06:40","discount":"0","total_mrp_price":"420","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"70","reference_id":"KM2019091808043471","order_id":"KM2019091808043412","final_price":"300","order_status":"Confirmed","order_date":"2019-09-18","order_time":"20:04:34","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"69","reference_id":"KM2019091805504675","order_id":"KM2019091805504674","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:50:46","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"68","reference_id":"KM2019091805325260","order_id":"KM2019091805325226","final_price":"420","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:32:52","discount":"0","total_mrp_price":"420","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"67","reference_id":"KM2019091805110383","order_id":"KM2019091805110360","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:11:03","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"66","reference_id":"KM2019091805034711","order_id":"KM2019091805034761","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:03:47","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"65","reference_id":"KM2019091805032580","order_id":"KM2019091805032547","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:03:25","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"64","reference_id":"KM2019091805014719","order_id":"KM2019091805014736","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:01:47","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"63","reference_id":"KM2019091804591340","order_id":"KM2019091804591399","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"16:59:13","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"62","reference_id":"KM2019091804431313","order_id":"KM2019091804431397","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"16:43:13","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"61","reference_id":"KM2019091804422151","order_id":"KM2019091804422142","final_price":"360","order_status":"Confirmed","order_date":"2019-09-18","order_time":"16:42:21","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"59","reference_id":"KM2019091803441065","order_id":"KM2019091803441077","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"15:44:10","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Cash on Delivery"},{"id":"58","reference_id":"KM2019091803352786","order_id":"KM2019091803352769","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"15:35:27","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Cash on Delivery"},{"id":"57","reference_id":"KM2019091803111086","order_id":"KM2019091803111061","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"15:11:10","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"56","reference_id":"KM2019091802463058","order_id":"KM2019091802463084","final_price":"360","order_status":"Confirmed","order_date":"2019-09-18","order_time":"14:46:31","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"55","reference_id":"KM2019091810404041","order_id":"KM2019091810404069","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:40:40","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"54","reference_id":"KM2019091810243649","order_id":"KM2019091810243620","final_price":"300","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:24:36","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"53","reference_id":"KM2019091810234827","order_id":"KM2019091810234821","final_price":"360","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:23:48","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"52","reference_id":"KM2019091810172726","order_id":"KM2019091810172784","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:17:27","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"51","reference_id":"KM2019091810135560","order_id":"KM2019091810135589","final_price":"300","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:13:55","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"50","reference_id":"KM2019091706155952","order_id":"KM2019091706155933","final_price":"480","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:15:59","discount":"0","total_mrp_price":"480","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"49","reference_id":"KM2019091706154183","order_id":"KM2019091706154157","final_price":"360","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:15:41","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"48","reference_id":"KM2019091706152418","order_id":"KM2019091706152491","final_price":"405","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:15:24","discount":"0","total_mrp_price":"405","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"47","reference_id":"KM2019091706091559","order_id":"KM2019091706091541","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:09:15","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"46","reference_id":"KM2019091706080652","order_id":"KM2019091706080664","final_price":"360","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:08:06","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"45","reference_id":"KM2019091705560563","order_id":"KM2019091705560581","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:56:05","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"44","reference_id":"KM2019091705542790","order_id":"KM2019091705542726","final_price":"540","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:54:27","discount":"0","total_mrp_price":"540","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"43","reference_id":"KM2019091705534295","order_id":"KM2019091705534280","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:53:42","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"42","reference_id":"KM2019091705494733","order_id":"KM2019091705494738","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:49:47","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"41","reference_id":"KM2019091705380227","order_id":"KM2019091705380284","final_price":"375","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:38:02","discount":"0","total_mrp_price":"375","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"40","reference_id":"KM2019091703122733","order_id":"KM2019091703122748","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"15:12:27","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"39","reference_id":"KM2019091703045599","order_id":"KM2019091703045597","final_price":"465","order_status":"Confirmed","order_date":"2019-09-17","order_time":"15:04:56","discount":"0","total_mrp_price":"465","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"35","reference_id":"KM2019091709455451","order_id":"KM2019091709455477","final_price":"405","order_status":"Confirmed","order_date":"2019-09-17","order_time":"09:45:54","discount":"0","total_mrp_price":"405","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"34","reference_id":"KM2019091606401096","order_id":"KM2019091606401085","final_price":"315","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:40:10","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"33","reference_id":"KM2019091606384399","order_id":"KM2019091606384364","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:38:43","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"32","reference_id":"KM2019091606264599","order_id":"KM2019091606264545","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:26:45","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"31","reference_id":"KM2019091606070377","order_id":"KM2019091606070350","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:07:03","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"30","reference_id":"KM2019091606044992","order_id":"KM2019091606044933","final_price":"375","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:04:49","discount":"0","total_mrp_price":"375","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"29","reference_id":"KM2019091605343718","order_id":"KM2019091605343727","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"17:34:37","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"28","reference_id":"KM2019091605335841","order_id":"KM2019091605335887","final_price":"430","order_status":"Confirmed","order_date":"2019-09-16","order_time":"17:33:58","discount":"0","total_mrp_price":"430","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"23","reference_id":"KM2019091604372857","order_id":"KM2019091604372827","final_price":"630","order_status":"Confirmed","order_date":"2019-09-16","order_time":"16:37:28","discount":"0","total_mrp_price":"630","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Cash on Delivery"},{"id":"16","reference_id":"KM2019091604332092","order_id":"KM2019091604332029","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"16:33:20","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"6","reference_id":"KM2019091604090728","order_id":"KM2019091604090787","final_price":"450","order_status":"Confirmed","order_date":"2019-09-16","order_time":"16:09:07","discount":"0","total_mrp_price":"450","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"}]}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * recordTotalCnt : 46
         * recordData : [{"id":"76","reference_id":"KM2019091906531823","order_id":"KM2019091906531840","final_price":"320","order_status":"Confirmed","order_date":"2019-09-19","order_time":"18:53:18","discount":"0","total_mrp_price":"320","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"73","reference_id":"KM2019091906112815","order_id":"KM2019091906112862","final_price":"320","order_status":"Confirmed","order_date":"2019-09-19","order_time":"18:11:28","discount":"0","total_mrp_price":"320","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"72","reference_id":"KM2019091906101153","order_id":"KM2019091906101153","final_price":"315","order_status":"Confirmed","order_date":"2019-09-19","order_time":"18:10:11","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"71","reference_id":"KM2019091911064031","order_id":"KM2019091911064073","final_price":"420","order_status":"Confirmed","order_date":"2019-09-19","order_time":"11:06:40","discount":"0","total_mrp_price":"420","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"70","reference_id":"KM2019091808043471","order_id":"KM2019091808043412","final_price":"300","order_status":"Confirmed","order_date":"2019-09-18","order_time":"20:04:34","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"69","reference_id":"KM2019091805504675","order_id":"KM2019091805504674","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:50:46","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"68","reference_id":"KM2019091805325260","order_id":"KM2019091805325226","final_price":"420","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:32:52","discount":"0","total_mrp_price":"420","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"67","reference_id":"KM2019091805110383","order_id":"KM2019091805110360","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:11:03","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"66","reference_id":"KM2019091805034711","order_id":"KM2019091805034761","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:03:47","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"65","reference_id":"KM2019091805032580","order_id":"KM2019091805032547","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:03:25","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"64","reference_id":"KM2019091805014719","order_id":"KM2019091805014736","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"17:01:47","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"63","reference_id":"KM2019091804591340","order_id":"KM2019091804591399","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"16:59:13","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"62","reference_id":"KM2019091804431313","order_id":"KM2019091804431397","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"16:43:13","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"61","reference_id":"KM2019091804422151","order_id":"KM2019091804422142","final_price":"360","order_status":"Confirmed","order_date":"2019-09-18","order_time":"16:42:21","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"59","reference_id":"KM2019091803441065","order_id":"KM2019091803441077","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"15:44:10","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Cash on Delivery"},{"id":"58","reference_id":"KM2019091803352786","order_id":"KM2019091803352769","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"15:35:27","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Cash on Delivery"},{"id":"57","reference_id":"KM2019091803111086","order_id":"KM2019091803111061","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"15:11:10","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"56","reference_id":"KM2019091802463058","order_id":"KM2019091802463084","final_price":"360","order_status":"Confirmed","order_date":"2019-09-18","order_time":"14:46:31","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"55","reference_id":"KM2019091810404041","order_id":"KM2019091810404069","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:40:40","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"54","reference_id":"KM2019091810243649","order_id":"KM2019091810243620","final_price":"300","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:24:36","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"53","reference_id":"KM2019091810234827","order_id":"KM2019091810234821","final_price":"360","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:23:48","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"52","reference_id":"KM2019091810172726","order_id":"KM2019091810172784","final_price":"315","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:17:27","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"51","reference_id":"KM2019091810135560","order_id":"KM2019091810135589","final_price":"300","order_status":"Confirmed","order_date":"2019-09-18","order_time":"10:13:55","discount":"0","total_mrp_price":"300","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"50","reference_id":"KM2019091706155952","order_id":"KM2019091706155933","final_price":"480","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:15:59","discount":"0","total_mrp_price":"480","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"49","reference_id":"KM2019091706154183","order_id":"KM2019091706154157","final_price":"360","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:15:41","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"48","reference_id":"KM2019091706152418","order_id":"KM2019091706152491","final_price":"405","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:15:24","discount":"0","total_mrp_price":"405","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"47","reference_id":"KM2019091706091559","order_id":"KM2019091706091541","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:09:15","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"46","reference_id":"KM2019091706080652","order_id":"KM2019091706080664","final_price":"360","order_status":"Confirmed","order_date":"2019-09-17","order_time":"18:08:06","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"45","reference_id":"KM2019091705560563","order_id":"KM2019091705560581","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:56:05","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"44","reference_id":"KM2019091705542790","order_id":"KM2019091705542726","final_price":"540","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:54:27","discount":"0","total_mrp_price":"540","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"43","reference_id":"KM2019091705534295","order_id":"KM2019091705534280","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:53:42","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"42","reference_id":"KM2019091705494733","order_id":"KM2019091705494738","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:49:47","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"41","reference_id":"KM2019091705380227","order_id":"KM2019091705380284","final_price":"375","order_status":"Confirmed","order_date":"2019-09-17","order_time":"17:38:02","discount":"0","total_mrp_price":"375","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"40","reference_id":"KM2019091703122733","order_id":"KM2019091703122748","final_price":"315","order_status":"Confirmed","order_date":"2019-09-17","order_time":"15:12:27","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"39","reference_id":"KM2019091703045599","order_id":"KM2019091703045597","final_price":"465","order_status":"Confirmed","order_date":"2019-09-17","order_time":"15:04:56","discount":"0","total_mrp_price":"465","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"35","reference_id":"KM2019091709455451","order_id":"KM2019091709455477","final_price":"405","order_status":"Confirmed","order_date":"2019-09-17","order_time":"09:45:54","discount":"0","total_mrp_price":"405","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"34","reference_id":"KM2019091606401096","order_id":"KM2019091606401085","final_price":"315","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:40:10","discount":"0","total_mrp_price":"315","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"33","reference_id":"KM2019091606384399","order_id":"KM2019091606384364","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:38:43","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"32","reference_id":"KM2019091606264599","order_id":"KM2019091606264545","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:26:45","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"31","reference_id":"KM2019091606070377","order_id":"KM2019091606070350","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:07:03","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"30","reference_id":"KM2019091606044992","order_id":"KM2019091606044933","final_price":"375","order_status":"Confirmed","order_date":"2019-09-16","order_time":"18:04:49","discount":"0","total_mrp_price":"375","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"29","reference_id":"KM2019091605343718","order_id":"KM2019091605343727","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"17:34:37","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"28","reference_id":"KM2019091605335841","order_id":"KM2019091605335887","final_price":"430","order_status":"Confirmed","order_date":"2019-09-16","order_time":"17:33:58","discount":"0","total_mrp_price":"430","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"23","reference_id":"KM2019091604372857","order_id":"KM2019091604372827","final_price":"630","order_status":"Confirmed","order_date":"2019-09-16","order_time":"16:37:28","discount":"0","total_mrp_price":"630","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Cash on Delivery"},{"id":"16","reference_id":"KM2019091604332092","order_id":"KM2019091604332029","final_price":"360","order_status":"Confirmed","order_date":"2019-09-16","order_time":"16:33:20","discount":"0","total_mrp_price":"360","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"},{"id":"6","reference_id":"KM2019091604090728","order_id":"KM2019091604090787","final_price":"450","order_status":"Confirmed","order_date":"2019-09-16","order_time":"16:09:07","discount":"0","total_mrp_price":"450","coupon_code":null,"status":"1","shipping_charges":"0","payment_status":"Pay on Delivery"}]
         */

        @SerializedName("recordTotalCnt")
        private int recordTotalCnt;
        @SerializedName("recordData")
        private List<RecordDataBean> recordData;

        public int getRecordTotalCnt() {
            return recordTotalCnt;
        }

        public void setRecordTotalCnt(int recordTotalCnt) {
            this.recordTotalCnt = recordTotalCnt;
        }

        public List<RecordDataBean> getRecordData() {
            return recordData;
        }

        public void setRecordData(List<RecordDataBean> recordData) {
            this.recordData = recordData;
        }

        public static class RecordDataBean {
            /**
             * id : 76
             * reference_id : KM2019091906531823
             * order_id : KM2019091906531840
             * final_price : 320
             * order_status : Confirmed
             * order_date : 2019-09-19
             * order_time : 18:53:18
             * discount : 0
             * total_mrp_price : 320
             * coupon_code : null
             * status : 1
             * shipping_charges : 0
             * payment_status : Pay on Delivery
             */

            @SerializedName("id")
            private String id;
            @SerializedName("reference_id")
            private String referenceId;
            @SerializedName("order_id")
            private String orderId;
            @SerializedName("final_price")
            private String finalPrice;
            @SerializedName("order_status")
            private String orderStatus;
            @SerializedName("order_date")
            private String orderDate;
            @SerializedName("order_time")
            private String orderTime;
            @SerializedName("discount")
            private String discount;
            @SerializedName("total_mrp_price")
            private String totalMrpPrice;
            @SerializedName("coupon_code")
            private Object couponCode;
            @SerializedName("status")
            private String status;
            @SerializedName("shipping_charges")
            private String shippingCharges;
            @SerializedName("payment_status")
            private String paymentStatus;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getReferenceId() {
                return referenceId;
            }

            public void setReferenceId(String referenceId) {
                this.referenceId = referenceId;
            }

            public String getOrderId() {
                return orderId;
            }

            public void setOrderId(String orderId) {
                this.orderId = orderId;
            }

            public String getFinalPrice() {
                return finalPrice;
            }

            public void setFinalPrice(String finalPrice) {
                this.finalPrice = finalPrice;
            }

            public String getOrderStatus() {
                return orderStatus;
            }

            public void setOrderStatus(String orderStatus) {
                this.orderStatus = orderStatus;
            }

            public String getOrderDate() {
                return orderDate;
            }

            public void setOrderDate(String orderDate) {
                this.orderDate = orderDate;
            }

            public String getOrderTime() {
                return orderTime;
            }

            public void setOrderTime(String orderTime) {
                this.orderTime = orderTime;
            }

            public String getDiscount() {
                return discount;
            }

            public void setDiscount(String discount) {
                this.discount = discount;
            }

            public String getTotalMrpPrice() {
                return totalMrpPrice;
            }

            public void setTotalMrpPrice(String totalMrpPrice) {
                this.totalMrpPrice = totalMrpPrice;
            }

            public Object getCouponCode() {
                return couponCode;
            }

            public void setCouponCode(Object couponCode) {
                this.couponCode = couponCode;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getShippingCharges() {
                return shippingCharges;
            }

            public void setShippingCharges(String shippingCharges) {
                this.shippingCharges = shippingCharges;
            }

            public String getPaymentStatus() {
                return paymentStatus;
            }

            public void setPaymentStatus(String paymentStatus) {
                this.paymentStatus = paymentStatus;
            }
        }
    }
}