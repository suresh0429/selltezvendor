package com.pivotasoft.selltezvendor.Model;

public class AddressModelItem {
    private String id;
    private String name;
    private String address_line1;
    private String address_line2;
    private String area;
    private String city;
    private String state;
    private String pincode;
    private String country;
    private String contact_no;
    private String alternate_contact_no;
    private String latitude;
    private String longitude;
    private String is_default;
    private boolean delivery_status;
    private String delivery_charges;

    public AddressModelItem(String id, String name, String address_line1, String address_line2, String area, String city, String state, String pincode, String country, String contact_no, String alternate_contact_no, String latitude, String longitude, String is_default, boolean delivery_status, String delivery_charges) {
        this.id = id;
        this.name = name;
        this.address_line1 = address_line1;
        this.address_line2 = address_line2;
        this.area = area;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.country = country;
        this.contact_no = contact_no;
        this.alternate_contact_no = alternate_contact_no;
        this.latitude = latitude;
        this.longitude = longitude;
        this.is_default = is_default;
        this.delivery_status = delivery_status;
        this.delivery_charges = delivery_charges;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress_line1() {
        return address_line1;
    }

    public void setAddress_line1(String address_line1) {
        this.address_line1 = address_line1;
    }

    public String getAddress_line2() {
        return address_line2;
    }

    public void setAddress_line2(String address_line2) {
        this.address_line2 = address_line2;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getAlternate_contact_no() {
        return alternate_contact_no;
    }

    public void setAlternate_contact_no(String alternate_contact_no) {
        this.alternate_contact_no = alternate_contact_no;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIs_default() {
        return is_default;
    }

    public void setIs_default(String is_default) {
        this.is_default = is_default;
    }

    public boolean isDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(boolean delivery_status) {
        this.delivery_status = delivery_status;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }
}
