package com.pivotasoft.selltezvendor.Model;

import java.util.ArrayList;

/**
 * Created by pratap.kesaboyina on 30-11-2015.
 */
public class HeaderSectionDataModel {



    private String headerTitle;
    private String categoryId;
    private String subcategoryId;
    private String storeId;
    private ArrayList<SingleItemModel> allItemsInSection;


    public HeaderSectionDataModel() {

    }

    public HeaderSectionDataModel(String headerTitle, String categoryId, String subcategoryId, String storeId, ArrayList<SingleItemModel> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.categoryId = categoryId;
        this.subcategoryId = subcategoryId;
        this.storeId = storeId;
        this.allItemsInSection = allItemsInSection;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public ArrayList<SingleItemModel> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<SingleItemModel> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }
}
