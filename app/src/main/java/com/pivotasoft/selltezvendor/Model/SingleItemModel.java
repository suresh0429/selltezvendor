package com.pivotasoft.selltezvendor.Model;

/**
 * Created by pratap.kesaboyina on 01-12-2015.
 */
public class SingleItemModel {


    private String productid;
    private String productpic;
    private String title;
    private String measureunits;
    private String unitprice;
    private String discount;
    private String finalprice;
    private String description;
    private String subcategoryid;
    private String storeid;
    private String status;


    public SingleItemModel(String productid, String productpic, String title, String measureunits, String unitprice, String discount, String finalprice, String description, String subcategoryid, String storeid,String  status) {
        this.productid = productid;
        this.productpic = productpic;
        this.title = title;
        this.measureunits = measureunits;
        this.unitprice = unitprice;
        this.discount = discount;
        this.finalprice = finalprice;
        this.description = description;
        this.subcategoryid = subcategoryid;
        this.storeid = storeid;
        this.status = status;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductpic() {
        return productpic;
    }

    public void setProductpic(String productpic) {
        this.productpic = productpic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMeasureunits() {
        return measureunits;
    }

    public void setMeasureunits(String measureunits) {
        this.measureunits = measureunits;
    }

    public String getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getFinalprice() {
        return finalprice;
    }

    public void setFinalprice(String finalprice) {
        this.finalprice = finalprice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubcategoryid() {
        return subcategoryid;
    }

    public void setSubcategoryid(String subcategoryid) {
        this.subcategoryid = subcategoryid;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
