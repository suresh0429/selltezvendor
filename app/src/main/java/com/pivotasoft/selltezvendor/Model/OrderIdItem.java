package com.pivotasoft.selltezvendor.Model;

public class OrderIdItem {

    private String orderid;
    private String storeid;
    private String bookingdatetime;
    private String billamount;
    private String discount;
    private String deliverycharges;
    private String finalbill;
    private String invoiceno;
    private String paymentmode;
    private String paymentsummary;
    private String paymentstatus;
    private String orderstatus;
    private String expectedtime;
    private String buyerid;
    private String fullname;
    private String mobile;
    private String fcmtoken;

    public OrderIdItem(String orderid, String storeid, String bookingdatetime, String billamount, String discount, String deliverycharges, String finalbill, String invoiceno, String paymentmode, String paymentsummary, String paymentstatus, String orderstatus, String expectedtime, String buyerid, String fullname, String mobile, String fcmtoken) {
        this.orderid = orderid;
        this.storeid = storeid;
        this.bookingdatetime = bookingdatetime;
        this.billamount = billamount;
        this.discount = discount;
        this.deliverycharges = deliverycharges;
        this.finalbill = finalbill;
        this.invoiceno = invoiceno;
        this.paymentmode = paymentmode;
        this.paymentsummary = paymentsummary;
        this.paymentstatus = paymentstatus;
        this.orderstatus = orderstatus;
        this.expectedtime = expectedtime;
        this.buyerid = buyerid;
        this.fullname = fullname;
        this.mobile = mobile;
        this.fcmtoken = fcmtoken;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getBookingdatetime() {
        return bookingdatetime;
    }

    public void setBookingdatetime(String bookingdatetime) {
        this.bookingdatetime = bookingdatetime;
    }

    public String getBillamount() {
        return billamount;
    }

    public void setBillamount(String billamount) {
        this.billamount = billamount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDeliverycharges() {
        return deliverycharges;
    }

    public void setDeliverycharges(String deliverycharges) {
        this.deliverycharges = deliverycharges;
    }

    public String getFinalbill() {
        return finalbill;
    }

    public void setFinalbill(String finalbill) {
        this.finalbill = finalbill;
    }

    public String getInvoiceno() {
        return invoiceno;
    }

    public void setInvoiceno(String invoiceno) {
        this.invoiceno = invoiceno;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getPaymentsummary() {
        return paymentsummary;
    }

    public void setPaymentsummary(String paymentsummary) {
        this.paymentsummary = paymentsummary;
    }

    public String getPaymentstatus() {
        return paymentstatus;
    }

    public void setPaymentstatus(String paymentstatus) {
        this.paymentstatus = paymentstatus;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

    public String getExpectedtime() {
        return expectedtime;
    }

    public void setExpectedtime(String expectedtime) {
        this.expectedtime = expectedtime;
    }

    public String getBuyerid() {
        return buyerid;
    }

    public void setBuyerid(String buyerid) {
        this.buyerid = buyerid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFcmtoken() {
        return fcmtoken;
    }

    public void setFcmtoken(String fcmtoken) {
        this.fcmtoken = fcmtoken;
    }
}
