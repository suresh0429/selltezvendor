package com.pivotasoft.selltezvendor;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Response.BaseResponse;
import com.pivotasoft.selltezvendor.Storage.PrefManager;
import com.pivotasoft.selltezvendor.Storage.Utilities;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOtpActivity extends Activity {
    private static final String PREFS_LOCATION = "LOCATION_PREF";
    public static String TAG ="VerifyOtpActivity";

        @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.logo_img)
    ImageView logoImg;
    @BindView(R.id.otp_txt)
    TextView otpTxt;
    @BindView(R.id.pinview)
    Pinview pinview;
    @BindView(R.id.verify_btn)
    Button verifyBtn;
    @BindView(R.id.txtresend)
    TextView txtresend;
    String mno,deviceID,name,email,password;
    PrefManager session;

    String fcmToken;
    private boolean doubleBackToExitPressedOnce = false;
    int code,otp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        session=new PrefManager(VerifyOtpActivity.this);

        deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        if (getIntent() != null) {
            mno = getIntent().getStringExtra("MobileNumber");
            name = getIntent().getStringExtra("name");
            email = getIntent().getStringExtra("email");
            password = getIntent().getStringExtra("password");
            otpSend(mno);
        }


        // fcm Token
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(VerifyOtpActivity.this,new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmToken = instanceIdResult.getToken();
                Log.e("TOKEN",""+fcmToken);
            }
        });

    }
    private void otpSend(String mobile) {
        code = new Random().nextInt(900000) + 100000;
        Log.d(TAG, "otpSend: "+code);
        String message =  "Thank you for choosing Selltez. Your registration OTP is "+code;
        String urlString = "http://app.smsmoon.com/submitsms.jsp?user=PIVOTAL&key=7d9a0596c8XX&mobile="+mobile+"&message="+message+"&senderid=PVOTAL&accusage=1";
        WebView webView = new WebView(getApplicationContext());
        webView.loadUrl(urlString);

        Toasty.success(getApplicationContext(),"OTP sent to your registered mobile no.",Toast.LENGTH_SHORT).show();
    }

    @OnClick({ R.id.otp_txt, R.id.pinview, R.id.verify_btn, R.id.txtresend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.verify_btn:

                otp= Integer.parseInt(pinview.getValue());
                Log.d(TAG, "onViewClicked: "+otp);

                if (code==otp){
                    progressBar.setVisibility(View.VISIBLE);
                    Call<ResponseBody> call = RetrofitClient.getInstance().getApi().userRegistrationRequest(name,email,mno,password);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            progressBar.setVisibility(View.GONE);
                            ResponseBody registerResponse = response.body();

                            if (response.isSuccessful()){
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                                        Toasty.success(getApplicationContext(),"Registration Success !",Toast.LENGTH_SHORT).show();

                                    }
                                }, 1000);

                            }else {
                                Toasty.error(getApplicationContext(),"Registration Failed !",Toast.LENGTH_SHORT).show();
                            }



                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }else {
                    Toasty.normal(getApplicationContext(),"OTP Invalid.",Toast.LENGTH_SHORT).show();
                }



                break;
            case R.id.txtresend:
               // progressBar.setVisibility(View.VISIBLE);
                otpSend(mno);

                break;
        }
    }


    public void setDefaults() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true);
        editor.putString(Utilities.KEY_AREA,"");
        editor.putString(Utilities.KEY_PINCODE,"");
        editor.putString(Utilities.KEY_SHIPPINGCHARGES,"");
        editor.apply();
    }
    // update Fcm Token
    /*private void updateFcmToken(String jwt, String user_id) {

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().updateFcmTocken(jwt,user_id,fcmToken);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {


                progressBar.setVisibility(View.GONE);


                BaseResponse loginResponse = response.body();


                if (loginResponse.getStatus().equals("10100")) {

                   // Toast.makeText(VerifyOtpActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: "+loginResponse.getMessage());

                }else if (loginResponse.getStatus().equals("10200")){
                    Log.d(TAG, "onResponse: "+loginResponse.getMessage());
                }
                else if (loginResponse.getStatus().equals("10300")){
                    Log.d(TAG, "onResponse: "+loginResponse.getMessage());
                }
                else if (loginResponse.getStatus().equals("10400")){
                    Log.d(TAG, "onResponse: "+loginResponse.getMessage());
                }



            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }*/

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toasty.normal(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);

    }
}
