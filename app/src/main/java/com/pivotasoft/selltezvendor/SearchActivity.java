package com.pivotasoft.selltezvendor;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.pivotasoft.selltezvendor.Adapters.SearchAdapter;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Response.ProductResponse;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchActivity extends Activity {

    @BindView(R.id.search)
    SearchView search;
    @BindView(R.id.rcProduct)
    RecyclerView rcProduct;
    @BindView(R.id.txtNodata)
    TextView txtNodata;
    @BindView(R.id.txtSearch)
    TextView txtSearch;
    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private SearchAdapter adapter;

    private PrefManager pref;
    String userid,tokenValue,deviceId,storeId,storeName;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);


        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userid = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (getIntent() != null) {
            storeId = getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        search.setActivated(true);
        search.setQueryHint("Type your keyword here");
        search.onActionViewExpanded();
        search.setIconified(false);


        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                Log.e("QUERY :", "" + query);

                adapter.getFilter().filter(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
              /*  if (search.getQuery().length() == 0) {

                   // prepareProductData(query,"");

                }*/

                adapter.getFilter().filter(query);
                return false;
            }
        });


        search.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                search.setQuery("", false);
                adapter.notifyDataSetChanged();
               // prepareProductData("","HideRecycler");
                return false;
            }
        });


        prepareProductData();



    }




    private void prepareProductData() {



        progressBar.setVisibility(View.VISIBLE);
        Call<ProductResponse> call = RetrofitClient.getInstance().getApi().searchProducts(tokenValue,storeId);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, retrofit2.Response<ProductResponse> response) {
                progressBar.setVisibility(View.GONE);

                ProductResponse addressResponse = response.body();

                if (response.isSuccessful()){

                    List<ProductResponse.ProductsdataBean> docsBeanList = response.body() != null ? response.body().getProductsdata() : null;

                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                    rcProduct.setLayoutManager(mLayoutManager);
                    rcProduct.setItemAnimator(new DefaultItemAnimator());


                    adapter = new SearchAdapter(getApplicationContext(), docsBeanList,storeId,storeName);
                    rcProduct.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                }

               /* if (response.isSuccessful()) {

                    List<ProductResponse.ProductsdataBean> docsBeanList = response.body() != null ? response.body().getProductsdata() : null;

                    if (addressResponse.getStatus().equalsIgnoreCase("10100")) {

                        txtSearch.setVisibility(View.VISIBLE);
                        txtNodata.setVisibility(View.GONE);
                        rcProduct.setVisibility(View.VISIBLE);
                        txtSearch.setText("Search results of " + query);


                        if (hideRecycler.equalsIgnoreCase("HideRecycler")){
                            rcProduct.setVisibility(View.GONE);
                        }else {

                            // homeKitchen Adapter
                            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                            rcProduct.setLayoutManager(mLayoutManager);
                            rcProduct.setItemAnimator(new DefaultItemAnimator());


                            adapter = new SearchAdapter(getApplicationContext(), docsBeanList);
                            rcProduct.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }

                    } else if (addressResponse.getStatus().equalsIgnoreCase("10300")){

                        txtNodata.setVisibility(View.VISIBLE);
                        txtSearch.setVisibility(View.GONE);
                        rcProduct.setVisibility(View.GONE);
                        if (finalSearchQuery.length() == 0 || finalSearchQuery.isEmpty() || finalSearchQuery.equalsIgnoreCase("")) {
                            txtNodata.setVisibility(View.GONE);
                            rcProduct.setVisibility(View.GONE);
                        } else {
                            txtNodata.setText("Your search " + query + " did not match any products !");
                        }

                    }else {


                    }

                }else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Snackbar.make(rcProduct, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.not_found)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Snackbar.make(rcProduct, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.server_broken)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                        default:
                            Snackbar.make(rcProduct, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.unknown_error)+"</font>"),Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }*/

            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(rcProduct, Html.fromHtml("<font color=\""+Color.RED+"\">"+ getResources().getString(R.string.slowInternetconnection)+"</font>"),Snackbar.LENGTH_SHORT).show();

            }
        });


    }


    // validate name
    private boolean isValidQuery(String name) {

         if (name.length() < 3 ) {

            return false;
        }else {
            // prepareProductData(name, "HideRecycler");
         }
        return true;
    }



    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
