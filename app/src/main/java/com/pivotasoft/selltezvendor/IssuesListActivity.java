package com.pivotasoft.selltezvendor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pivotasoft.selltezvendor.Adapters.IssuesListAdapter;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Interface.IssuesInterface;
import com.pivotasoft.selltezvendor.Response.IssuesListResponse;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssuesListActivity extends AppCompatActivity implements IssuesInterface {


    String orderId;
    AppController appController;
    @BindView(R.id.recyclerIssues)
    RecyclerView recyclerIssues;
    @BindView(R.id.txtError)
    TextView txtError;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PrefManager pref;
    String userId, tokenValue, deviceId;
    IssuesInterface issuesInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issues_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Issues");

        appController = (AppController) getApplication();
        issuesInterface = (IssuesInterface)this;
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");


        if (getIntent() != null) {
            orderId = getIntent().getStringExtra("Order_ID");
            Log.d("TAG", "onCreate: " + orderId);
        }

        if (appController.isConnection()) {

            getIssues();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;


        }

        return super.onOptionsItemSelected(item);
    }


    private void getIssues() {

        progressBar.setVisibility(View.VISIBLE);
        Call<IssuesListResponse> call = RetrofitClient.getInstance().getApi().issuesList(tokenValue, userId);
        call.enqueue(new Callback<IssuesListResponse>() {
            @Override
            public void onResponse(Call<IssuesListResponse> call, Response<IssuesListResponse> response) {
                if (response.isSuccessful()) {

                    progressBar.setVisibility(View.GONE);
                    IssuesListResponse myOrdersResponse = response.body();

                    List<IssuesListResponse.StoreissuelistdataBean> modulesBeanList = response.body().getStoreissuelistdata();

                    if (modulesBeanList.size() != 0) {
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerIssues.setLayoutManager(layoutManager);
                        recyclerIssues.setItemAnimator(new DefaultItemAnimator());

                        IssuesListAdapter myOrderAdapter = new IssuesListAdapter(IssuesListActivity.this, modulesBeanList,issuesInterface);
                        recyclerIssues.setAdapter(myOrderAdapter);
                        myOrderAdapter.notifyDataSetChanged();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        Toasty.normal(IssuesListActivity.this, myOrdersResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("No Issues Found !");
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Issues Found !");
                }
            }

            @Override
            public void onFailure(Call<IssuesListResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toasty.normal(IssuesListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void issueModify(final IssuesListResponse.StoreissuelistdataBean storeissuelistdataBean) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.diloge_issues, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();


        final EditText etDescription = (EditText) dialogView.findViewById(R.id.etDescription);
        Button buttonOk = (Button) dialogView.findViewById(R.id.btnsubmit);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!etDescription.getText().toString().isEmpty()) {

                    progressBar.setVisibility(View.VISIBLE);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    String currentDateandTime = sdf.format(new Date());

                    Call<ResponseBody> call = RetrofitClient.getInstance().getApi().issueresponse(tokenValue,storeissuelistdataBean.getIssueid(),etDescription.getText().toString());
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {
                                progressBar.setVisibility(View.GONE);
                                ResponseBody myOrdersResponse = response.body();

                                Toasty.success(getApplicationContext(), "Issue Resolved Successfully.", Toast.LENGTH_SHORT).show();
                                keypadHide(IssuesListActivity.this);
                                getIssues();
                                alertDialog.dismiss();

                            } else {
                                progressBar.setVisibility(View.GONE);

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            Toasty.normal(IssuesListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                } else {
                    Toasty.normal(getApplicationContext(), "Enter Issue !", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public static void keypadHide(Activity context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(Objects.requireNonNull(context.getCurrentFocus()).getWindowToken(), 0);
        } catch (Exception ignored) {

        }
    }
}
