package com.pivotasoft.selltezvendor;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Reciever.ConnectivityReceiver;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateProfileActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    @BindView(R.id.etUsername)
    TextInputEditText etUsername;
    @BindView(R.id.user_til)
    TextInputLayout userTil;
    @BindView(R.id.etPhone)
    TextInputEditText etPhone;
    @BindView(R.id.mobile_til)
    TextInputLayout mobileTil;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.email_til)
    TextInputLayout emailTil;
    @BindView(R.id.btnUpdate)
    Button btnUpdate;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.tilPassword_til)
    TextInputLayout tilPasswordTil;
    private PrefManager pref;

    RadioButton radioButton;

    String userId, tokenValue, deviceId, ownerName,refreshToken;
    AppController app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Update Profile");

        app = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());


        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        String username = profile.get("name");
        String mobile = profile.get("mobile");
        String email = profile.get("email");
        ownerName = profile.get("ownerName");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");
        refreshToken = profile.get("keyFcm");

        etUsername.setText(username);
        etPhone.setText(mobile);
        etEmail.setText(email);

        etUsername.addTextChangedListener(new MyTextWatcher(etUsername));
        etPhone.addTextChangedListener(new MyTextWatcher(etPhone));
        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));


    }


    @OnClick(R.id.btnUpdate)
    public void onViewClicked() {
        boolean isConnected = app.isConnection();
        if (isConnected) {
            updateProfile();
        } else {

            String message = "Sorry! Not connected to internet";
            int color = Color.RED;
            snackBar(message, color);
        }

    }

    private void updateProfile() {

        final String name = etUsername.getText().toString().trim();
        final String mobile = etPhone.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();
        final String newPassword = etPassword.getText().toString().trim();


        if ((!isValidName(name))) {
            return;
        }
        if ((!isValidPhoneNumber(mobile))) {
            return;
        }
        if ((!isValidEmail(email))) {
            return;
        }
        if (newPassword.isEmpty()){
            Toasty.error(getApplicationContext(),"Please Enter New Password",Toast.LENGTH_SHORT).show();
            return;
        }


        progressBar.setVisibility(View.VISIBLE);
        btnUpdate.setEnabled(false);

        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updateProfile(tokenValue, userId, ownerName,name, email);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

               /* progressBar.setVisibility(View.GONE);
                btnUpdate.setEnabled(true);*/

                ResponseBody registerResponse = response.body();

                if (response.isSuccessful()) {

                    changePassword(newPassword,mobile);

                   /* new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent intent = new Intent(getApplicationContext(), MyAccountActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            pref.createLogin(userId, etUsername.getText().toString(), etEmail.getText().toString(), mobile, "", tokenValue);


                        }
                    }, 1000);*/

                    Toasty.success(UpdateProfileActivity.this, "Profile Updated Successfully.", Toast.LENGTH_SHORT).show();

                } else {
                    Toasty.normal(UpdateProfileActivity.this, "Server error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnUpdate.setEnabled(true);
            }
        });
    }

    private void changePassword(String newPassword, final String mobile){
        Call<ResponseBody> call = RetrofitClient.getInstance().getApi().updatePassword(tokenValue,userId,newPassword);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressBar.setVisibility(View.GONE);
                btnUpdate.setEnabled(true);
                ResponseBody baseResponse = response.body();

                if (response.isSuccessful()) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent intent = new Intent(getApplicationContext(), MyAccountActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            pref.createLogin(userId, etUsername.getText().toString(), etEmail.getText().toString(), mobile, "", tokenValue,ownerName,refreshToken);


                        }
                    }, 2000);

                    // update session
                    //session.createLogin(userId, username, email, mobile,null,deviceId,tokenValue,gender);

                    Toasty.success(getApplicationContext(), "Profile Update Success", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }


    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    // validate name
    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            userTil.setError("name is required");
            requestFocus(etUsername);
            return false;
        } else if (!matcher.matches()) {
            userTil.setError("Enter Alphabets Only");
            requestFocus(etUsername);
            return false;
        } else if (name.length() < 5 || name.length() > 20) {
            userTil.setError("Name Should be 5 to 20 characters");
            requestFocus(etUsername);
            return false;
        } else {
            userTil.setErrorEnabled(false);
        }
        return matcher.matches();
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTil.setError("Phone no is required");
            requestFocus(etPhone);

            return false;
        } else if (!matcher.matches()) {
            mobileTil.setError("Enter a valid mobile");
            requestFocus(etPhone);
            return false;
        } else {
            mobileTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }


    // validate your email address
    private boolean isValidEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        if (email.isEmpty()) {
            emailTil.setError("Email is required");
            requestFocus(etEmail);
            return false;
        } else if (!matcher.matches()) {
            emailTil.setError("Enter a valid email");
            requestFocus(etEmail);
            return false;
        } else {
            emailTil.setErrorEnabled(false);
        }


        return matcher.matches();
    }


    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etUsername:
                    isValidName(etUsername.getText().toString().trim());
                    break;
                case R.id.etPhone:
                    isValidPhoneNumber(etPhone.getText().toString().trim());
                    break;

                case R.id.etEmail:
                    isValidEmail(etEmail.getText().toString().trim());
                    break;

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
