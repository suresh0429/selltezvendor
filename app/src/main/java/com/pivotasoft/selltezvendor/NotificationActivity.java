package com.pivotasoft.selltezvendor;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pivotasoft.selltezvendor.Adapters.NotificationsAdapter;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Response.NotificationsResponse;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {
    @BindView(R.id.notifction_recycleview)
    RecyclerView notifctionRecycleview;
    String userId,email, tokenValue, deviceId;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PrefManager pref;
    NotificationsAdapter notificationsAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notifications");



        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        email = profile.get("email");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        Call<NotificationsResponse> call = RetrofitClient.getInstance().getApi().notification("0","1234567890", userId);
        call.enqueue(new Callback<NotificationsResponse>() {
            @Override
            public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                if (response.isSuccessful()) {
                    NotificationsResponse notificationsResponse = response.body();
                    progressBar.setVisibility(View.GONE);
//                    Toast.makeText(NotificationActivity.this, notificationsResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    List<NotificationsResponse.NotificationsDataBean> dataBeans = notificationsResponse.getNotifications_data();

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    notifctionRecycleview.setLayoutManager(layoutManager);
                    notifctionRecycleview.setItemAnimator(new DefaultItemAnimator());
                    notificationsAdapter = new NotificationsAdapter(NotificationActivity.this, dataBeans);
                    notifctionRecycleview.setAdapter(notificationsAdapter);
                } else {
                    Toasty.error(NotificationActivity.this, "No Notifications Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(NotificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
