package com.pivotasoft.selltezvendor.Apis;


import com.pivotasoft.selltezvendor.Response.NotificationBody;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiPush {

    //String SERVER_KEY = "AAAAByHSAiU:APA91bEmwYRW99VEk7qaGz4OL0BhE9ESJ-4YeQl2Bs_XhDfG7AEn3CaorlN--__NBiywt8RpFB1RiErro5AhM75ipLU2FfDObj8KK0Ge_mZMjuPmg85i7VOrU2XRxGLmyAlDmt59HzDn";
    String SERVER_KEY = "AAAAByHSAiU:APA91bHkbu1jnN14OEwuRkV9C1H3MkAm88sBW423vqKbm7VVzkdzf5R2wDoL2BlPDikcMmY2VQuvCV0P7uobqTs6AgLsmNDdlS7lFs1GjuhJWF93PqMfYdYVvjLLQPV0uM6SH40mdVG-";


    @Headers({"Authorization: key=" + SERVER_KEY, "Content-Type:application/json"})
    @POST("fcm/send")
    Call<ResponseBody> sendNotification(@Body NotificationBody root);
}
