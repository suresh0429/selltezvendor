package com.pivotasoft.selltezvendor.Apis;



import com.pivotasoft.selltezvendor.Response.AddressResponse;
import com.pivotasoft.selltezvendor.Response.AnaliticsResponse;
import com.pivotasoft.selltezvendor.Response.AppVersionResponse;
import com.pivotasoft.selltezvendor.Response.BannersResponse;
import com.pivotasoft.selltezvendor.Response.BaseResponse;
import com.pivotasoft.selltezvendor.Response.CartCountResponse;
import com.pivotasoft.selltezvendor.Response.CartResponse;
import com.pivotasoft.selltezvendor.Response.CategoriesResponse;
import com.pivotasoft.selltezvendor.Response.CheckoutPostResponse;
import com.pivotasoft.selltezvendor.Response.CheckoutResponse;
import com.pivotasoft.selltezvendor.Response.CouponResponse;
import com.pivotasoft.selltezvendor.Response.IssuesListResponse;
import com.pivotasoft.selltezvendor.Response.IssuesResponse;
import com.pivotasoft.selltezvendor.Response.LocationsResponse;
import com.pivotasoft.selltezvendor.Response.NotificationReadResponse;
import com.pivotasoft.selltezvendor.Response.NotificationsResponse;
import com.pivotasoft.selltezvendor.Response.OrderListResponse;
import com.pivotasoft.selltezvendor.Response.OrdersResponse;
import com.pivotasoft.selltezvendor.Response.CreateOrderResponse;
import com.pivotasoft.selltezvendor.Response.PostOrderResponse;
import com.pivotasoft.selltezvendor.Response.ProductResponse;
import com.pivotasoft.selltezvendor.Response.ProductSingleResponse;
import com.pivotasoft.selltezvendor.Response.HomeResponse;
import com.pivotasoft.selltezvendor.Response.LoginResponse;
import com.pivotasoft.selltezvendor.Response.MyOrdersResponse;
import com.pivotasoft.selltezvendor.Response.StoreLocations;
import com.pivotasoft.selltezvendor.Response.SubCatResponse;
import com.pivotasoft.selltezvendor.Response.WishListDeleteresponse;
import com.pivotasoft.selltezvendor.Response.WishListPostResponse;
import com.pivotasoft.selltezvendor.Response.WishlistResponse;
import com.pivotasoft.selltezvendor.Response.getMobileresponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

   /* @FormUrlEncoded
    @POST("users/verify-login")
    Call<LoginResponse> userLogin(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("browser_id") String brower_id);
*/


    @FormUrlEncoded
    @POST("storesignin")
    Call<LoginResponse> userLogin(@Field("mobile") String email,
                                  @Field("password") String password
                                  );



    @FormUrlEncoded
    @POST("newBuyer")
    Call<ResponseBody> userRegistrationRequest(@Field("fullname") String name, @Field("email") String email,
                                               @Field("mobile") String mobile, @Field("password") String conf_pwd);



    @GET("storelogin/{mobile}")
    Call<getMobileresponse> getMobileNo(@Path("mobile") String mobile);


    @FormUrlEncoded
    @POST("storefcmvendor")
    Call<ResponseBody> updateFcmTocken(@Field("storeid") String buyerid,
                                @Field("userkey") String userkey,
                                @Field("fcmtoken") String fcmtoken
    );

    @FormUrlEncoded
    @POST("savenotification")
    Call<ResponseBody> saveNotification(@Field("buyerid") String buyerid,
                                        @Field("userkey") String userkey,
                                        @Field("storeid") String storeid,
                                        @Field("content") String content
    );

    @FormUrlEncoded
    @POST("notifications")
    Call<NotificationsResponse> notification(@Field("buyerid") String buyerid,
                                    @Field("userkey") String userkey,
                                    @Field("storeid") String storeid
    );


    @FormUrlEncoded
    @POST("storelocations")
    Call<StoreLocations> storeLocations(@Field("userkey") String userkey,
                                        @Field("latitude") double latitude,
                                        @Field("longitude") double longitude);

    @FormUrlEncoded
    @POST("storecoupons")
    Call<BannersResponse> storeCoupens(@Field("userkey") String userkey,@Field("storeid") String storeid);

    @GET("categories/{userkey}")
    Call<CategoriesResponse> categories(@Path("userkey") String userkey);



    @FormUrlEncoded
    @POST("subcategories")
    Call<SubCatResponse> subcategory(@Field("categoryid") String categoryid, @Field("userkey") String userkey);


    @FormUrlEncoded
    @POST("products")
    Call<ProductResponse> products(@Field("storeid") String storeid, @Field("subcategoryid") String subcategoryid, @Field("userkey") String userkey);


    @FormUrlEncoded
    @POST("cartitems")
    Call<ResponseBody> addCart(@Field("userkey") String userkey,
                               @Field("buyerid") String buyerid,
                               @Field("storeid") String storeid,
                               @Field("productid") String productid,
                               @Field("finalprice") String finalprice,
                               @Field("quantity") String quantity,
                               @Field("itemtotal") String itemtotal,
                               @Field("addedat") String addedat
    );


    @FormUrlEncoded
    @POST("listcart")
    Call<CartResponse> getCart(@Field("userkey") String userkey, @Field("buyerid") String buyerid);

    @FormUrlEncoded
    @POST("removecart")
    Call<ResponseBody> removeCart(@Field("userkey") String userkey, @Field("cartid") String cartid);


    @FormUrlEncoded
    @POST("modifycart")
    Call<ResponseBody> updateCart(@Field("userkey") String userkey,
                                  @Field("cartid") String cartid,
                                  @Field("finalprice") String finalprice,
                                  @Field("quantity") String quantity,
                                  @Field("itemtotal") String itemtotal,
                                  @Field("addedat") String addedat
    );


    @FormUrlEncoded
    @POST("productsearch")
    Call<ProductResponse> searchProducts(@Field("userkey") String userkey, @Field("storeid") String storeid);


    @FormUrlEncoded
    @POST("addresslist")
    Call<AddressResponse> addressList(@Field("userkey") String userkey, @Field("buyerid") String buyerid);

    @FormUrlEncoded
    @POST("billingaddress")
    Call<ResponseBody> createAddress(@Field("userkey") String userkey,
                                     @Field("buyerid") String buyerid,
                                     @Field("title") String title,
                                     @Field("laneandbuilding") String laneandbuilding,
                                     @Field("landmark") String landmark,
                                     @Field("city") String city,
                                     @Field("state") String state

    );


    @FormUrlEncoded
    @POST("modifyaddress")
    Call<ResponseBody> updateAddress(@Field("userkey") String userkey,
                                     @Field("addressid") String addressid,
                                     @Field("title") String title,
                                     @Field("laneandbuilding") String laneandbuilding,
                                     @Field("landmark") String landmark,
                                     @Field("city") String city,
                                     @Field("state") String state);


    @FormUrlEncoded
    @POST("removeaddress")
    Call<ResponseBody> removeAddress(@Field("userkey") String userkey, @Field("addressid") String addressid);


    @FormUrlEncoded
    @POST("verifycoupon")
    Call<CouponResponse> couponsData(@Field("userkey") String userkey,
                                     @Field("storeid") String storeid,
                                     @Field("couponcode") String couponcode,
                                     @Field("buyerid") String buyerid
    );

    @FormUrlEncoded
    @POST("neworder")
    Call<PostOrderResponse> newOrder(@Field("userkey") String userkey,
                                     @Field("storeid") String storeid,
                                     @Field("buyerid") String buyerid,
                                     @Field("bookingdatetime") String bookingdatetime,
                                     @Field("billamount") String billamount,
                                     @Field("couponid") String couponid,
                                     @Field("discount") String discount,
                                     @Field("deliverycharges") String deliverycharges,
                                     @Field("finalbill") String finalbill,
                                     @Field("addressid") String addressid
                                      /* @Field("paymentmode") String paymentmode,
                                       @Field("paymentsummary") String paymentsummary,
                                       @Field("expectedtime") String expectedtime*/
    );


    @FormUrlEncoded
    @POST("processorder")
    Call<CreateOrderResponse> processOrder(@Field("userkey") String userkey,
                                      // @Field("storeid") String storeid,
                                       @Field("buyerid") String buyerid,
                                       @Field("orderid") String orderid,
                                      // @Field("billamount") String billamount,
                                       @Field("couponid") String couponid,
                                      // @Field("discount") String discount,
                                      // @Field("deliverycharges") String deliverycharges,
                                      // @Field("finalbill") String finalbill,
                                      // @Field("addressid") String addressid
                                       @Field("paymentmode") String paymentmode,
                                       @Field("paymentsummary") String paymentsummary,
                                       @Field("expectedtime") String expectedtime
    );



    @FormUrlEncoded
    @POST("cancelorder")
    Call<ResponseBody> cancelOrder(@Field("userkey") String userkey,
                                      @Field("orderid") String orderid
    );




    @FormUrlEncoded
    @POST("storeorderslist")
    Call<OrdersResponse> ordersIdList(@Field("userkey") String userkey,
                                      @Field("storeid") String storeid

    );

    @FormUrlEncoded
    @POST("orderitemlist")
    Call<OrderListResponse> ordersList(@Field("userkey") String userkey,
                                       @Field("orderid") String orderid
    );


    @FormUrlEncoded
    @POST("updateorderstatus")
    Call<ResponseBody> updateOrderStatus(@Field("orderstatus") String orderstatus,
                                       @Field("orderid") String orderid,
                                       @Field("expectedtime") String expectedtime,
                                       @Field("userkey") String userkey
    );

    @FormUrlEncoded
    @POST("storeanalytics")
    Call<AnaliticsResponse> storeAnalitics(@Field("storeid") String storeid,
                                           @Field("userkey") String userkey,
                                           @Field("duration") String duration
    );




    @FormUrlEncoded
    @POST("storereviews")
    Call<ResponseBody> addReview(@Field("storeid") String storeid,
                                       @Field("orderid") String orderid,
                                       @Field("rating") String rating
    );


    @FormUrlEncoded
    @POST("storeissueslist")
    Call<IssuesListResponse> issuesList(@Field("userkey") String userkey,
                                        @Field("storeid") String storeid
    );

    @FormUrlEncoded
    @POST("issueresponse")
    Call<ResponseBody> issueresponse(@Field("userkey") String userkey,
                                        @Field("issueid") String issueid,
                                        @Field("resolvedescription") String resolvedescription
    );


    @FormUrlEncoded
    @POST("orderissue")
    Call<IssuesResponse> addIssue(@Field("description") String description,
                                  @Field("orderid") String orderid,
                                  @Field("assignedon") String assignedon,
                                  @Field("userkey") String userkey
    );


    @FormUrlEncoded
    @POST("clearcart")
    Call<ResponseBody> clearCart(@Field("userkey") String userkey,
                                        @Field("buyerid") String buyerid
    );

    @FormUrlEncoded
    @POST("updatestorepassword")
    Call<ResponseBody> updatePassword(@Field("userkey") String userkey,
                                 @Field("storeid") String buyerid,
                                 @Field("password") String password
    );

    @FormUrlEncoded
    @POST("updatestoreprofile")
    Call<ResponseBody> updateProfile(@Field("userkey") String userkey,
                                 @Field("storeid") String buyerid,
                                 @Field("owner") String owner,
                                 @Field("title") String title,
                                 @Field("email") String email
    );


    @FormUrlEncoded
    @POST("modifyproduct")
    Call<ResponseBody> modifyProduct(@Field("userkey") String userkey,
                                     @Field("storeid") String buyerid,
                                     @Field("productid") String productid,
                                     @Field("title") String title,
                                     @Field("unitprice") String unitprice,
                                     @Field("measureunits") String measureunits,
                                     @Field("discount") String discount,
                                     @Field("finalprice") String finalprice,
                                     @Field("description") String description,
                                     @Field("status") String status
    );

    @FormUrlEncoded
    @POST("newcoupon")
    Call<ResponseBody> newCoupon(@Field("userkey") String userkey,
                                     @Field("storeid") String buyerid,
                                     @Field("couponcode") String couponcode,
                                     @Field("description") String description,
                                     @Field("mintransaction") String mintransaction,
                                     @Field("discountpercent") String discountpercent,
                                     @Field("maxdiscount") String maxdiscount,
                                     @Field("fromdate") String fromdate,
                                     @Field("todate") String todate
    );


    @FormUrlEncoded
    @POST("modifycoupon")
    Call<ResponseBody> modifyCoupon(@Field("userkey") String userkey,
                                 @Field("storeid") String buyerid,
                                 @Field("couponid") String couponid,
                                 @Field("couponcode") String couponcode,
                                 @Field("description") String description,
                                 @Field("mintransaction") String mintransaction,
                                 @Field("discountpercent") String discountpercent,
                                 @Field("maxdiscount") String maxdiscount,
                                 @Field("fromdate") String fromdate,
                                 @Field("todate") String todate
    );































    @FormUrlEncoded
    @POST("users/forgot-password")
    Call<BaseResponse> userForgotPassword(@Field("mobile") String mobile);


    @FormUrlEncoded
    @POST("users/verify-otp-forgot-password")
    Call<BaseResponse> userVerifyForgotPassword(@Field("mobile") String mobile,
                                                @Field("otp") String otp,
                                                @Field("browser_id") String browser_id,
                                                @Field("new_pwd") String new_pwd);


    @FormUrlEncoded
    @POST("user/{id}/logout")
    Call<BaseResponse> userLogout(@Header ("Authorization-Basic") String tokenValue,@Path("id") String id, @Field("browser_id") String browser_id);

    @FormUrlEncoded
    @POST("user/{id}/change-password")
    Call<BaseResponse> userChangePassword(@Header ("Authorization-Basic") String tokenValue,@Path("id") String id, @Field("old_pwd") String old_pwd,@Field("new_pwd") String new_pwd);

    @FormUrlEncoded
    @POST("user/{id}/profile")
    Call<BaseResponse> updateProfile(@Header ("Authorization-Basic") String tokenValue,@Path("id") String id,
                                          @Field("name") String name,@Field("email") String email,
                                          @Field("mobile") String mobile,@Field("gender") String gender);



    @GET("banners")
    Call<BannersResponse> getHomeBanners();


    @GET("locations")
    Call<LocationsResponse> getLocations();



    @GET("categories")
    Call<HomeResponse> getHomePageRequest(@Query("type") String type,@Query("main_category") String main_category,@Query("sub_category") String sub_category);



    @GET("products")
    Call<ProductResponse> getPopularProduct(@Query("type") String type,
                                           @Query("browser_id") String browser_id,
                                           @Query("sort_by") String sort_by
                                           //@Query("search") String search
    );



    @GET("products")
    Call<ProductResponse> getProductSearch(@Query("type") String type,
                                         /*@Query("user_id") String user_id,*/
                                         @Query("browser_id") String browser_id,
                                         @Query("search") String search

    );


    @GET("products")
    Call<ProductResponse> getProductList(@Query("main_category") String main_category,
                                         @Query("sub_category") String sub_category,
                                         @Query("child_category") String child_category,
                                         @Query("type") String type,
                                         @Query("page") String page,
                                         @Query("user_id") String user_id,
                                         @Query("browser_id") String browser_id,
                                         @Query("search") String search,
                                         @Query("brand_id") String brand_id,
                                         @Query("sort_by") String sort_by
                                          );





    @GET("product/{id}")
    Call<ProductSingleResponse> getProductdetails(@Path("id") String productId, @Query("browser_id") String browser_id);

    @FormUrlEncoded
    @POST("cart")
    Call<BaseResponse> addtoCart(@Field("product_id") String product_id, @Field("browser_id") String browser_id,
                                                 @Field("user_id") String user_id, @Field("purchase_quantity") String purchase_quantity);


    @GET("cart")
    Call<CartResponse> cartItemsList(@Query("user_id") String userId,@Query("browser_id") String browser_id);


    @FormUrlEncoded
    @POST("cart/quantity")
    Call<BaseResponse> updateQuantity(@Field("cart_id") String cart_id,
                                                @Field("purchase_quantity") String purchase_quantity);

    @FormUrlEncoded
    @POST("cart/delete")
    Call<BaseResponse> deleteCart(@Field("user_id") String user_id,@Field("cart_id") String cart_id,@Field("browser_id") String browser_id);



    @GET("cart/count")
    Call<CartCountResponse> getCartCount(@Query("user_id") String userId, @Query("browser_id") String  browser_id);


    @GET("user/{id}/addresses")
    Call<AddressResponse> getAddress(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId);


    @FormUrlEncoded
    @POST("user/{id}/address")
    Call<BaseResponse> userAddress(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId,
                                          @Field("name") String name, @Field("address_line1") String address_line1,
                                          @Field("address_line2") String address_line2, @Field("area") String area,
                                          @Field("city") String city, @Field("state") String state,
                                          @Field("pincode") String pincode, @Field("contact_no") String contact_no,
                                          @Field("alternate_contact_no") String alternate_contact_no,@Field("latitude") String latitude,
                                          @Field("longitude") String longitude,@Field("is_default") String is_default
                                          );


    @FormUrlEncoded
    @POST("user/{id}/address/{id1}")
    Call<BaseResponse> updateuserAddress(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId,@Path("id1") String addressid,
                                   @Field("name") String name, @Field("address_line1") String address_line1,
                                   @Field("address_line2") String address_line2, @Field("area") String area,
                                   @Field("city") String city, @Field("state") String state,
                                   @Field("pincode") String pincode, @Field("contact_no") String contact_no,
                                   @Field("alternate_contact_no") String alternate_contact_no,@Field("latitude") String latitude,
                                   @Field("longitude") String longitude,@Field("is_default") String is_default
    );



    @DELETE("user/{id}/address/{id1}")
    Call<BaseResponse> deleteuserAddress(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId,@Path("id1") String addressid);



    @GET("checkout")
    Call<CheckoutResponse> checkOutStatus(@Header ("Authorization-Basic") String tokenValue,@Query("user_id") String user_id,@Query("address_id") String address_id);


    @FormUrlEncoded
    @POST("checkout")
    Call<CheckoutPostResponse> checkoutPost(@Header ("Authorization-Basic") String tokenValue, @Field("user_id") String user_id, @Field("address_id") String address_id, @Field("code") String code,
                                                     @Field("payment_gateway_id") String payment_gateway_id, @Field("platform") String platform);


    @GET("user/{id}/orders")
    Call<MyOrdersResponse> MyOrders(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId);

    @GET("user/{id}/orders/{id1}")
    Call<OrdersResponse> getOrderList(@Header ("Authorization-Basic") String tokenValue, @Path("id") String userId, @Path("id1") String orderId);

    @GET("user/{id}/notification")
    Call<NotificationsResponse> Notifications(@Header ("Authorization-Basic") String tokenValue, @Path("id") String userId);

    @PUT("user/{id}/notification/{id1}/read")
    Call<NotificationReadResponse> NotificationRead(@Header ("Authorization-Basic") String tokenValue, @Path("id") String userId, @Path("id1") String notificationId);


  /*  @FormUrlEncoded
    @PUT("fcm-token")
    Call<BaseResponse> updateFcmTocken(@Header ("Authorization-Basic") String tokenValue, @Field("user_id") String user_id, @Field("fcm_token") String fcm_token);
*/

    @GET("app/version")
    Call<AppVersionResponse> CheckAppUpdate(@Header ("Authorization-Basic") String tokenValue);



    @GET("wishList")
    Call<WishlistResponse> getWhishlisList(@Query("user_id") String userId);



    @DELETE("wishList_items/{id}")
    Call<WishListDeleteresponse> deleteWishList(@Path("id") String id);


    @FormUrlEncoded
    @POST("wishList")
    Call<WishListPostResponse> addWishList(@Field("module") String module, @Field("user_id") String userId,
                                           @Field("item_id") String item_id, @Field("add_type") String add_type);

}
