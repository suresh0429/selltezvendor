package com.pivotasoft.selltezvendor.Apis;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    //public static final String MAIN_URL = "http://www.vizagnext.com/";
    public static final String MAIN_URL = "http://www.win365cpanel.in/";

    public static final String BASE_URL = MAIN_URL+"selltez/index.php/api/doctor/";
    public static final String IMAGE_PRODUCT_URL = MAIN_URL+"selltez/uploads/productpics/";
    public static final String IMAGE_CAT_URL = MAIN_URL+"selltez/uploads/catpics/";

    public static final String PRODUCT_IMAGE_BASE_URL = IMAGE_PRODUCT_URL +"images/products/70x70/";
    public static final String PRODUCT_IMAGE_BASE_URL1 = IMAGE_PRODUCT_URL +"images/products/150x200/";
    public static final String PRODUCT_IMAGE_BASE_URL2 = IMAGE_PRODUCT_URL +"images/products/400x400/";

    // web links url
    public static final String ABOUT_US_URL ="http://selltez.com/app-about.html";
    public static final String PRIVACY_POLICY_URL ="http://selltez.com/privacy-policy.html";
    public static final String TERMS_CONDITIONS_URL ="http://selltez.com/terms-conditions.html";
    public static final String FAQ_URL ="http://selltez.com/faq.html";
    public static final String RETURN_POLICY_URL ="";


    private static RetrofitClient mInstance;
    private Retrofit retrofit;

    private RetrofitClient() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Interceptor.Chain chain) throws IOException {
                                Request request = chain.request().newBuilder()
                                        .addHeader("Accept", "Application/JSON")
                                       /* .addHeader("X-Api-Key", "003026bbc133714df1834b8638bb496e-8f4b3d9a-e931-478d-a994-28a725159ab9")
                                        .addHeader("X-Platform", "ANDROID")*/
                                        .build();
                                return chain.proceed(request);
                            }
                        }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }
}
