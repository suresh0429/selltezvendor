package com.pivotasoft.selltezvendor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.pivotasoft.selltezvendor.Adapters.SubCatRecyclerViewDataAdapter;
import com.pivotasoft.selltezvendor.Adapters.SubCatageoryAdapter;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Helper.Converter;
import com.pivotasoft.selltezvendor.Model.HeaderSectionDataModel;
import com.pivotasoft.selltezvendor.Model.SingleItemModel;
import com.pivotasoft.selltezvendor.Response.ProductResponse;
import com.pivotasoft.selltezvendor.Response.SubCatResponse;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pivotasoft.selltezvendor.Storage.Utilities.capitalize;

public class GroceryHomeActivity extends AppCompatActivity {
    public static String MODULE = "grocery";
    AppController appController;
    @BindView(R.id.catRecycler)
    RecyclerView catRecycler;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.simpleSwipeRefreshLayout)
    SwipeRefreshLayout simpleSwipeRefreshLayout;

    ArrayList<HeaderSectionDataModel> allSampleData;


    private SubCatageoryAdapter adapter;
    private SubCatRecyclerViewDataAdapter subCatRecyclerViewDataAdapter;

    private PrefManager pref;
    String userid,tokenValue,deviceId,storeName;
    int cartindex;
    String catId,cat_name,storeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_home);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        appController = (AppController) getApplication();

        pref = new PrefManager(getApplicationContext());

        if (getIntent() != null){
            catId = getIntent().getStringExtra("CatId");
            cat_name=getIntent().getStringExtra("Cat_name");
            storeId=getIntent().getStringExtra("storeId");
            storeName = getIntent().getStringExtra("storeName");
        }

        getSupportActionBar().setTitle(capitalize(cat_name));

        allSampleData = new ArrayList<HeaderSectionDataModel>();


        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userid = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        simpleSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);
        appController = (AppController) getApplicationContext();
        if (appController.isConnection()) {

            prepareSubCatData();
            // cart count
            appController.cartCount(userid,tokenValue);
            SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
            cartindex = preferences.getInt("itemCount", 0);
            Log.e("cartindex", "" + cartindex);
            invalidateOptionsMenu();


            simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    prepareSubCatData();

                    simpleSwipeRefreshLayout.setRefreshing(false);
                }
            });

        } else {

            setContentView(R.layout.internet);
            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }


    }

    private void prepareSubCatData() {

        simpleSwipeRefreshLayout.setRefreshing(true);
        Call<SubCatResponse> call = RetrofitClient.getInstance().getApi().subcategory(catId,tokenValue);
        call.enqueue(new Callback<SubCatResponse>() {
            @Override
            public void onResponse(Call<SubCatResponse> call, Response<SubCatResponse> response) {
                simpleSwipeRefreshLayout.setRefreshing(false);

                SubCatResponse subCatResponse = response.body();

                if (response.isSuccessful()) {

                    // subCat Recycler
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                    catRecycler.setLayoutManager(mLayoutManager);
                    catRecycler.setItemAnimator(new DefaultItemAnimator());
                    catRecycler.setHasFixedSize(true);
                    catRecycler.setNestedScrollingEnabled(false);

                    // subcat Adapter
                    adapter = new SubCatageoryAdapter(getApplicationContext(), subCatResponse.getSubcategoriesdata(),catId,storeId,storeName);
                    catRecycler.setAdapter(adapter);
                    adapter.notifyDataSetChanged();


                    allSampleData.clear();
                    for (SubCatResponse.SubcategoriesdataBean subCategoryBean : subCatResponse.getSubcategoriesdata()) {

                        productsData(subCategoryBean.getSubcategoryname(), subCategoryBean.getSubcategoryid(), catId,storeId,storeName);
                    }


                    } else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.not_found) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.server_broken) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                        default:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.unknown_error) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }


            }

            @Override
            public void onFailure(Call<SubCatResponse> call, Throwable t) {
                simpleSwipeRefreshLayout.setRefreshing(false);
                Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });


    }

    private void productsData(String title, String subCategory_Id, String category_id, String storeId, final String storeName){

        final HeaderSectionDataModel dm = new HeaderSectionDataModel();
        dm.setHeaderTitle(title);
        dm.setCategoryId(category_id);
        dm.setSubcategoryId(subCategory_Id);
        dm.setStoreId(storeId);

        final ArrayList<SingleItemModel> singleItem = new ArrayList<SingleItemModel>();

        simpleSwipeRefreshLayout.setRefreshing(true);
        Call<ProductResponse> call = RetrofitClient.getInstance().getApi().products(storeId,subCategory_Id,tokenValue);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                simpleSwipeRefreshLayout.setRefreshing(false);

                ProductResponse productResponse1 = response.body();


                if (response.isSuccessful()) {

                    for (ProductResponse.ProductsdataBean productResponse : productResponse1.getProductsdata()){

                        singleItem.add(new SingleItemModel(productResponse.getProductid(),productResponse.getProductpic(),productResponse.getTitle(),productResponse.getMeasureunits(),
                                productResponse.getUnitprice(),productResponse.getDiscount(),productResponse.getFinalprice(),productResponse.getDescription(),productResponse.getSubcategoryid(),productResponse.getStoreid(),productResponse.getStatus()));
                    }

                    dm.setAllItemsInSection(singleItem);
                    allSampleData.add(dm);

                    // product Recycler
                    RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    myRecyclerView.setLayoutManager(mLayoutManager1);
                    myRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    myRecyclerView.setHasFixedSize(true);
                    myRecyclerView.setNestedScrollingEnabled(false);

                    // product Adapter
                    subCatRecyclerViewDataAdapter = new SubCatRecyclerViewDataAdapter(getApplicationContext(), allSampleData,singleItem,storeName);
                    myRecyclerView.setAdapter(subCatRecyclerViewDataAdapter);
                    subCatRecyclerViewDataAdapter.notifyDataSetChanged();


                } else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.not_found) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.server_broken) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                        default:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.unknown_error) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }


            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                simpleSwipeRefreshLayout.setRefreshing(false);
                Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });


    }



    @Override
    protected void onRestart() {

        appController.cartCount(userid,tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        appController.cartCount(userid,tokenValue);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onStart();
    }

    ///
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setIcon(Converter.convertLayoutToImage(GroceryHomeActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("storeId",storeId);
                intent.putExtra("storeName",storeName);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.action_search:
                Intent intent1 = new Intent(GroceryHomeActivity.this, SearchActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
