package com.pivotasoft.selltezvendor;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import java.util.Objects;

public class SubCategoeryActivity extends AppCompatActivity {
    String catId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_categoery);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null){

            catId = getIntent().getStringExtra("catId");
            String catName = getIntent().getStringExtra("title");
            getSupportActionBar().setTitle(catName);
        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

              onBackPressed();

                break;

        }

        return super.onOptionsItemSelected(item);
    }

}
