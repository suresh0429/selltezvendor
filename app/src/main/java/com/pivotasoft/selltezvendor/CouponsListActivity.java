package com.pivotasoft.selltezvendor;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pivotasoft.selltezvendor.Adapters.CouponsAdapter;
import com.pivotasoft.selltezvendor.Apis.RetrofitClient;
import com.pivotasoft.selltezvendor.Response.BannersResponse;
import com.pivotasoft.selltezvendor.Singleton.AppController;
import com.pivotasoft.selltezvendor.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponsListActivity extends AppCompatActivity {

    @BindView(R.id.recyclerCoupons)
    RecyclerView recyclerCoupons;
    @BindView(R.id.fabAdd)
    FloatingActionButton fabAdd;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.txtError)
    TextView txtError;
    AppController appController;
    private PrefManager pref;
    String userId, tokenValue, deviceId,storeId,storeName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Coupons List");

        appController = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (appController.isConnection()) {

            couponsList(userId);

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }
    }

    public void couponsList(String storeId) {

        Call<BannersResponse> call = RetrofitClient.getInstance().getApi().storeCoupens(tokenValue, storeId);
        call.enqueue(new Callback<BannersResponse>() {
            @Override
            public void onResponse(Call<BannersResponse> call, Response<BannersResponse> response) {
                BannersResponse bannersResponse = response.body();
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {

                    assert response.body() != null;
                    final List<BannersResponse.CouponsdataBean> modulesBeanList = response.body().getCouponsdata();
                    if (modulesBeanList.size() != 0) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        layoutManager.setReverseLayout(true);
                        layoutManager.setStackFromEnd(true);
                        recyclerCoupons.setItemAnimator(new DefaultItemAnimator());
                        recyclerCoupons.setLayoutManager(layoutManager);
                        CouponsAdapter adapter = new CouponsAdapter(getApplicationContext(), modulesBeanList);
                        recyclerCoupons.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        //Toasty.success(CouponsListActivity.this, modulesBeanList.getMessage(), Toast.LENGTH_SHORT).show();
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText("No Coupons Found !");
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("No Coupons Found !");
                }




            }

            @Override
            public void onFailure(Call<BannersResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fabAdd)
    public void onViewClicked() {
        Intent intent = new Intent(getApplicationContext(), AddCouponActivity.class);
        intent.putExtra("actionBarTitle", "Add Coupon");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
