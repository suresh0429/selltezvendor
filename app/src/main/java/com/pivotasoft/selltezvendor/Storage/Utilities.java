package com.pivotasoft.selltezvendor.Storage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utilities {

    public static final String PREFS_LOCATION = "LOCATION_PREF";

    public static final String KEY_LOCATIONFIRSTTIME="LocationFirstTime";
    public static final String KEY_STORE_ID="storeId";
    public static final String KEY_STORE_NAME="storeName";
    public static final String KEY_PINCODE="pincode";
    public static final String KEY_AREA="area";
    public static final String KEY_SHIPPINGCHARGES="shipping_charges";



    public static String capitalize(String capString){
            StringBuffer capBuffer = new StringBuffer();
            Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
            while (capMatcher.find()){
                capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
            }

            return capMatcher.appendTail(capBuffer).toString();

    }
}
